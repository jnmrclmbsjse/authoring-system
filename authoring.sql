-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: authoring
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.4-MariaDB-1:10.5.4+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblAssessments`
--

DROP TABLE IF EXISTS `tblAssessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblAssessments` (
  `fldAssessmentId` int(11) NOT NULL AUTO_INCREMENT,
  `fldAssessmentDescription` text DEFAULT NULL,
  `fldAssessmentDeleted` tinyint(4) DEFAULT 0,
  `fldAssessmentCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldAssessmentModified` datetime DEFAULT NULL,
  `tblTeachersCourses_fldTeacherCourseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldAssessmentId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAssessments`
--

LOCK TABLES `tblAssessments` WRITE;
/*!40000 ALTER TABLE `tblAssessments` DISABLE KEYS */;
INSERT INTO `tblAssessments` VALUES (1,'Sample description',0,'2020-04-26 19:13:29','2020-04-26 19:13:29',3);
/*!40000 ALTER TABLE `tblAssessments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblAssessmentsAnswers`
--

DROP TABLE IF EXISTS `tblAssessmentsAnswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblAssessmentsAnswers` (
  `fldAssessmentAnswerId` int(11) NOT NULL AUTO_INCREMENT,
  `fldAssessmentAnswerContent` text DEFAULT NULL,
  `fldAssessmentAnswerCorrect` tinyint(4) DEFAULT 0,
  `fldAssessmentAnswerDeleted` tinyint(4) DEFAULT 0,
  `fldAssessmentAnswerCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldAssessmentAnswerModified` datetime DEFAULT NULL,
  `tblAssessmentsQuestions_fldAssessmentQuestionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldAssessmentAnswerId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAssessmentsAnswers`
--

LOCK TABLES `tblAssessmentsAnswers` WRITE;
/*!40000 ALTER TABLE `tblAssessmentsAnswers` DISABLE KEYS */;
INSERT INTO `tblAssessmentsAnswers` VALUES (1,'2',1,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1),(2,'3',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1),(3,'4',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1),(4,'5',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1),(5,'2',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',2),(6,'3',1,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',2),(7,'4',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',2),(8,'5',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',2),(9,'2',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',3),(10,'3',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',3),(11,'4',1,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',3),(12,'5',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',3),(13,'2',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',4),(14,'3',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',4),(15,'4',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',4),(16,'5',1,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',4),(17,'8',1,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',5),(18,'3',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',5),(19,'4',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',5),(20,'5',0,0,'2020-04-26 19:13:29','2020-04-26 19:13:29',5);
/*!40000 ALTER TABLE `tblAssessmentsAnswers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblAssessmentsQuestions`
--

DROP TABLE IF EXISTS `tblAssessmentsQuestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblAssessmentsQuestions` (
  `fldAssessmentQuestionId` int(11) NOT NULL AUTO_INCREMENT,
  `fldAssessmentQuestionContent` longtext DEFAULT NULL,
  `fldAssessmentQuestionDeleted` tinyint(4) DEFAULT 0,
  `fldAssessmentQuestionCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldAssessmentQuestionModified` datetime DEFAULT NULL,
  `tblAssessments_fldAssessmentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldAssessmentQuestionId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAssessmentsQuestions`
--

LOCK TABLES `tblAssessmentsQuestions` WRITE;
/*!40000 ALTER TABLE `tblAssessmentsQuestions` DISABLE KEYS */;
INSERT INTO `tblAssessmentsQuestions` VALUES (1,'What is the answer?\n1 + 1 = ?',0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1),(2,'What is the answer?\n2 + 1 = ?',0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1),(3,'What is the answer?\n3 + 1 = ?',0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1),(4,'What is the answer?\n1 + 4 = ?',0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1),(5,'What is the answer?\n3 + 5 = ?',0,'2020-04-26 19:13:29','2020-04-26 19:13:29',1);
/*!40000 ALTER TABLE `tblAssessmentsQuestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblAssessmentsResults`
--

DROP TABLE IF EXISTS `tblAssessmentsResults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblAssessmentsResults` (
  `fldAssessmentResultId` int(11) NOT NULL AUTO_INCREMENT,
  `fldAssessmentResultScore` varchar(45) DEFAULT NULL,
  `fldAssessmentResultPercentage` varchar(45) DEFAULT NULL,
  `fldAssessmentResultReference` varchar(45) DEFAULT NULL,
  `fldAssessmentResultDeleted` tinyint(4) DEFAULT 0,
  `fldAssessmentResultCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldAssessmentResultModified` datetime DEFAULT NULL,
  `tblTeachersCourses_fldTeacherCourseId` int(11) DEFAULT NULL,
  `tblStudents_fldStudentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldAssessmentResultId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAssessmentsResults`
--

LOCK TABLES `tblAssessmentsResults` WRITE;
/*!40000 ALTER TABLE `tblAssessmentsResults` DISABLE KEYS */;
INSERT INTO `tblAssessmentsResults` VALUES (1,'4','80',NULL,0,'2020-04-26 19:19:35','2020-04-26 19:19:35',3,1),(2,'4','80',NULL,0,'2020-07-12 15:01:37','2020-07-12 15:01:37',3,3);
/*!40000 ALTER TABLE `tblAssessmentsResults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblConcepts`
--

DROP TABLE IF EXISTS `tblConcepts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblConcepts` (
  `fldConceptId` int(11) NOT NULL AUTO_INCREMENT,
  `fldConceptTitle` varchar(45) DEFAULT NULL,
  `fldConceptMetadata` longtext DEFAULT NULL,
  `fldConceptActive` tinyint(4) DEFAULT 1,
  `fldConceptDeleted` tinyint(4) DEFAULT 0,
  `fldConceptCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldConceptModified` datetime DEFAULT NULL,
  `tblTeachers_fldTeacherId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fldConceptId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblConcepts`
--

LOCK TABLES `tblConcepts` WRITE;
/*!40000 ALTER TABLE `tblConcepts` DISABLE KEYS */;
INSERT INTO `tblConcepts` VALUES (1,'Updated','<mxGraphModel><root><mxCell id=\"0\" value=\"concept-map\"/><mxCell id=\"1\" parent=\"0\"/><Basic-Mathematics title=\"Basic Mathematics\" isCourse=\"true\" id=\"2\"><mxCell parent=\"1\" vertex=\"1\"><mxGeometry x=\"320\" y=\"50\" width=\"200\" height=\"40\" as=\"geometry\"/></mxCell></Basic-Mathematics><mxCell id=\"b42bd356-aeea-4633-8187-43d6f6012766\" value=\"Algebra\" style=\"\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"319.84375\" y=\"140\" width=\"200\" height=\"40\" as=\"geometry\"/></mxCell><mxCell id=\"3\" parent=\"1\" source=\"2\" target=\"b42bd356-aeea-4633-8187-43d6f6012766\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"15127f62-a465-48bf-ba72-b3b735159f21\" value=\"Trigonometry\" style=\"\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"319.84375\" y=\"230\" width=\"200\" height=\"40\" as=\"geometry\"/></mxCell><mxCell id=\"4\" parent=\"1\" source=\"b42bd356-aeea-4633-8187-43d6f6012766\" target=\"15127f62-a465-48bf-ba72-b3b735159f21\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"eeb48ae4-5bb4-4f4b-9c54-226ca27a7395\" value=\"Differential Calculus\" style=\"\" vertex=\"1\" parent=\"1\"><mxGeometry x=\"319.84375\" y=\"320\" width=\"200\" height=\"40\" as=\"geometry\"/></mxCell><mxCell id=\"5\" parent=\"1\" source=\"15127f62-a465-48bf-ba72-b3b735159f21\" target=\"eeb48ae4-5bb4-4f4b-9c54-226ca27a7395\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell></root></mxGraphModel>',1,0,'2020-04-26 18:34:01','2020-04-26 18:36:18','1');
/*!40000 ALTER TABLE `tblConcepts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblContents`
--

DROP TABLE IF EXISTS `tblContents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblContents` (
  `fldContentId` varchar(250) NOT NULL,
  `fldContentTitle` varchar(45) DEFAULT NULL,
  `fldContentType` varchar(45) DEFAULT NULL,
  `fldContentFile` varchar(100) DEFAULT NULL,
  `fldContentDeleted` tinyint(4) DEFAULT 0,
  `fldContentCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldContentModified` datetime DEFAULT NULL,
  `tblNodes_fldNodeId` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`fldContentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblContents`
--

LOCK TABLES `tblContents` WRITE;
/*!40000 ALTER TABLE `tblContents` DISABLE KEYS */;
INSERT INTO `tblContents` VALUES ('550653dd-89b2-4fa0-b6b3-71ec04b43d05','WORD','word','be271756dafca1bb.xlsx',0,'2020-04-26 19:05:58','2020-04-26 19:07:27','15127f62-a465-48bf-ba72-b3b735159f21'),('7ed1fca5-8a14-41a8-a3ad-c1d1e97ae521','WORD','word','35001f84f6eff81c.png',0,'2020-04-26 19:00:01','2020-05-02 19:59:48','b42bd356-aeea-4633-8187-43d6f6012766'),('9d7dbb8e-2010-431d-bcd6-969e24ab537e','PRESENTATION','presentation','aa34a7d1d60d0019.sql',0,'2020-04-26 19:07:27','2020-04-26 19:07:27','15127f62-a465-48bf-ba72-b3b735159f21'),('af97eab2-169b-4ca2-a83b-69ef3c79395a','FINAL','final','af97eab2-169b-4ca2-a83b-69ef3c79395a',0,'2020-04-26 19:07:27','2020-04-26 19:07:27','15127f62-a465-48bf-ba72-b3b735159f21'),('cd63c3a8-0854-4934-9c75-162b94f40547','QUIZ','quiz','cd63c3a8-0854-4934-9c75-162b94f40547',0,'2020-04-26 19:00:01','2020-05-02 19:59:48','b42bd356-aeea-4633-8187-43d6f6012766'),('d04a8e97-4ea2-4e55-b641-d3511e57c79e','QUIZ','quiz','d04a8e97-4ea2-4e55-b641-d3511e57c79e',0,'2020-04-26 19:05:58','2020-04-26 19:07:27','15127f62-a465-48bf-ba72-b3b735159f21'),('e37f1fdb-42b9-403c-829a-669c4d2fd50e','FINAL','final','e37f1fdb-42b9-403c-829a-669c4d2fd50e',0,'2020-04-26 19:00:01','2020-05-02 19:59:48','b42bd356-aeea-4633-8187-43d6f6012766');
/*!40000 ALTER TABLE `tblContents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblCourses`
--

DROP TABLE IF EXISTS `tblCourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblCourses` (
  `fldCourseId` int(11) NOT NULL AUTO_INCREMENT,
  `fldCourseCode` varchar(45) DEFAULT NULL,
  `fldCourseTitle` varchar(45) DEFAULT NULL,
  `fldCourseDeleted` tinyint(4) DEFAULT 0,
  `fldCourseCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldCourseModified` datetime DEFAULT NULL,
  PRIMARY KEY (`fldCourseId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblCourses`
--

LOCK TABLES `tblCourses` WRITE;
/*!40000 ALTER TABLE `tblCourses` DISABLE KEYS */;
INSERT INTO `tblCourses` VALUES (1,'MATH-202','Mathematics',1,'2020-04-26 17:09:37','2020-04-26 17:23:56'),(2,'MATH-102','Basic Mathematics',1,'2020-04-26 17:24:25','2020-05-02 18:50:43'),(3,'MATH-103','Advanced Mathematics',0,'2020-05-02 17:12:02','2020-05-02 17:12:02'),(4,'MATH-102','Basic Mathematics',0,'2020-05-02 18:51:00','2020-05-02 18:51:00');
/*!40000 ALTER TABLE `tblCourses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblHabits`
--

DROP TABLE IF EXISTS `tblHabits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblHabits` (
  `fldHabitId` int(11) NOT NULL AUTO_INCREMENT,
  `fldHabitDeleted` tinyint(4) DEFAULT 0,
  `fldHabitCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldHabitModified` datetime DEFAULT NULL,
  `tblSemesters_fldSemesterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldHabitId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblHabits`
--

LOCK TABLES `tblHabits` WRITE;
/*!40000 ALTER TABLE `tblHabits` DISABLE KEYS */;
INSERT INTO `tblHabits` VALUES (1,0,'2020-04-26 19:10:12','2020-04-26 19:10:12',2);
/*!40000 ALTER TABLE `tblHabits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblHabitsQuestions`
--

DROP TABLE IF EXISTS `tblHabitsQuestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblHabitsQuestions` (
  `fldHabitQuestionId` int(11) NOT NULL AUTO_INCREMENT,
  `fldHabitQuestionContent` text DEFAULT NULL,
  `fldHabitQuestionCategory` varchar(45) DEFAULT NULL,
  `fldHabitQuestionDeleted` tinyint(4) DEFAULT 0,
  `fldHabitQuestionCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldHabitQuestionModified` datetime DEFAULT NULL,
  `tblHabits_fldHabitId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldHabitQuestionId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblHabitsQuestions`
--

LOCK TABLES `tblHabitsQuestions` WRITE;
/*!40000 ALTER TABLE `tblHabitsQuestions` DISABLE KEYS */;
INSERT INTO `tblHabitsQuestions` VALUES (1,'How fast can you answer questions?','time_management_and_procrastination',0,'2020-04-26 19:10:12','2020-04-26 19:10:12',1),(2,'How focused are you?','concentration_and_memory',0,'2020-04-26 19:10:12','2020-04-26 19:10:12',1),(3,'How often do you jot down notes?','study_aids_and_note-taking',0,'2020-04-26 19:10:12','2020-04-26 19:10:12',1),(4,'Are you good at organizing things?','organizing_and_processing_information',0,'2020-04-26 19:10:12','2020-04-26 19:10:12',1),(5,'Reading comprehension capability?','reading_and_selecting_the_main_idea',0,'2020-04-26 19:10:12','2020-04-26 19:10:12',1);
/*!40000 ALTER TABLE `tblHabitsQuestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblHabitsResults`
--

DROP TABLE IF EXISTS `tblHabitsResults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblHabitsResults` (
  `fldHabitResultId` int(11) NOT NULL AUTO_INCREMENT,
  `fldHabitResultScore` int(11) DEFAULT 0,
  `fldHabitResultDeleted` tinyint(4) DEFAULT 0,
  `fldHabitResultCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldHabitResultModified` datetime DEFAULT NULL,
  `tblHabitsQuestions_fldHabitQuestionId` int(11) DEFAULT NULL,
  `tblStudents_fldStudentId` int(11) DEFAULT NULL,
  `fldHabitResultQuestionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldHabitResultId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblHabitsResults`
--

LOCK TABLES `tblHabitsResults` WRITE;
/*!40000 ALTER TABLE `tblHabitsResults` DISABLE KEYS */;
INSERT INTO `tblHabitsResults` VALUES (1,5,0,'2020-04-26 19:15:01','2020-04-26 19:15:01',1,1,NULL),(2,5,0,'2020-04-26 19:15:01','2020-04-26 19:15:01',2,1,NULL),(3,4,0,'2020-04-26 19:15:01','2020-04-26 19:15:01',3,1,NULL),(4,4,0,'2020-04-26 19:15:01','2020-04-26 19:15:01',4,1,NULL),(5,3,0,'2020-04-26 19:15:01','2020-04-26 19:15:01',5,1,NULL);
/*!40000 ALTER TABLE `tblHabitsResults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblLogs`
--

DROP TABLE IF EXISTS `tblLogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblLogs` (
  `fldLogId` int(11) NOT NULL AUTO_INCREMENT,
  `fldLogType` varchar(45) DEFAULT NULL,
  `fldLogDescription` varchar(250) DEFAULT NULL,
  `fldLogDeleted` tinyint(4) DEFAULT 0,
  `fldLogCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldLogModified` datetime DEFAULT NULL,
  `tblUsers_fldUserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldLogId`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblLogs`
--

LOCK TABLES `tblLogs` WRITE;
/*!40000 ALTER TABLE `tblLogs` DISABLE KEYS */;
INSERT INTO `tblLogs` VALUES (1,'authenticate','User logged in.',0,'2020-04-26 14:59:48','2020-04-26 14:59:48',1),(2,'authenticate','User logged in.',0,'2020-04-26 15:00:33','2020-04-26 15:00:33',1),(3,'authenticate','User logged in.',0,'2020-04-26 15:06:42','2020-04-26 15:06:42',1),(4,'authenticate','User logged in.',0,'2020-04-26 15:07:15','2020-04-26 15:07:15',1),(5,'authenticate','User logged in.',0,'2020-04-26 15:21:27','2020-04-26 15:21:27',1),(6,'authenticate','User logged in.',0,'2020-04-26 16:09:30','2020-04-26 16:09:30',1),(7,'authenticate','User logged in.',0,'2020-04-26 17:22:02','2020-04-26 17:22:02',1),(8,'authenticate','User logged in.',0,'2020-04-26 17:38:02','2020-04-26 17:38:02',1),(9,'authenticate','User logged in.',0,'2020-04-26 18:08:38','2020-04-26 18:08:38',2),(10,'authenticate','User logged in.',0,'2020-04-26 19:08:40','2020-04-26 19:08:40',3),(11,'authenticate','User logged in.',0,'2020-04-26 19:09:00','2020-04-26 19:09:00',1),(12,'authenticate','User logged in.',0,'2020-04-26 19:11:09','2020-04-26 19:11:09',2),(13,'authenticate','User logged in.',0,'2020-04-26 19:13:39','2020-04-26 19:13:39',3),(14,'authenticate','User logged in.',0,'2020-04-26 19:32:19','2020-04-26 19:32:19',3),(15,'authenticate','User logged in.',0,'2020-04-26 19:36:17','2020-04-26 19:36:17',2),(16,'authenticate','User logged in.',0,'2020-04-27 15:53:48','2020-04-27 15:53:48',1),(17,'authenticate','User logged in.',0,'2020-04-27 15:54:10','2020-04-27 15:54:10',1),(18,'authenticate','User logged in.',0,'2020-04-27 16:31:12','2020-04-27 16:31:12',1),(19,'authenticate','User logged in.',0,'2020-05-02 10:10:23','2020-05-02 10:10:23',1),(20,'authenticate','User logged in.',0,'2020-05-02 15:38:42','2020-05-02 15:38:42',1),(21,'authenticate','User logged in.',0,'2020-05-02 17:51:11','2020-05-02 17:51:11',1),(22,'authenticate','User logged in.',0,'2020-05-02 18:51:38','2020-05-02 18:51:38',2),(23,'authenticate','User logged in.',0,'2020-05-02 18:52:23','2020-05-02 18:52:23',3),(24,'authenticate','User logged in.',0,'2020-05-02 18:52:45','2020-05-02 18:52:45',3),(25,'authenticate','User logged in.',0,'2020-05-02 18:52:57','2020-05-02 18:52:57',1),(26,'authenticate','User logged in.',0,'2020-05-02 19:02:18','2020-05-02 19:02:18',3),(27,'authenticate','User logged in.',0,'2020-05-02 19:03:02','2020-05-02 19:03:02',2),(28,'authenticate','User logged in.',0,'2020-05-02 19:05:19','2020-05-02 19:05:19',3),(29,'authenticate','User logged in.',0,'2020-05-02 19:08:46','2020-05-02 19:08:46',2),(30,'authenticate','User logged in.',0,'2020-05-02 19:09:29','2020-05-02 19:09:29',3),(31,'authenticate','User logged in.',0,'2020-05-02 19:10:44','2020-05-02 19:10:44',2),(32,'authenticate','User logged in.',0,'2020-05-02 19:10:59','2020-05-02 19:10:59',1),(33,'authenticate','User logged in.',0,'2020-05-02 19:13:20','2020-05-02 19:13:20',2),(34,'authenticate','User logged in.',0,'2020-05-02 19:17:08','2020-05-02 19:17:08',2),(35,'authenticate','User logged in.',0,'2020-05-02 19:17:22','2020-05-02 19:17:22',3),(36,'authenticate','User logged in.',0,'2020-05-02 19:20:10','2020-05-02 19:20:10',1),(37,'authenticate','User logged in.',0,'2020-05-02 19:20:18','2020-05-02 19:20:18',2),(38,'authenticate','User logged in.',0,'2020-05-02 19:21:00','2020-05-02 19:21:00',3),(39,'authenticate','User logged in.',0,'2020-05-02 19:22:06','2020-05-02 19:22:06',2),(40,'authenticate','User logged in.',0,'2020-05-02 19:39:35','2020-05-02 19:39:35',3),(41,'authenticate','User logged in.',0,'2020-05-02 19:56:18','2020-05-02 19:56:18',2),(42,'authenticate','User logged in.',0,'2020-05-02 20:00:36','2020-05-02 20:00:36',3),(43,'authenticate','User logged in.',0,'2020-05-02 20:08:39','2020-05-02 20:08:39',2),(44,'authenticate','User logged in.',0,'2020-06-18 17:05:44','2020-06-18 17:05:44',1),(45,'authenticate','User logged in.',0,'2020-06-18 17:05:56','2020-06-18 17:05:56',3),(46,'authenticate','User logged in.',0,'2020-07-06 15:27:24','2020-07-06 15:27:24',3),(47,'authenticate','User logged in.',0,'2020-07-06 15:33:56','2020-07-06 15:33:56',1),(48,'authenticate','User logged in.',0,'2020-07-06 15:50:43','2020-07-06 15:50:43',2),(49,'authenticate','User logged in.',0,'2020-07-06 15:52:17','2020-07-06 15:52:17',1),(50,'authenticate','User logged in.',0,'2020-07-06 15:54:00','2020-07-06 15:54:00',1),(51,'authenticate','User logged in.',0,'2020-07-06 15:54:21','2020-07-06 15:54:21',6),(52,'authenticate','User logged in.',0,'2020-07-06 17:22:23','2020-07-06 17:22:23',6),(53,'authenticate','User logged in.',0,'2020-07-12 14:47:51','2020-07-12 14:47:51',3),(54,'authenticate','User logged in.',0,'2020-07-12 14:48:23','2020-07-12 14:48:23',3),(55,'authenticate','User logged in.',0,'2020-07-12 14:48:51','2020-07-12 14:48:51',3),(56,'authenticate','User logged in.',0,'2020-07-12 14:49:30','2020-07-12 14:49:30',3),(57,'authenticate','User logged in.',0,'2020-07-12 14:52:53','2020-07-12 14:52:53',3),(58,'authenticate','User logged in.',0,'2020-07-12 14:59:25','2020-07-12 14:59:25',1),(59,'authenticate','User logged in.',0,'2020-07-12 15:00:28','2020-07-12 15:00:28',7),(60,'authenticate','User logged in.',0,'2020-07-12 15:00:44','2020-07-12 15:00:44',1),(61,'authenticate','User logged in.',0,'2020-07-12 15:01:09','2020-07-12 15:01:09',7),(62,'authenticate','User logged in.',0,'2020-08-06 01:58:41','2020-08-06 01:58:41',3),(63,'authenticate','User logged in.',0,'2020-08-06 05:24:01','2020-08-06 05:24:01',3),(64,'authenticate','User logged in.',0,'2020-08-06 08:31:58','2020-08-06 08:31:58',3),(65,'authenticate','User logged in.',0,'2020-08-06 09:02:28','2020-08-06 09:02:28',3),(66,'authenticate','User logged in.',0,'2020-08-06 09:04:20','2020-08-06 09:04:20',3),(67,'authenticate','User logged in.',0,'2020-08-06 09:05:00','2020-08-06 09:05:00',3),(68,'authenticate','User logged in.',0,'2020-08-06 09:05:36','2020-08-06 09:05:36',3),(69,'authenticate','User logged in.',0,'2020-08-06 09:06:58','2020-08-06 09:06:58',3),(70,'authenticate','User logged in.',0,'2020-08-06 09:07:59','2020-08-06 09:07:59',3),(71,'authenticate','User logged in.',0,'2020-08-06 09:08:15','2020-08-06 09:08:15',3),(72,'authenticate','User logged in.',0,'2020-08-09 00:54:36','2020-08-09 00:54:36',3),(73,'authenticate','User logged in.',0,'2020-08-26 09:24:51','2020-08-26 09:24:51',1),(74,'authenticate','User logged in.',0,'2020-08-26 09:25:20','2020-08-26 09:25:20',3),(75,'authenticate','User logged in.',0,'2020-09-30 11:06:30','2020-09-30 11:06:30',3),(76,'authenticate','User logged in.',0,'2020-09-30 11:11:07','2020-09-30 11:11:07',1),(77,'authenticate','User logged in.',0,'2020-10-13 11:41:26','2020-10-13 11:41:26',3),(78,'authenticate','User logged in.',0,'2020-10-13 11:41:45','2020-10-13 11:41:45',1),(79,'authenticate','User logged in.',0,'2020-10-13 11:42:04','2020-10-13 11:42:04',2);
/*!40000 ALTER TABLE `tblLogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblNodes`
--

DROP TABLE IF EXISTS `tblNodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblNodes` (
  `fldNodeId` varchar(250) NOT NULL,
  `fldNodeTitle` varchar(45) DEFAULT NULL,
  `fldNodeMetadata` longtext DEFAULT NULL,
  `fldNodeDeleted` tinyint(4) DEFAULT 0,
  `fldNodeCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldNodeModified` datetime DEFAULT NULL,
  PRIMARY KEY (`fldNodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblNodes`
--

LOCK TABLES `tblNodes` WRITE;
/*!40000 ALTER TABLE `tblNodes` DISABLE KEYS */;
INSERT INTO `tblNodes` VALUES ('15127f62-a465-48bf-ba72-b3b735159f21','Node','<mxGraphModel><root><mxCell id=\"0\" value=\"content\"/><mxCell id=\"1\" parent=\"0\"/><WORD type=\"word\" id=\"550653dd-89b2-4fa0-b6b3-71ec04b43d05\"><mxCell style=\"\" vertex=\"1\" parent=\"1\"><mxGeometry x=\"319.84375\" y=\"70\" width=\"200\" height=\"80\" as=\"geometry\"/></mxCell></WORD><mxCell id=\"2\" value=\"word\" style=\"identifier\" parent=\"550653dd-89b2-4fa0-b6b3-71ec04b43d05\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"3\" value=\"be271756dafca1bb.xlsx\" style=\"image\" parent=\"550653dd-89b2-4fa0-b6b3-71ec04b43d05\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.9\" y=\"0.8\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell><QUIZ type=\"quiz\" id=\"d04a8e97-4ea2-4e55-b641-d3511e57c79e\"><mxCell style=\"\" vertex=\"1\" parent=\"1\"><mxGeometry x=\"319.84375\" y=\"190\" width=\"200\" height=\"80\" as=\"geometry\"/></mxCell></QUIZ><mxCell id=\"4\" value=\"quiz\" style=\"identifier\" parent=\"d04a8e97-4ea2-4e55-b641-d3511e57c79e\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"6\" value=\"d04a8e97-4ea2-4e55-b641-d3511e57c79e\" style=\"quiz\" parent=\"d04a8e97-4ea2-4e55-b641-d3511e57c79e\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.03\" y=\"0.6\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"5\" parent=\"1\" source=\"550653dd-89b2-4fa0-b6b3-71ec04b43d05\" target=\"d04a8e97-4ea2-4e55-b641-d3511e57c79e\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\" style=\"\" vertex=\"1\"><PRESENTATION type=\"presentation\" as=\"value\"/><mxGeometry x=\"319.84375\" y=\"320\" width=\"200\" height=\"80\" as=\"geometry\"/><Array as=\"mxTransient\"><add value=\"id\"/><add value=\"value\"/><add value=\"parent\"/><add value=\"source\"/><add value=\"target\"/><add value=\"children\"/><add value=\"edges\"/></Array><Array as=\"children\"><mxCell id=\"7\" value=\"presentation\" style=\"identifier\" vertex=\"1\" connectable=\"0\" parent=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"9\" value=\"aa34a7d1d60d0019.sql\" style=\"image\" vertex=\"1\" connectable=\"0\" parent=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\"><mxGeometry x=\"0.9\" y=\"0.8\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell></Array><mxCell id=\"1\" parent=\"0\" as=\"parent\"/><Array as=\"edges\"><mxCell id=\"8\" edge=\"1\" parent=\"1\" source=\"d04a8e97-4ea2-4e55-b641-d3511e57c79e\" target=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"11\" edge=\"1\" parent=\"1\" source=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\" target=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell></Array></mxCell><mxCell id=\"7\" value=\"presentation\" style=\"identifier\" vertex=\"1\" connectable=\"0\" parent=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"9\" value=\"aa34a7d1d60d0019.sql\" style=\"image\" vertex=\"1\" connectable=\"0\" parent=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\"><mxGeometry x=\"0.9\" y=\"0.8\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"8\" edge=\"1\" parent=\"1\" source=\"d04a8e97-4ea2-4e55-b641-d3511e57c79e\" target=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\" style=\"\" vertex=\"1\"><FINAL type=\"final\" as=\"value\"/><mxGeometry x=\"319.84375\" y=\"450\" width=\"200\" height=\"80\" as=\"geometry\"/><Array as=\"mxTransient\"><add value=\"id\"/><add value=\"value\"/><add value=\"parent\"/><add value=\"source\"/><add value=\"target\"/><add value=\"children\"/><add value=\"edges\"/></Array><Array as=\"children\"><mxCell id=\"10\" value=\"final\" style=\"identifier\" vertex=\"1\" connectable=\"0\" parent=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"12\" value=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\" style=\"quiz\" vertex=\"1\" connectable=\"0\" parent=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\"><mxGeometry x=\"0.03\" y=\"0.6\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell></Array><mxCell id=\"1\" parent=\"0\" as=\"parent\"/><Array as=\"edges\"><mxCell id=\"11\" edge=\"1\" parent=\"1\" source=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\" target=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell></Array></mxCell><mxCell id=\"10\" value=\"final\" style=\"identifier\" vertex=\"1\" connectable=\"0\" parent=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"12\" value=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\" style=\"quiz\" vertex=\"1\" connectable=\"0\" parent=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\"><mxGeometry x=\"0.03\" y=\"0.6\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"11\" edge=\"1\" parent=\"1\" source=\"9d7dbb8e-2010-431d-bcd6-969e24ab537e\" target=\"af97eab2-169b-4ca2-a83b-69ef3c79395a\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell></root></mxGraphModel>',0,'2020-04-26 19:05:58','2020-04-26 19:07:27'),('b42bd356-aeea-4633-8187-43d6f6012766','Algebra','<mxGraphModel><root><mxCell id=\"0\" value=\"content\"/><mxCell id=\"1\" parent=\"0\"/><WORD type=\"word\" id=\"7ed1fca5-8a14-41a8-a3ad-c1d1e97ae521\"><mxCell style=\"\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"319.84375\" y=\"70\" width=\"200\" height=\"80\" as=\"geometry\"/></mxCell></WORD><mxCell id=\"2\" value=\"word\" style=\"identifier\" parent=\"7ed1fca5-8a14-41a8-a3ad-c1d1e97ae521\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"11\" value=\"35001f84f6eff81c.png\" style=\"image\" parent=\"7ed1fca5-8a14-41a8-a3ad-c1d1e97ae521\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.9\" y=\"0.8\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell><QUIZ type=\"quiz\" id=\"cd63c3a8-0854-4934-9c75-162b94f40547\"><mxCell style=\"\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"319.84375\" y=\"200\" width=\"200\" height=\"80\" as=\"geometry\"/></mxCell></QUIZ><mxCell id=\"3\" value=\"quiz\" style=\"identifier\" parent=\"cd63c3a8-0854-4934-9c75-162b94f40547\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"5\" value=\"cd63c3a8-0854-4934-9c75-162b94f40547\" style=\"quiz\" parent=\"cd63c3a8-0854-4934-9c75-162b94f40547\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.03\" y=\"0.6\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell><FINAL type=\"final\" id=\"e37f1fdb-42b9-403c-829a-669c4d2fd50e\"><mxCell style=\"\" parent=\"1\" vertex=\"1\"><mxGeometry x=\"319.84375\" y=\"330\" width=\"200\" height=\"80\" as=\"geometry\"/></mxCell></FINAL><mxCell id=\"6\" value=\"final\" style=\"identifier\" parent=\"e37f1fdb-42b9-403c-829a-669c4d2fd50e\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.5\" y=\"1\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"9\" value=\"e37f1fdb-42b9-403c-829a-669c4d2fd50e\" style=\"quiz\" parent=\"e37f1fdb-42b9-403c-829a-669c4d2fd50e\" vertex=\"1\" connectable=\"0\"><mxGeometry x=\"0.03\" y=\"0.6\" width=\"30\" height=\"30\" relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"7\" parent=\"1\" source=\"7ed1fca5-8a14-41a8-a3ad-c1d1e97ae521\" target=\"cd63c3a8-0854-4934-9c75-162b94f40547\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell><mxCell id=\"8\" parent=\"1\" source=\"cd63c3a8-0854-4934-9c75-162b94f40547\" target=\"e37f1fdb-42b9-403c-829a-669c4d2fd50e\" edge=\"1\"><mxGeometry relative=\"1\" as=\"geometry\"/></mxCell></root></mxGraphModel>',0,'2020-04-26 19:00:01','2020-05-02 19:59:48');
/*!40000 ALTER TABLE `tblNodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblQuizAnswers`
--

DROP TABLE IF EXISTS `tblQuizAnswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblQuizAnswers` (
  `fldQuizAnswerId` int(11) NOT NULL AUTO_INCREMENT,
  `fldQuizAnswerContent` text DEFAULT NULL,
  `fldQuizAnswerCorrect` tinyint(4) DEFAULT 0,
  `fldQuizAnswerDeleted` tinyint(4) DEFAULT 0,
  `fldQuizAnswerCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldQuizAnswerModified` datetime DEFAULT NULL,
  `tblQuizQuestions_fldQuizQuestionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldQuizAnswerId`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblQuizAnswers`
--

LOCK TABLES `tblQuizAnswers` WRITE;
/*!40000 ALTER TABLE `tblQuizAnswers` DISABLE KEYS */;
INSERT INTO `tblQuizAnswers` VALUES (1,'2',1,0,'2020-04-26 18:57:52','2020-04-26 18:57:52',1),(2,'3',0,0,'2020-04-26 18:57:52','2020-04-26 18:57:52',1),(3,'4',0,0,'2020-04-26 18:57:52','2020-04-26 18:57:52',1),(4,'5',0,0,'2020-04-26 18:57:52','2020-04-26 18:57:52',1),(5,'Subtraction',0,0,'2020-04-26 18:57:52','2020-04-26 18:57:52',2),(6,'Addition',1,0,'2020-04-26 18:57:52','2020-04-26 18:57:52',2),(7,'Multiplication',0,0,'2020-04-26 18:57:52','2020-04-26 18:57:52',2),(8,'Division',0,0,'2020-04-26 18:57:52','2020-04-26 18:57:52',2),(9,'2',1,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',3),(10,'3',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',3),(11,'4',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',3),(12,'5',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',3),(13,'Subtraction',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',4),(14,'Addition',1,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',4),(15,'Multiplication',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',4),(16,'Division',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',4),(17,'Subtraction',1,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',5),(18,'Addition',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',5),(19,'Multiplication',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',5),(20,'Division',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',5),(21,'Subtraction',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',6),(22,'Addition',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',6),(23,'Multiplication',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',6),(24,'Division',1,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',6),(25,'Subtraction',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',7),(26,'Addition',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',7),(27,'Multiplication',1,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',7),(28,'Division',0,0,'2020-04-26 18:59:35','2020-04-26 18:59:35',7),(29,'2',1,0,'2020-04-26 19:04:49','2020-04-26 19:04:49',8),(30,'3',0,0,'2020-04-26 19:04:49','2020-04-26 19:04:49',8),(31,'4',0,0,'2020-04-26 19:04:49','2020-04-26 19:04:49',8),(32,'5',0,0,'2020-04-26 19:04:49','2020-04-26 19:04:49',8),(33,'2',0,0,'2020-04-26 19:07:20','2020-04-26 19:07:20',9),(34,'1',1,0,'2020-04-26 19:07:20','2020-04-26 19:07:20',9),(35,'3',0,0,'2020-04-26 19:07:20','2020-04-26 19:07:20',9),(36,'4',0,0,'2020-04-26 19:07:20','2020-04-26 19:07:20',9);
/*!40000 ALTER TABLE `tblQuizAnswers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblQuizQuestions`
--

DROP TABLE IF EXISTS `tblQuizQuestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblQuizQuestions` (
  `fldQuizQuestionId` int(11) NOT NULL AUTO_INCREMENT,
  `fldQuizQuestionContent` text DEFAULT NULL,
  `fldQuizQuestionDeleted` tinyint(4) DEFAULT 0,
  `fldQuizQuestionCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldQuizQuestionModified` datetime DEFAULT NULL,
  `tblContents_fldContentId` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`fldQuizQuestionId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblQuizQuestions`
--

LOCK TABLES `tblQuizQuestions` WRITE;
/*!40000 ALTER TABLE `tblQuizQuestions` DISABLE KEYS */;
INSERT INTO `tblQuizQuestions` VALUES (1,'Compute for the answer: \n1+1 = ?',0,'2020-04-26 18:57:52','2020-04-26 18:57:52','cd63c3a8-0854-4934-9c75-162b94f40547'),(2,'The act of adding number?',0,'2020-04-26 18:57:52','2020-04-26 18:57:52','cd63c3a8-0854-4934-9c75-162b94f40547'),(3,'Compute for the answer: \n1+1 = ?',0,'2020-04-26 18:59:35','2020-04-26 18:59:35','e37f1fdb-42b9-403c-829a-669c4d2fd50e'),(4,'The act of adding numbers?',0,'2020-04-26 18:59:35','2020-04-26 18:59:35','e37f1fdb-42b9-403c-829a-669c4d2fd50e'),(5,'The act of subtracting numbers?',0,'2020-04-26 18:59:35','2020-04-26 18:59:35','e37f1fdb-42b9-403c-829a-669c4d2fd50e'),(6,'The act of dividing numbers?',0,'2020-04-26 18:59:35','2020-04-26 18:59:35','e37f1fdb-42b9-403c-829a-669c4d2fd50e'),(7,'The act of multiplying numbers?',0,'2020-04-26 18:59:35','2020-04-26 18:59:35','e37f1fdb-42b9-403c-829a-669c4d2fd50e'),(8,'Value of x:\n1 + x = 3',0,'2020-04-26 19:04:49','2020-04-26 19:04:49','d04a8e97-4ea2-4e55-b641-d3511e57c79e'),(9,'What is the x:\nx + 2 = 3',0,'2020-04-26 19:07:20','2020-04-26 19:07:20','af97eab2-169b-4ca2-a83b-69ef3c79395a');
/*!40000 ALTER TABLE `tblQuizQuestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblQuizResults`
--

DROP TABLE IF EXISTS `tblQuizResults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblQuizResults` (
  `fldQuizResultId` int(11) NOT NULL AUTO_INCREMENT,
  `fldQuizResultScore` varchar(45) DEFAULT NULL,
  `fldQuizResultPercentage` varchar(45) DEFAULT NULL,
  `fldQuizResultReference` varchar(45) DEFAULT NULL,
  `fldQuizResultDeleted` tinyint(4) DEFAULT 0,
  `fldQuizResultCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldQuizResultModified` datetime DEFAULT NULL,
  `tblStudents_fldStudentId` int(11) DEFAULT NULL,
  `tblContents_fldContentId` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`fldQuizResultId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblQuizResults`
--

LOCK TABLES `tblQuizResults` WRITE;
/*!40000 ALTER TABLE `tblQuizResults` DISABLE KEYS */;
INSERT INTO `tblQuizResults` VALUES (1,'0','0','096f579f278a8bca.png',0,'2020-04-26 19:30:52','2020-04-26 19:30:52',1,'cd63c3a8-0854-4934-9c75-162b94f40547'),(2,'2','100','b506833694381167.png',0,'2020-04-26 19:31:24','2020-04-26 19:31:24',1,'cd63c3a8-0854-4934-9c75-162b94f40547');
/*!40000 ALTER TABLE `tblQuizResults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblRoles`
--

DROP TABLE IF EXISTS `tblRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblRoles` (
  `fldRoleId` int(11) NOT NULL AUTO_INCREMENT,
  `fldRoleTitle` varchar(45) DEFAULT NULL,
  `fldRoleCreated` timestamp NULL DEFAULT NULL,
  `fldRoleModified` datetime DEFAULT NULL,
  `fldRoleDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`fldRoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblRoles`
--

LOCK TABLES `tblRoles` WRITE;
/*!40000 ALTER TABLE `tblRoles` DISABLE KEYS */;
INSERT INTO `tblRoles` VALUES (1,'Administrator','2020-02-06 22:57:00','2020-02-06 22:57:00',0),(2,'Teachers','2020-02-06 22:57:00','2020-02-06 22:57:00',0),(3,'Students','2020-02-06 22:57:00','2020-02-06 22:57:00',0);
/*!40000 ALTER TABLE `tblRoles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblSemesters`
--

DROP TABLE IF EXISTS `tblSemesters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblSemesters` (
  `fldSemesterId` int(11) NOT NULL AUTO_INCREMENT,
  `fldSemesterTitle` varchar(45) DEFAULT NULL,
  `fldSemesterActive` tinyint(4) DEFAULT 0,
  `fldSemesterDeleted` tinyint(4) DEFAULT 0,
  `fldSemesterCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldSemesterModified` datetime DEFAULT NULL,
  PRIMARY KEY (`fldSemesterId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblSemesters`
--

LOCK TABLES `tblSemesters` WRITE;
/*!40000 ALTER TABLE `tblSemesters` DISABLE KEYS */;
INSERT INTO `tblSemesters` VALUES (1,'First Semester 2019 - 2020',0,1,'2020-04-26 16:51:01','2020-04-26 17:06:59'),(2,'2019-2020 First Semester',1,0,'2020-04-26 17:07:22','2020-04-26 17:07:22');
/*!40000 ALTER TABLE `tblSemesters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblStudents`
--

DROP TABLE IF EXISTS `tblStudents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblStudents` (
  `fldStudentId` int(11) NOT NULL AUTO_INCREMENT,
  `fldStudentFirstname` varchar(45) DEFAULT NULL,
  `fldStudentMiddlename` varchar(45) DEFAULT NULL,
  `fldStudentLastname` varchar(45) DEFAULT NULL,
  `fldStudentMobile` varchar(45) DEFAULT NULL,
  `fldStudentAddress` varchar(300) DEFAULT NULL,
  `fldStudentBirthday` varchar(45) DEFAULT NULL,
  `fldStudentAssessed` tinyint(4) DEFAULT 0,
  `fldStudentDeleted` tinyint(4) DEFAULT 0,
  `fldStudentCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldStudentModified` datetime DEFAULT NULL,
  `tblUsers_fldUserId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fldStudentId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblStudents`
--

LOCK TABLES `tblStudents` WRITE;
/*!40000 ALTER TABLE `tblStudents` DISABLE KEYS */;
INSERT INTO `tblStudents` VALUES (1,'Student','Dummy','Data','123123',NULL,NULL,1,0,'2020-04-26 18:05:42','2020-04-26 19:15:01','3'),(2,'Student','Student','Student','123123123',NULL,NULL,0,0,'2020-07-06 15:53:42','2020-07-06 15:53:41','6'),(3,'Stu','Stu','Stu','123123',NULL,NULL,0,0,'2020-07-12 15:00:20','2020-07-12 15:00:19','7');
/*!40000 ALTER TABLE `tblStudents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblStudentsContents`
--

DROP TABLE IF EXISTS `tblStudentsContents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblStudentsContents` (
  `fldStudentContentId` int(11) NOT NULL AUTO_INCREMENT,
  `fldStudentContentDeleted` tinyint(4) DEFAULT 0,
  `fldStudentContentCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldStudentContentModified` datetime DEFAULT NULL,
  `tblStudents_fldStudentId` int(11) DEFAULT NULL,
  `tblContents_fldContentId` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`fldStudentContentId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblStudentsContents`
--

LOCK TABLES `tblStudentsContents` WRITE;
/*!40000 ALTER TABLE `tblStudentsContents` DISABLE KEYS */;
INSERT INTO `tblStudentsContents` VALUES (1,0,'2020-04-26 19:30:29','2020-05-02 19:21:31',1,'7ed1fca5-8a14-41a8-a3ad-c1d1e97ae521'),(2,0,'2020-04-26 19:31:24','2020-04-26 19:31:24',1,'cd63c3a8-0854-4934-9c75-162b94f40547');
/*!40000 ALTER TABLE `tblStudentsContents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblStudentsCourses`
--

DROP TABLE IF EXISTS `tblStudentsCourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblStudentsCourses` (
  `fldStudentCourseId` int(11) NOT NULL AUTO_INCREMENT,
  `fldStudentCourseDeleted` tinyint(4) DEFAULT 0,
  `fldStudentCourseCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldStudentCourseModified` datetime DEFAULT NULL,
  `tblStudents_fldStudentId` int(11) DEFAULT NULL,
  `tblTeachersCourses_fldTeacherCourseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldStudentCourseId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblStudentsCourses`
--

LOCK TABLES `tblStudentsCourses` WRITE;
/*!40000 ALTER TABLE `tblStudentsCourses` DISABLE KEYS */;
INSERT INTO `tblStudentsCourses` VALUES (1,0,'2020-04-26 17:40:57','2020-04-26 17:40:57',NULL,NULL),(2,0,'2020-04-26 18:08:18','2020-05-02 19:02:11',1,3),(3,0,'2020-07-06 15:54:11','2020-07-06 15:54:11',2,4),(4,0,'2020-07-12 15:00:56','2020-07-12 15:00:59',3,3);
/*!40000 ALTER TABLE `tblStudentsCourses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblStudentsNodes`
--

DROP TABLE IF EXISTS `tblStudentsNodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblStudentsNodes` (
  `fldStudentNodeId` int(11) NOT NULL AUTO_INCREMENT,
  `fldStudentNodeDeleted` tinyint(4) DEFAULT 0,
  `fldStudentNodeCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldStudentNodeModified` datetime DEFAULT NULL,
  `tblStudents_fldStudentId` int(11) DEFAULT NULL,
  `tblNodes_fldNodeId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fldStudentNodeId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblStudentsNodes`
--

LOCK TABLES `tblStudentsNodes` WRITE;
/*!40000 ALTER TABLE `tblStudentsNodes` DISABLE KEYS */;
INSERT INTO `tblStudentsNodes` VALUES (1,0,'2020-04-26 19:30:29','2020-05-02 19:21:31',1,'b42bd356-aeea-4633-8187-43d6f6012766');
/*!40000 ALTER TABLE `tblStudentsNodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTeachers`
--

DROP TABLE IF EXISTS `tblTeachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblTeachers` (
  `fldTeacherId` int(11) NOT NULL AUTO_INCREMENT,
  `fldTeacherFirstname` varchar(45) DEFAULT NULL,
  `fldTeacherLastname` varchar(45) DEFAULT NULL,
  `fldTeacherMobile` varchar(45) DEFAULT NULL,
  `fldTeacherDeleted` tinyint(4) DEFAULT 0,
  `fldTeacherCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldTeacherModified` datetime DEFAULT NULL,
  `tblUsers_fldUserId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fldTeacherId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTeachers`
--

LOCK TABLES `tblTeachers` WRITE;
/*!40000 ALTER TABLE `tblTeachers` DISABLE KEYS */;
INSERT INTO `tblTeachers` VALUES (1,'Dummy','Teacher','123123123',0,'2020-04-26 17:54:32','2020-04-26 17:54:31','2'),(2,'Sample','Teacher','9123123123',0,'2020-05-02 18:39:18','2020-05-02 18:39:18','5');
/*!40000 ALTER TABLE `tblTeachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTeachersCourses`
--

DROP TABLE IF EXISTS `tblTeachersCourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblTeachersCourses` (
  `fldTeacherCourseId` int(11) NOT NULL AUTO_INCREMENT,
  `fldTeacherCourseDeleted` tinyint(4) DEFAULT 0,
  `fldTeacherCourseCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldTeacherCourseModified` datetime DEFAULT NULL,
  `tblTeachers_fldTeacherId` int(11) DEFAULT NULL,
  `tblCourses_fldCourseId` int(11) DEFAULT NULL,
  `tblSemesters_fldSemesterId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fldTeacherCourseId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTeachersCourses`
--

LOCK TABLES `tblTeachersCourses` WRITE;
/*!40000 ALTER TABLE `tblTeachersCourses` DISABLE KEYS */;
INSERT INTO `tblTeachersCourses` VALUES (1,1,'2020-04-26 18:07:46','2020-05-02 18:51:11',1,4,'2'),(2,1,'2020-05-02 18:38:44','2020-05-02 18:51:11',1,4,'2'),(3,0,'2020-05-02 18:49:17','2020-05-02 18:51:11',1,4,'2'),(4,0,'2020-05-02 18:50:15','2020-05-02 18:50:15',2,3,'2');
/*!40000 ALTER TABLE `tblTeachersCourses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblUsers`
--

DROP TABLE IF EXISTS `tblUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblUsers` (
  `fldUserId` int(11) NOT NULL AUTO_INCREMENT,
  `fldUserUsername` varchar(45) DEFAULT NULL,
  `fldUserPassword` text DEFAULT NULL,
  `fldUserActive` tinyint(4) DEFAULT NULL,
  `fldUserCreated` timestamp NULL DEFAULT current_timestamp(),
  `fldUserModified` datetime DEFAULT NULL,
  `fldUserDeleted` tinyint(4) DEFAULT 0,
  `tblRoles_fldRoleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`fldUserId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblUsers`
--

LOCK TABLES `tblUsers` WRITE;
/*!40000 ALTER TABLE `tblUsers` DISABLE KEYS */;
INSERT INTO `tblUsers` VALUES (1,'admin','$2a$12$ypcl1RwEmUG5lal9dmaBnO0P78P.qrH2ZaOTpcCxy3kt7ybEujhYW',1,'2020-02-06 15:22:37','2020-02-06 15:22:37',0,1),(2,'teacher1','$2a$12$ngMSaTGw0Wg0JavVRxlYkuOvFlRPgl1DN7aAfB/SY/wacMEutNm5.',1,'2020-04-26 17:54:32','2020-04-26 17:54:31',0,2),(3,'student1','$2a$12$Da.WNAC0PitB78lJkeOrJ.LR3KoBoR9q1kk6piZR0DPSTtxhIfiLW',1,'2020-04-26 18:05:42','2020-04-26 18:05:41',0,3),(4,'sample','$2a$12$zCXIWKyEvuMgrn27N6cf7uL9nhBWVM.akFkzT7oAyZuBCwGI8QQya',0,'2020-05-02 17:03:04','2020-05-02 17:03:29',1,1),(5,'teacher2','$2a$12$6hDUbg/wh84qd6ILLngijOUXUIoZiol3r53d6s4eHY.mWF7YC7Z0K',1,'2020-05-02 18:39:18','2020-05-02 18:39:18',0,2),(6,'student3','$2a$12$//rIrpu8CUmaXypdFd8FHuxehdSma0XimtxlVa5LCH8NTFZpfA8Aq',1,'2020-07-06 15:53:42','2020-07-06 15:53:41',0,3),(7,'student4','$2a$12$DVdhDVncgNCEaGIC3WQr2.sRKgo4JCrAAkBC5wcEUK41ovst.H7WG',1,'2020-07-12 15:00:20','2020-07-12 15:00:19',0,3);
/*!40000 ALTER TABLE `tblUsers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-13 20:13:53
