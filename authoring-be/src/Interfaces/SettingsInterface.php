<?php

namespace App\Interfaces;

interface SettingsInterface extends \ArrayAccess {
    public function setArray(array $values);
    /**
     * As we can't extend multiple interfaces, we need to mimic \IteratorAggregate
     * @return \ArrayIterator
     */
    public function getIterator();
}