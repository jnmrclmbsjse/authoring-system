<?php

namespace App\Domain\Assessments;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Courses\CoursesRepository;
use App\Domain\Semesters\SemestersRepository;
use App\Domain\Assessments\AssessmentsRepository;
use App\Domain\AssessmentsAnswers\AssessmentsAnswersRepository;
use App\Domain\AssessmentsQuestions\AssessmentsQuestionsRepository;

/**
 * Service.
 */
final class AssessmentsService
{
    private $repository;
    private $semesterRepository;
    private $answerRepository;
    private $questionRepository;
    private $courseRepository;

    /**
     * The constructor.
     *
     */
    public function __construct(AssessmentsRepository $repository, SemestersRepository $semesterRepository, AssessmentsAnswersRepository $answerRepository, AssessmentsQuestionsRepository $questionRepository, CoursesRepository $courseRepository)
    {
        $this->repository = $repository;
        $this->answerRepository = $answerRepository;
        $this->semesterRepository = $semesterRepository;
        $this->questionRepository = $questionRepository;
        $this->courseRepository = $courseRepository;
    }

    /**
     * Create a new quiz.
     *
     * @return int The content id
     */
    public function create($request, $teacherId): string
    {
        $assessment = [
            'description' => isset($request['description']) ? $request['description'] : 'Sample description'
        ];

        $semesterId = $this->semesterRepository->findActiveSemesterId();

        $course = $this->courseRepository->findCourseByTeacherAndSemester($semesterId, $teacherId);
        $teacherCourseId = $course['teacherCourseId'];

        $assessmentId = $this->repository->insert($assessment, $teacherCourseId);

        $items = $request['items'];
        // Insert question and answers
        foreach ($items as $item) {
            $question = $item['topic'];
            $questionId = $this->questionRepository->insert($question, $assessmentId);

            $answers = $item['answers'];
            foreach ($answers as $answer) {
                $answerObject = [];
                $answerObject['content'] = $answer;
                $answerObject['correct'] = 0;
                if (strpos($answer, '(c)') !== false) {
                    $answerObject['content'] = str_replace('(c)', '', $answer);
                    $answerObject['correct'] = 1;
                }
                $answerId = $this->answerRepository->insert($answerObject, $questionId);
            }
        }
        // Logging here: User created successfully

        return $assessmentId;
    }

    /**
     * Get assessment details.
     *
     * @param int $id The assessment ID
     *
     * @return array Assessment details
     */
    public function details($id)
    {
        if (empty($id)) {
            throw new UnexpectedValueException('Missing id parameter');
        }

        $assessment = $this->repository->findAssessmentDetailsById($id);
        if (!$assessment) {
            return null;
        }
        $assessment['items'] = [];
        $questions = $this->questionRepository->findQuestionsByAssessmentId($assessment['id']);
        foreach ($questions as $question) {
            $answer = $this->answerRepository->findAnswersByQuestionId($question['id']);
            $builder = [
                'topic' => $question,
                'answers' => $answer
            ];
            array_push($assessment['items'], $builder);
        }

        return $assessment;
    }

    /**
     * Compute score and pecentage.
     *
     * @param array $answers the answer array
     * @param int $teacherCourseId the teacherCourseId
     * @param int $studentId the studentId
     *
     * @return array exam result
     */
    public function evaluate($answers, $teacherCourseId, $studentId): array
    {
        $score = 0;
        $total = count($answers);
        foreach($answers as $questionId => $answerId) {
            $correct = $this->answerRepository->findCorrectAnswerIdByQuestionId($questionId);

            if ($correct == $answerId) {
                $score++;
            }
        }
        $percentage = ($score / $total) * 100;
        $percentage = round($percentage);

        $resultId = $this->repository->insertAssessmentsResults($score, $percentage, $teacherCourseId, $studentId);

        $result = [
            "resultId" => $resultId, 
            "score" => $score,
            "percentage" => $percentage
        ];

        return $result;
    }

    /**
     * Get assessment results.
     *
     * @param int $teacherCourseId The teacher course ID
     * @param int $studentId The student ID
     *
     * @return array assessment results
     */
    public function getResults($teacherCourseId, $studentId)
    {
        $results = $this->repository->findAssessmentResultByCourseAndStudentId($teacherCourseId, $studentId);

        return $results;
    }
}