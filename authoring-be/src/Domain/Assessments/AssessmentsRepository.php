<?php

namespace App\Domain\Assessments;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class AssessmentsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert answers row.
     *
     * @param $answer The answer
     * @param $questionId The questionId
     *
     * @return int The new ID
     */
    public function insert($assessment, $teacherCourseId): string
    {
        $now = new \DateTime();
        $row = [
            'fldAssessmentDescription' => $assessment['description'],
            'fldAssessmentModified' => $now->format('Y-m-d H:i:s'),
            'tblTeachersCourses_fldTeacherCourseId' => $teacherCourseId
        ];

        $this->connection->insert('tblAssessments', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }
    
    public function findAssessmentDetailsById($teacherCourseId)
    {
        $sql = 'SELECT tblAssessments.fldAssessmentId AS id, tblAssessments.fldAssessmentDescription AS description FROM tblAssessments WHERE tblAssessments.tblTeachersCourses_fldTeacherCourseId = ? AND tblAssessments.fldAssessmentDeleted = 0';

        $assessment = $this->connection->row($sql, $teacherCourseId);

        return $assessment;
    }

    public function findAssessmentResultByCourseAndStudentId($teacherCourseId, $studentId) {
       $sql = 'SELECT tblAssessmentsResults.fldAssessmentResultScore AS score, tblAssessmentsResults.fldAssessmentResultPercentage AS percentage, tblAssessmentsResults.fldAssessmentResultCreated AS date FROM tblAssessmentsResults WHERE tblAssessmentsResults.tblTeachersCourses_fldTeacherCourseId = ? AND tblAssessmentsResults.tblStudents_fldStudentId = ?';
       
       $results = $this->connection->row($sql, $teacherCourseId, $studentId);

       return $results;
    }

    public function insertAssessmentsResults($score, $percentage, $teacherCourseId, $studentId): int
    {
        $now = new \DateTime();
        $row = [
            'fldAssessmentResultScore' => $score,
            'fldAssessmentResultPercentage' => $percentage,
            'fldAssessmentResultModified' => $now->format('Y-m-d H:i:s'),
            'tblTeachersCourses_fldTeacherCourseId' => $teacherCourseId,
            'tblStudents_fldStudentId' => $studentId
        ];

        $this->connection->insert('tblAssessmentsResults', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }
}