<?php

namespace App\Domain\Users;

use ParagonIE\EasyDB\EasyDB;
use App\Domain\Users\UsersObject;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class UsersRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert user row.
     *
     * @param UsersObject $user The user
     *
     * @return int The new ID
     */
    public function insert(UsersObject $user, $details): int
    {
        $now = new \DateTime();
        $row = [
            'fldUserUsername' => $user->username,
            'fldUserPassword' => $this->context->hash($user->password),
            'fldUserActive' => 1,
            'fldUserModified' => $now->format('Y-m-d H:i:s'),
            'tblRoles_fldRoleId' => $user->role,
        ];

        $this->connection->insert('tblUsers', $row);

        $id = (int)$this->connection->lastInsertId();

        if ($user->role === "2") {
            $row = [
                'fldTeacherFirstname' => $details['firstname'],
                'fldTeacherLastname' => $details['lastname'],
                'fldTeacherMobile' => $details['mobile'],
                'fldTeacherModified' => $now->format('Y-m-d H:i:s'),
                'tblUsers_fldUserId' => $id,
            ];
            $this->connection->insert('tblTeachers', $row);
        }

        if ($user->role === "3") {
            $row = [
                'fldStudentFirstname' => $details['firstname'],
                'fldStudentMiddlename' => $details['middlename'],
                'fldStudentLastname' => $details['lastname'],
                'fldStudentMobile' => $details['mobile'],
                'fldStudentAddress' => $details['address'],
                'fldStudentBirthday' => $details['birthday'],
                'fldStudentModified' => $now->format('Y-m-d H:i:s'),
                'tblUsers_fldUserId' => $id,
            ];
            $this->connection->insert('tblStudents', $row);
        }

        return $id;
    }

    /**
     * Check if user exist
     *
     * @param UsersObject $user The user
     *
     * @return int The new ID
     */
    public function authenticate(UsersObject $user): int
    {

        $sql = "SELECT fldUserId, fldUserPassword FROM tblUsers WHERE fldUserUsername = ?
                AND fldUserDeleted = 0 AND fldUserActive = 1";

        try {
            $account = $this->connection->row($sql, $user->username);
            if ($this->context->verify($user->password, $account['fldUserPassword'])) {
                return $account['fldUserId'];
            } else {
                return false;
            }    
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Find user using ID
     *
     * @param int $id
     *
     * @return array User details
     */
    public function findUserDetailsById($id): array
    {

        $sql = "SELECT tblUsers.fldUserUsername AS username, tblUsers.fldUserCreated AS created, tblRoles.fldRoleTitle AS role FROM tblUsers LEFT JOIN tblRoles ON tblUsers.tblRoles_fldRoleId = tblRoles.fldRoleId WHERE tblUsers.fldUserId = ? AND tblUsers.fldUserDeleted != 1;";

        try {
            $account = $this->connection->row($sql, $id);
            $sql = null;
            $details = $account;

            if ($account['role'] === 'Teachers') {
                $sql = "SELECT tblTeachers.fldTeacherId AS teacherId, tblTeachers.fldTeacherFirstname AS firstname, tblTeachers.fldTeacherLastname AS lastname, tblTeachers.fldTeacherMobile AS mobile, tblCourses.fldCourseId AS courseId, tblCourses.fldCourseTitle AS courseTitle FROM tblTeachers LEFT JOIN tblTeachersCourses ON tblTeachersCourses.tblTeachers_fldTeacherId = tblTeachers.fldTeacherId AND tblTeachersCourses.fldTeacherCourseDeleted != 1 LEFT JOIN tblCourses ON tblCourses.fldCourseId = tblTeachersCourses.tblCourses_fldCourseId WHERE tblTeachers.tblUsers_fldUserId = ?;";
            }

            if ($account['role'] === 'Students') {
                $sql = "SELECT tblStudents.fldStudentId AS studentId, tblStudents.fldStudentFirstname AS firstname, tblStudents.fldStudentMiddlename AS middlename, tblStudents.fldStudentLastname AS lastname, tblStudents.fldStudentMobile AS mobile, tblStudents.fldStudentAddress AS address, tblStudents.fldStudentBirthday AS birthday, tblTeachersCourses.fldTeacherCourseId AS teacherCourseId, tblCourses.fldCourseTitle AS courseTitle, tblStudents.fldStudentAssessed AS assessed FROM tblStudents LEFT JOIN tblStudentsCourses ON tblStudentsCourses.tblStudents_fldStudentId = tblStudents.fldStudentId AND tblStudentsCourses.fldStudentCourseDeleted != 1 LEFT JOIN tblTeachersCourses ON tblTeachersCourses.fldTeacherCourseId = tblStudentsCourses.tblTeachersCourses_fldTeacherCourseId LEFT JOIN tblCourses ON tblCourses.fldCourseId = tblTeachersCourses.tblCourses_fldCourseId WHERE tblStudents.tblUsers_fldUserId = ?;";
            }

            if ($sql) {
                $information = $this->connection->row($sql, $id);
                $details['info'] = $information;
            }

            if ($details) {
                return $details;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Find all users
     *
     * @return array Users list
     */
    public function findAll(): array
    {

        $sql = "SELECT tblUsers.fldUserId AS id, tblUsers.fldUserUsername AS username, tblRoles.fldRoleTitle AS role, tblUsers.fldUserCreated AS created FROM tblUsers LEFT JOIN tblRoles ON tblUsers.tblRoles_fldRoleId = tblRoles.fldRoleId WHERE tblUsers.fldUserDeleted != 1 ORDER BY tblUsers.fldUserId DESC";

        try {
            $users = $this->connection->run($sql);
            if ($users) {
                return $users;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    public function updateStudentAssessed($studentId, $assessed): bool {
        $now = new \DateTime();

        $this->connection->update('tblStudents', $row = [
            'fldStudentAssessed' => $assessed,
            'fldStudentModified' => $now->format('Y-m-d H:i:s'),
        ], [
            'fldStudentId' => $studentId
        ]);

        return true;
    }

    public function deleteUser($semesterId)
    {
        $now = new \DateTime();
        $row = [
            'fldUserActive' => 0,
            'fldUserDeleted' => 1,
            'fldUserModified' => $now->format('Y-m-d H:i:s'),
        ];

        $this->connection->update('tblUsers', $row, [
            'fldUserId' => $semesterId
        ]);

        return 1;
    }
}