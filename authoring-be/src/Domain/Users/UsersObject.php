<?php

namespace App\Domain\Users;

final class UsersObject
{
    /** @var string */
    public $username;

    /** @var string */
    public $password;

    /** @var string */
    public $role = null;
}