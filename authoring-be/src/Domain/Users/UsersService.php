<?php

namespace App\Domain\Users;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Users\UsersObject;
use App\Domain\Logs\LogsRepository;
use App\Domain\Users\UsersRepository;
use App\Domain\Users\UsersNotFoundException;

/**
 * Service.
 */
final class UsersService
{
    /**
     * @var UsersRepository
     */
    private $repository;
    private $logsRepository;

    /**
     * The constructor.
     *
     */
    public function __construct(UsersRepository $repository, LogsRepository $logsRepository)
    {
        $this->repository = $repository;
        $this->logsRepository = $logsRepository;
    }

    /**
     * Create a new user.
     *
     * @param UsersObject $user The user data
     *
     * @return int The new user ID
     */
    public function create(UsersObject $user, $details): int
    {
        // Validation
        if (empty($user->username)) {
            throw new UnexpectedValueException('Username required');
        }

        // Insert user
        $userId = $this->repository->insert($user, $details);

        // Logging here: User created successfully

        return $userId;
    }

    /**
     * Authenticate user.
     *
     * @param UsersObject $user The user data
     *
     * @return int The new user ID
     */
    public function authenticate(UsersObject $user): int
    {
        // Validation
        if (empty($user->username)) {
            throw new UnexpectedValueException('Username is required');
        }

        if (empty($user->password)) {
            throw new UnexpectedValueException('Password is required');
        }

        $userId = $this->repository->authenticate($user);

        if (!$userId) {
            throw new UsersNotFoundException;
        }

        $this->logsRepository->insert($userId, "authenticate", "User logged in.");

        return $userId;
    }

    /**
     * Get user details.
     *
     * @param int $id The user ID
     *
     * @return array User details
     */
    public function details($id): array
    {
        if (empty($id)) {
            throw new UnexpectedValueException('Missing id parameter');
        }

        $details = $this->repository->findUserDetailsById($id);

        if (!$details) {
            throw new UsersNotFoundException;
        }

        return $details;
    }

    /**
     * Get all users.
     *
     * @return array Users list
     */
    public function list(): array
    {
        $users = $this->repository->findAll();

        if (!$users) {
            throw new UsersNotFoundException;
        }

        return $users;
    }

    /**
     * Delete user.
     *
     * @return int success
     */
    public function delete($userId): int
    {
        $success = $this->repository->deleteUser($userId);

        return $success;
    }
}