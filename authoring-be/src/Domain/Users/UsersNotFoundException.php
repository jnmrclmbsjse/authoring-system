<?php
declare(strict_types=1);

namespace App\Domain\Users;

use App\Domain\DomainException\DomainRecordNotFoundException;

class UsersNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The user you requested does not exist.';
}
