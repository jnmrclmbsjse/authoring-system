<?php

namespace App\Domain\Contents;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Contents\ContentsObject;
use App\Domain\Contents\ContentsRepository;

/**
 * Service.
 */
final class ContentsService
{
    /**
     * @var ContentsRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     */
    public function __construct(ContentsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get content details.
     *
     * @param int $id The user ID
     *
     * @return array User details
     */
    public function details($id, $studentId): array
    {
        if (empty($id)) {
            throw new UnexpectedValueException('Missing id parameter');
        }

        $details = $this->repository->findContentDetailsById($id);
        $this->seenContent($studentId, $id);
        if (!$details) {
            throw new UsersNotFoundException;
        }

        return $details;
    }

    /**
     * Get all contents.
     *
     * @return array Contents list
     */
    public function list($teacherId): array
    {
        $contents = $this->repository->findAllForTeacher($teacherId);
        return $contents;
    }

    /**
     * Check if content is viewed.
     *
     * @return array Content id
     */
    public function viewed($studentId, $contentId): int
    {
        $id = $this->repository->findViewedContent($studentId, $contentId);
        return $id;
    }

    private function seenContent($studentId, $contentId) {
        $this->repository->insertViewedContent($studentId, $contentId);
    }
}