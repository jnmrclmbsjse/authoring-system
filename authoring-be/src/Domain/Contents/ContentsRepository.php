<?php

namespace App\Domain\Contents;

use ParagonIE\EasyDB\EasyDB;
use App\Domain\Contents\ContentsObject;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class ContentsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert user row.
     *
     * @param ContentsObject $user The user
     *
     * @return int The new ID
     */
    public function insert($content): string
    {
        $now = new \DateTime();
        $row = [
            'fldContentId' => $content['id'],
            'fldContentTitle' => $content['title'],
            'fldContentType' => $content['type'],
            'fldContentFile' => $content['filename'] ? $content['filename'] : null,
            'fldContentModified' => $now->format('Y-m-d H:i:s'),
            'tblNodes_fldNodeId' => $content['nodeId']
        ];

        $exist = $this->connection->cell('SELECT fldContentId AS id FROM tblContents WHERE fldContentDeleted != 1 AND fldContentId = ?', $content['id']);

        if ($exist) {
            $this->connection->update('tblContents', $row, [
                'fldContentId' => $content['id']
            ]);
            return (string)$exist;
        }

        $this->connection->insert('tblContents', $row);

        $id = (string)$content['id'];

        return $id;
    }

    /**
     * Find content using node ID
     *
     * @param int $id
     *
     * @return array Contents
     */
    public function findContentsByNodeId($nodeId): array
    {

        $sql = "SELECT tblContents.fldContentTitle AS title, tblContents.fldContentType AS type, tblContents.fldContentFile AS filename FROM tblContents WHERE tblContents.tblNodes_fldNodeId = ?;";

        try {
            $contents = $this->connection->run($sql, $nodeId);

            if ($contents) {
                return $contents;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Find concept using ID
     *
     * @param int $id
     *
     * @return array Content details
     */
    public function findContentDetailsById($id): array
    {

        $sql = "SELECT tblContents.fldContentTitle AS title, tblContents.fldContentType AS type, tblContents.fldContentFile AS file, tblContents.tblNodes_fldNodeId AS nodeId FROM tblContents WHERE tblContents.fldContentId = ?;";

        try {
            $details = $this->connection->row($sql, $id);

            if ($details) {
                return $details;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Find concept using ID
     *
     * @param int $id
     *
     * @return array Content details
     */
    public function insertViewedContent($studentId, $contentId): int
    {
        $now = new \DateTime();
        $row = [
            'tblContents_fldContentId' => $contentId,
            'tblStudents_fldStudentId' => $studentId,
            'fldStudentContentModified' => $now->format('Y-m-d H:i:s'),
        ];

        $exist = $this->connection->cell('SELECT fldStudentContentId AS id FROM tblStudentsContents WHERE fldStudentContentDeleted != 1 AND tblStudents_fldStudentId = ? AND tblContents_fldContentId = ?', $studentId, $contentId);

        if ($exist) {
            $this->connection->update('tblStudentsContents', $row, [
                'tblStudents_fldStudentId' => $studentId,
                'tblContents_fldContentId' => $contentId,
            ]);
            return $exist;
        }

        $this->connection->insert('tblStudentsContents', $row);

        return (int)$this->connection->lastInsertId();
    }

    /**
     * Find viewed content
     *
     * @return int $id
     */
    public function findViewedContent($studentId, $contentId)
    {
        $id = $this->connection->cell('SELECT fldStudentContentId AS id FROM tblStudentsContents WHERE fldStudentContentDeleted != 1 AND tblStudents_fldStudentId = ? AND tblContents_fldContentId = ?', $studentId, $contentId);

        return $id;
    }
}