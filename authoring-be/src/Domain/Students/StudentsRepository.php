<?php

namespace App\Domain\Students;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class StudentsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert answers row.
     *
     * @param $answer The answer
     * @param $questionId The questionId
     *
     * @return int The new ID
     */
    public function updateAssessed($assessed): void
    {
        $now = new \DateTime();
        $row = [
            'fldStudentAssessed' => $assessed,
            'fldStudentModified' => $now->format('Y-m-d H:i:s'),
        ];

        $this->connection->update('tblStudents', $row, [
            'fldStudentAssessed' => 1
        ]);
    }

    /**
     * Insert answers row.
     *
     * @param $answer The answer
     * @param $questionId The questionId
     *
     * @return int The new ID
     */
    public function findUserByStudentId($studentId): int
    {
        $sql = "SELECT tblUsers.fldUserId AS id FROM tblUsers LEFT JOIN tblStudents ON tblUsers.fldUserId = tblStudents.tblUsers_fldUserId WHERE tblStudents.fldStudentId = ?";

        try {
            $logs = $this->connection->cell($sql, $studentId);
            if ($logs) {
                return $logs;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }
}