<?php

namespace App\Domain\QuizQuestions;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class QuizQuestionsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert questions row.
     *
     * @param $question The question
     * @param $contentId The contentId
     *
     * @return int The new ID
     */
    public function insert($question, $contentId): string
    {
        $now = new \DateTime();
        $row = [
            'fldQuizQuestionContent' => $question,
            'fldQuizQuestionModified' => $now->format('Y-m-d H:i:s'),
            'tblContents_fldContentId' => $contentId
        ];

        $this->connection->insert('tblQuizQuestions', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }

    public function findQuestionsByContentId($id): array
    {
        $sql = 'SELECT tblQuizQuestions.fldQuizQuestionId AS id, tblQuizQuestions.fldQuizQuestionContent AS content FROM tblQuizQuestions WHERE tblQuizQuestions.tblContents_fldContentId = ? AND tblQuizQuestions.fldQuizQuestionDeleted = 0';

        $questions = $this->connection->run($sql, $id);

        return $questions;
    }
}