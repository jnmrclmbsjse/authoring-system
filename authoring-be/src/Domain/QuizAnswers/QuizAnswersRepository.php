<?php

namespace App\Domain\QuizAnswers;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class QuizAnswersRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert answers row.
     *
     * @param $answer The answer
     * @param $questionId The questionId
     *
     * @return int The new ID
     */
    public function insert($answer, $questionId): string
    {
        $now = new \DateTime();
        $row = [
            'fldQuizAnswerContent' => $answer['content'],
            'fldQuizAnswerCorrect' => $answer['correct'],
            'fldQuizAnswerModified' => $now->format('Y-m-d H:i:s'),
            'tblQuizQuestions_fldQuizQuestionId' => $questionId
        ];

        $this->connection->insert('tblQuizAnswers', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }

    public function findAnswersByQuestionId($id): array
    {
        $sql = 'SELECT tblQuizAnswers.fldQuizAnswerId AS id, tblQuizAnswers.fldQuizAnswerContent AS content FROM tblQuizAnswers WHERE tblQuizAnswers.tblQuizQuestions_fldQuizQuestionId = ? AND tblQuizAnswers.fldQuizAnswerDeleted = 0';

        $questions = $this->connection->run($sql, $id);

        return $questions;
    }

    public function findCorrectAnswerIdByQuestionId($id): int
    {
        $sql = 'SELECT fldQuizAnswerId AS id FROM tblQuizAnswers WHERE tblQuizQuestions_fldQuizQuestionId = ? AND fldQuizAnswerCorrect = 1';

        $answerId = $this->connection->cell($sql, $id);

        return $answerId;
    }

    public function insertQuizResults($score, $percentage, $contentId, $studentId, $filename): int
    {
        $now = new \DateTime();
        $row = [
            'fldQuizResultScore' => $score,
            'fldQuizResultPercentage' => $percentage,
            'fldQuizResultModified' => $now->format('Y-m-d H:i:s'),
            'fldQuizResultReference' => $filename,
            'tblContents_fldContentId' => $contentId,
            'tblStudents_fldStudentId' => $studentId
        ];

        $this->connection->insert('tblQuizResults', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }
}