<?php

namespace App\Domain\Nodes;

use ParagonIE\EasyDB\EasyDB;
use App\Domain\Nodes\NodesObject;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class NodesRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert node row.
     *
     * @param NodesObject $node The node
     *
     * @return int The new ID
     */
    public function insert(NodesObject $node): string
    {
        $now = new \DateTime();
        $row = [
            'fldNodeId' => $node->id,
            'fldNodeTitle' => $node->title,
            'fldNodeMetadata' => $node->metadata,
            'fldNodeModified' => $now->format('Y-m-d H:i:s'),
        ];

        $exist = $this->connection->cell('SELECT fldNodeId AS id FROM tblNodes WHERE fldNodeDeleted != 1 AND fldNodeId = ?', $node->id);

        if ($exist) {
            $this->connection->update('tblNodes', $row, [
                'fldNodeId' => $node->id
            ]);
            return (string)$exist;
        }

        $this->connection->insert('tblNodes', $row);

        $id = (string)$node->id;

        return $id;
    }

    /**
     * Find node using ID
     *
     * @param int $id
     *
     * @return array Node details
     */
    public function findNodeDetailsById($id): array
    {

        $sql = "SELECT tblNodes.fldNodeTitle AS title, tblNodes.fldNodeMetadata AS metadata FROM tblNodes WHERE tblNodes.fldNodeId = ?;";

        try {
            $details = $this->connection->row($sql, $id);

            if ($details) {
                return $details;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Insert viewed node
     *
     * @param int $id
     *
     * @return array Content details
     */
    public function insertViewedNode($studentId, $nodeId): int
    {
        $now = new \DateTime();
        $row = [
            'tblNodes_fldNodeId' => $nodeId,
            'tblStudents_fldStudentId' => $studentId,
            'fldStudentNodeModified' => $now->format('Y-m-d H:i:s'),
        ];

        $exist = $this->connection->cell('SELECT fldStudentNodeId AS id FROM tblStudentsNodes WHERE fldStudentNodeDeleted != 1 AND tblStudents_fldStudentId = ? AND tblNodes_fldNodeId = ?', $studentId, $nodeId);

        if ($exist) {
            $this->connection->update('tblStudentsNodes', $row, [
                'tblStudents_fldStudentId' => $studentId,
                'tblNodes_fldNodeId' => $nodeId,
            ]);
            return $exist;
        }

        $this->connection->insert('tblStudentsNodes', $row);

        return (int)$this->connection->lastInsertId();
    }

    /**
     * Find all done nodes
     *
     * @return array Nodes list
     */
    public function findDoneNodesList($studentId): array
    {

        $sql = "SELECT tblStudentsNodes.fldStudentNodeId AS id, tblNodes.fldNodeId AS nodeId, tblNodes.fldNodeTitle AS title FROM tblStudentsNodes JOIN tblNodes ON tblStudentsNodes.tblNodes_fldNodeId = tblNodes.fldNodeId WHERE tblStudentsNodes.fldStudentNodeDeleted != 1";

        try {
            $nodes = $this->connection->run($sql);
            if ($nodes) {
                return $nodes;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }
}