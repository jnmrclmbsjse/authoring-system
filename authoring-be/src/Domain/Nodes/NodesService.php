<?php

namespace App\Domain\Nodes;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Nodes\NodesObject;
use App\Domain\Nodes\NodesRepository;
use App\Domain\Contents\ContentsRepository;

/**
 * Service.
 */
final class NodesService
{
    /**
     * @var NodesRepository
     */
    private $repository;
    private $contentsRepository;

    /**
     * The constructor.
     *
     */
    public function __construct(NodesRepository $repository, ContentsRepository $contentsRepository)
    {
        $this->repository = $repository;
        $this->contentsRepository = $contentsRepository;
    }

    /**
     * Create a new node.
     *
     * @param NodesObject $user The user data
     *
     * @return int The new node ID
     */
    public function create(NodesObject $node, $contents): string
    {
        // Insert node
        $nodeId = $this->repository->insert($node);
        foreach($contents as $content) {
            $content['nodeId'] = $nodeId;
            $this->contentsRepository->insert($content);
        }
        // Logging here: User created successfully

        return $nodeId;
    }

    /**
     * Get concept details.
     *
     * @param int $id The user ID
     *
     * @return array User details
     */
    public function details($id): array
    {
        if (empty($id)) {
            throw new UnexpectedValueException('Missing id parameter');
        }
        $payload = [];
        $node = $this->repository->findNodeDetailsById($id);
        $contents = $this->contentsRepository->findContentsByNodeId($id);
        $payload = $node;
        $payload['contents'] = $contents;

        return $payload;
    }

    /**
     * Insert viewed node
     *
     * @return array Node id
     */
    public function done($studentId, $nodeId): int
    {
        $id = $this->repository->insertViewedNode($studentId, $nodeId);
        return $id;
    }

    /**
     * Viewed nodes list
     *
     * @return array Node id
     */
    public function viewedList($studentId): array
    {
        $nodes = $this->repository->findDoneNodesList($studentId);
        return $nodes;
    }
}