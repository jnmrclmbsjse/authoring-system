<?php

namespace App\Domain\Nodes;

final class NodesObject
{
    /** @var int */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $metadata;
}