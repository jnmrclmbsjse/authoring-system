<?php

namespace App\Domain\Token;

use Firebase\JWT\JWT;
use UnexpectedValueException;
use App\Domain\Token\TokenObject;

/**
 * Service.
 */
final class TokenService
{
    /**
     * Encode a payload to JWT.
     *
     * @param TokenObject $payload The token data
     *
     * @return string the encoded token data
     */
    public function encode(TokenObject $payload): string
    {
        $token = JWT::encode($payload, getenv('jwt_secret'));
        return $token;
    }

    /**
     * Decode a JWT
     *
     * @param string $token the json web token
     *
     * @return string the decoded token data
     */
    public function decode($token): array
    {
        // Validation
        if (!$token) {
            throw new UnexpectedValueException('No token provided');
        }

        $token = JWT::decode($token, getenv('jwt_secret'), ['HS256']);
        return (array)$token;
    }
}