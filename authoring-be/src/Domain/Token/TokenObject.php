<?php

namespace App\Domain\Token;

final class TokenObject
{
    /** @var string */
    public $role;

    /** @var string */
    public $username;

    /** @var string */
    public $id;

    /** @var array */
    public $extra = [];
    
    /** @var string */
    public $iat;

    /** @var string */
    public $exp;

    public function __construct()
    {
        $now = new \DateTime();
        $expiration = new \DateTime('now +2 hours');
        $this->iat = $now->getTimestamp();
        $this->exp = $expiration->getTimestamp();
    }
}