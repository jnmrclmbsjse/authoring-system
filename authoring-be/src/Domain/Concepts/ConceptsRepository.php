<?php

namespace App\Domain\Concepts;

use ParagonIE\EasyDB\EasyDB;
use App\Domain\Concepts\ConceptsObject;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class ConceptsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert user row.
     *
     * @param ConceptsObject $user The user
     *
     * @return int The new ID
     */
    public function insert(ConceptsObject $concept, $teacherId): int
    {
        $now = new \DateTime();
        $row = [
            'fldConceptTitle' => $concept->title,
            'fldConceptMetadata' => $concept->metadata,
            'fldConceptModified' => $now->format('Y-m-d H:i:s'),
            'tblTeachers_fldTeacherId' => $teacherId
        ];

        $this->connection->insert('tblConcepts', $row);

        $id = (int)$this->connection->lastInsertId();

        return $id;
    }

    /**
     * Update concept.
     *
     * @param ConceptsObject $user The user
     *
     * @return int The new ID
     */
    public function update(ConceptsObject $concept, $conceptId): int
    {
        $now = new \DateTime();
        $row = [
            'fldConceptTitle' => $concept->title,
            'fldConceptMetadata' => $concept->metadata,
            'fldConceptModified' => $now->format('Y-m-d H:i:s')
        ];

        $this->connection->update('tblConcepts', $row, [
            'fldConceptId' => $conceptId
        ]);

        return $conceptId;
    }

    /**
     * Find concept using ID
     *
     * @param int $id
     *
     * @return array Concept details
     */
    public function findConceptDetailsById($id): array
    {

        $sql = "SELECT tblConcepts.fldConceptTitle AS title, tblConcepts.fldConceptMetadata AS metadata FROM tblConcepts WHERE tblConcepts.fldConceptId = ?;";

        try {
            $details = $this->connection->row($sql, $id);

            if ($details) {
                return $details;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Find all concepts
     *
     * @return array Concepts list
     */
    public function findAllForTeacher($teacherId): array
    {
        $sql = "SELECT tblConcepts.fldConceptId AS id, tblConcepts.fldConceptTitle AS title, tblConcepts.fldConceptActive AS active, tblConcepts.fldConceptCreated AS created FROM tblConcepts WHERE tblConcepts.tblTeachers_fldTeacherId = ?";

        try {
            $concepts = $this->connection->run($sql, $teacherId);
            if ($concepts) {
                return $concepts;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }
}