<?php

namespace App\Domain\Concepts;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Concepts\ConceptsObject;
use App\Domain\Concepts\ConceptsRepository;

/**
 * Service.
 */
final class ConceptsService
{
    /**
     * @var ConceptsRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     */
    public function __construct(ConceptsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new concept map.
     *
     * @param ConceptsObject $user The user data
     *
     * @return int The new user ID
     */
    public function create(ConceptsObject $concept, $teacherId): int
    {
        // Insert user
        $conceptId = $this->repository->insert($concept, $teacherId);

        // Logging here: User created successfully

        return $conceptId;
    }

    /**
     * Create a new concept map.
     *
     * @param ConceptsObject $user The user data
     *
     * @return int The new user ID
     */
    public function update(ConceptsObject $concept, $conceptId): int
    {
        // Insert user
        $conceptId = $this->repository->update($concept, $conceptId);

        // Logging here: User created successfully

        return $conceptId;
    }

    /**
     * Get concept details.
     *
     * @param int $id The user ID
     *
     * @return array User details
     */
    public function details($id): array
    {
        if (empty($id)) {
            throw new UnexpectedValueException('Missing id parameter');
        }

        $details = $this->repository->findConceptDetailsById($id);

        if (!$details) {
            throw new UsersNotFoundException;
        }

        return $details;
    }

    /**
     * Get all concepts.
     *
     * @return array Concepts list
     */
    public function list($teacherId): array
    {
        $concepts = $this->repository->findAllForTeacher($teacherId);
        return $concepts;
    }
}