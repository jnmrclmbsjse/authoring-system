<?php

namespace App\Domain\Concepts;

final class ConceptsObject
{
    /** @var string */
    public $title;

    /** @var string */
    public $metadata;

    /** @var int */
    public $teacherId;
}