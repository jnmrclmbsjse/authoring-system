<?php

namespace App\Domain\AssessmentsAnswers;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class AssessmentsAnswersRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert answers row.
     *
     * @param $answer The answer
     * @param $questionId The questionId
     *
     * @return int The new ID
     */
    public function insert($answer, $questionId): string
    {
        $now = new \DateTime();
        $row = [
            'fldAssessmentAnswerContent' => $answer['content'],
            'fldAssessmentAnswerCorrect' => $answer['correct'],
            'fldAssessmentAnswerModified' => $now->format('Y-m-d H:i:s'),
            'tblAssessmentsQuestions_fldAssessmentQuestionId' => $questionId
        ];

        $this->connection->insert('tblAssessmentsAnswers', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }

    public function findAnswersByQuestionId($id): array
    {
        $sql = 'SELECT tblAssessmentsAnswers.fldAssessmentAnswerId AS id, tblAssessmentsAnswers.fldAssessmentAnswerContent AS content FROM tblAssessmentsAnswers WHERE tblAssessmentsAnswers.tblAssessmentsQuestions_fldAssessmentQuestionId = ? AND tblAssessmentsAnswers.fldAssessmentAnswerDeleted = 0';

        $questions = $this->connection->run($sql, $id);

        return $questions;
    }

    public function findCorrectAnswerIdByQuestionId($id): int
    {
        $sql = 'SELECT fldAssessmentAnswerId AS id FROM tblAssessmentsAnswers WHERE tblAssessmentsQuestions_fldAssessmentQuestionId = ? AND fldAssessmentAnswerCorrect = 1';

        $answerId = $this->connection->cell($sql, $id);

        return $answerId;
    }
}