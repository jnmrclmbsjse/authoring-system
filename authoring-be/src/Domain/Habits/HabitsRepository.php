<?php

namespace App\Domain\Habits;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class HabitsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert answers row.
     *
     * @param $answer The answer
     * @param $questionId The questionId
     *
     * @return int The new ID
     */
    public function insert($semesterId): string
    {
        $now = new \DateTime();
        $row = [
            'fldHabitModified' => $now->format('Y-m-d H:i:s'),
            'tblSemesters_fldSemesterId' => $semesterId
        ];

        $this->connection->insert('tblHabits', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }

    public function findHabitDetailsBySemesterId($semesterId): array
    {
        $sql = 'SELECT tblHabits.fldHabitId AS id FROM tblHabits WHERE tblHabits.tblSemesters_fldSemesterId = ? AND tblHabits.fldHabitDeleted = 0';

        $habit = $this->connection->row($sql, $semesterId);

        return $habit;
    }

    /**
     * Insert habits result.
     *
     * @param $score The answer
     * @param $questionId The questionId
     * @param $studentId The studentId
     *
     * @return int The new ID
     */
    public function insertHabitsResults($score, $questionId, $studentId): string
    {
        $now = new \DateTime();
        $row = [
            'fldHabitResultScore' => $score,
            'fldHabitResultModified' => $now->format('Y-m-d H:i:s'),
            'tblStudents_fldStudentId' => $studentId,
            'tblHabitsQuestions_fldHabitQuestionId' => 0,
            'fldHabitResultQuestionId' => $questionId,
        ];

        $this->connection->insert('tblHabitsResults', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }
}