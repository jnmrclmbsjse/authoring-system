<?php

namespace App\Domain\Habits;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Users\UsersRepository;
use App\Domain\Habits\HabitsRepository;
use App\Domain\Semesters\SemestersRepository;
use App\Domain\HabitsQuestions\HabitsQuestionsRepository;

/**
 * Service.
 */
final class HabitsService
{
    private $repository;
    private $semesterRepository;
    private $usersRepository;
    private $questionRepository;

    /**
     * The constructor.
     *
     */
    public function __construct(HabitsRepository $repository, SemestersRepository $semesterRepository, HabitsQuestionsRepository $questionRepository, UsersRepository $usersRepository)
    {
        $this->repository = $repository;
        $this->semesterRepository = $semesterRepository;
        $this->questionRepository = $questionRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Create a new quiz.
     *
     * @return int The content id
     */
    public function create($habits): string
    {
        $semesterId = $this->semesterRepository->findActiveSemesterId();
        if (!$semesterId) {
            return "undefined";
        }

        $habitId = $this->repository->insert($semesterId);

        $items = $habits['items'];
        // Insert question and answers
        foreach ($items as $item) {
            $question = $item['question'];
            $category = $item['category'];
            $questionId = $this->questionRepository->insert($question, $category, $habitId);
        }
        // Logging here: User created successfully

        return $habitId;
    }

    /**
     * Get assessment details.
     *
     * @return array Habit details
     */
    public function details(): array
    {
        $semesterId = $this->semesterRepository->findActiveSemesterId();

        $habit = $this->repository->findHabitDetailsBySemesterId($semesterId);
        $habit['questions'] = [];
        $questions = $this->questionRepository->findQuestionsByHabitId($habit['id']);
        $habit['questions'] = $questions;
        return $habit;
    }

    /**
     * Submit habits assessment answers.
     *
     * @param array $answers the answer array
     * @param int $studentId the studentId
     *
     * @return array assessment total
     */
    public function submit($answers, $studentId): int
    {
        $score = 0;
        $total = count($answers);

        foreach($answers as $questionId => $score) {
           $this->repository->insertHabitsResults($score, $questionId, $studentId);
        }

        $this->usersRepository->updateStudentAssessed($studentId, 1);

        return $total;
    }
}