<?php

namespace App\Domain\Roles;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class RolesRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Find all roles
     *
     * @return array Roles list
     */
    public function findAll(): array
    {

        $sql = "SELECT tblRoles.fldRoleId AS id, tblRoles.fldRoleTitle AS title FROM tblRoles WHERE tblRoles.fldRoleDeleted != 1";

        try {
            $roles = $this->connection->run($sql);
            if ($roles) {
                return $roles;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }
}