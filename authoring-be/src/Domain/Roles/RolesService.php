<?php

namespace App\Domain\Roles;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Roles\RolesRepository;

/**
 * Service.
 */
final class RolesService
{
    /**
     * @var RolesRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     */
    public function __construct(RolesRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get all users.
     *
     * @return array Users kust
     */
    public function list(): array
    {
        $users = $this->repository->findAll();

        if (!$users) {
            throw new UsersNotFoundException;
        }

        return $users;
    }
}