<?php

namespace App\Domain\Files;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Interfaces\SettingsInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\UploadedFileInterface;

/**
 * Service.
 */
final class FilesService
{
    private $uploadDirectory;
    /**
     * The constructor.
     *
     */
    public function __construct(ContainerInterface $container)
    {
        $this->uploadDirectory = $container->get('settings')['uploadDirectory'];
        // Empty constructor
    }

    /**
     * Upload files.
     *
     * @param $files the uploaded files
     *
     * @return string the filename
     */
    public function upload($files): string
    {
        $uploadedFile = $files['file'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = $this->moveUploadedFile($this->uploadDirectory, $uploadedFile);
        }
        // Logging here: User created successfully

        return $filename;
    }

    /**
    * Moves the uploaded file to the upload directory and assigns it a unique name
    * to avoid overwriting an existing uploaded file.
    *
    * @param string $directory directory to which the file is moved
    * @param UploadedFileInterface $uploaded file uploaded file to move
    * @return string filename of moved file
    */
    public function moveUploadedFile($directory, UploadedFileInterface $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
        return $filename;
    }
}