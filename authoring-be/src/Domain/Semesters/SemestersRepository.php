<?php

namespace App\Domain\Semesters;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class SemestersRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert semester row.
     *
     * @param array $semester The semester
     *
     * @return int The new ID
     */
    public function removeActiveThenInsert($semester): int
    {
        $now = new \DateTime();

        $this->connection->update('tblSemesters', [
            'fldSemesterActive' => 0,
            'fldSemesterModified' => $now->format('Y-m-d H:i:s'),
        ], [
            'fldSemesterActive' => 1
        ]);

        $row = [
            'fldSemesterTitle' => $semester['title'],
            'fldSemesterActive' => 1,
            'fldSemesterModified' => $now->format('Y-m-d H:i:s'),
        ];

        $this->connection->insert('tblSemesters', $row);

        $id = (int)$this->connection->lastInsertId();

        return $id;
    }

    /**
     * Find all users
     *
     * @return array Users list
     */
    public function findAll(): array
    {

        $sql = "SELECT tblSemesters.fldSemesterId AS id, tblSemesters.fldSemesterTitle AS title, tblSemesters.fldSemesterCreated AS created, tblSemesters.fldSemesterActive AS active FROM tblSemesters WHERE tblSemesters.fldSemesterDeleted != 1";

        try {
            $semesters = $this->connection->run($sql);
            if ($semesters) {
                return $semesters;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Find active semester
     *
     * @return array Users list
     */
    public function findActiveSemesterId()
    {

        $sql = "SELECT tblSemesters.fldSemesterId AS id FROM tblSemesters WHERE tblSemesters.fldSemesterDeleted != 1 AND tblSemesters.fldSemesterActive = 1";

        try {
            $semester = $this->connection->cell($sql);
            if ($semester) {
                return $semester;
            } else {
                return null;
            }    
        } catch (Exception $e) {
            return null;
        }
    }

    public function deleteSemester($semesterId)
    {
        $now = new \DateTime();
        $row = [
            'fldSemesterActive' => 0,
            'fldSemesterDeleted' => 1,
            'fldSemesterModified' => $now->format('Y-m-d H:i:s'),
        ];

        $this->connection->update('tblSemesters', $row, [
            'fldSemesterId' => $semesterId
        ]);

        return 1;
    }
}