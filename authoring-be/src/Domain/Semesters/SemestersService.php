<?php

namespace App\Domain\Semesters;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Logs\LogsRepository;
use App\Domain\Students\StudentsRepository;
use App\Domain\Semesters\SemestersRepository;

/**
 * Service.
 */
final class SemestersService
{
    /**
     * @var SemestersRepository
     */
    private $repository;
    private $logsRepository;
    private $studentsRepository;

    /**
     * The constructor.
     *
     */
    public function __construct(SemestersRepository $repository, StudentsRepository $studentsRepository, LogsRepository $logsRepository)
    {
        $this->repository = $repository;
        $this->logsRepository = $logsRepository;
        $this->studentsRepository = $studentsRepository;
    }

    /**
     * Create a new semester.
     */
    public function create($semester): int
    {
        // Insert semester
        $semesterId = $this->repository->removeActiveThenInsert($semester);
        $this->studentsRepository->updateAssessed(0);
        // $this->logsRepository->insert()
        // Logging here: Semester created successfully

        return $semesterId;
    }

    /**
     * Get all users.
     *
     * @return array Users list
     */
    public function list(): array
    {
        $semesters = $this->repository->findAll();

        return $semesters;
    }

    /**
     * Delete semester.
     *
     * @return int success
     */
    public function delete($semesterId): int
    {
        $success = $this->repository->deleteSemester($semesterId);

        return $success;
    }
}