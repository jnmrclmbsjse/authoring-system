<?php

namespace App\Domain\HabitsQuestions;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class HabitsQuestionsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert questions row.
     *
     * @param $question The question
     * @param $contentId The contentId
     *
     * @return int The new ID
     */
    public function insert($question, $category, $contentId): string
    {
        $now = new \DateTime();
        $row = [
            'fldHabitQuestionContent' => $question,
            'fldHabitQuestionCategory' => $category,
            'fldHabitQuestionModified' => $now->format('Y-m-d H:i:s'),
            'tblHabits_fldHabitId' => $contentId
        ];

        $this->connection->insert('tblHabitsQuestions', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }

    public function findQuestionsByHabitId($id): array
    {
        $sql = 'SELECT tblHabitsQuestions.fldHabitQuestionId AS id, tblHabitsQuestions.fldHabitQuestionContent AS content, tblHabitsQuestions.fldHabitQuestionCategory AS category FROM tblHabitsQuestions WHERE tblHabitsQuestions.tblHabits_fldHabitId = ? AND tblHabitsQuestions.fldHabitQuestionDeleted = 0';

        $questions = $this->connection->run($sql, $id);

        return $questions;
    }
}