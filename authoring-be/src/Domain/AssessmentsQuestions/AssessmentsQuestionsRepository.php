<?php

namespace App\Domain\AssessmentsQuestions;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class AssessmentsQuestionsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert questions row.
     *
     * @param $question The question
     * @param $contentId The contentId
     *
     * @return int The new ID
     */
    public function insert($question, $contentId): string
    {
        $now = new \DateTime();
        $row = [
            'fldAssessmentQuestionContent' => $question,
            'fldAssessmentQuestionModified' => $now->format('Y-m-d H:i:s'),
            'tblAssessments_fldAssessmentId' => $contentId
        ];

        $this->connection->insert('tblAssessmentsQuestions', $row);

        $id = $this->connection->lastInsertId();

        return $id;
    }

    public function findQuestionsByAssessmentId($id): array
    {
        $sql = 'SELECT tblAssessmentsQuestions.fldAssessmentQuestionId AS id, tblAssessmentsQuestions.fldAssessmentQuestionContent AS content FROM tblAssessmentsQuestions WHERE tblAssessmentsQuestions.tblAssessments_fldAssessmentId = ? AND tblAssessmentsQuestions.fldAssessmentQuestionDeleted = 0';

        $questions = $this->connection->run($sql, $id);

        return $questions;
    }
}