<?php

namespace App\Domain\Quizzes;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class QuizzesRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    public function findAllResultsForStudent($studentId): array
    {
        $sql = 'SELECT tblQuizResults.fldQuizResultId AS id, tblQuizResults.fldQuizResultScore AS score, tblQuizResults.fldQuizResultPercentage AS percentage, tblQuizResults.fldQuizResultReference AS reference, tblQuizResults.fldQuizResultCreated AS created FROM tblQuizResults WHERE tblQuizResults.tblStudents_fldStudentId = ? AND tblQuizResults.fldQuizResultDeleted != 1 ORDER BY tblQuizResults.fldQuizResultCreated DESC';

        $results = $this->connection->run($sql, $studentId);

        return $results;
    }
}