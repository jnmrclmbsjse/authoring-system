<?php

namespace App\Domain\Quizzes;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use Psr\Container\ContainerInterface;
use App\Domain\Quizzes\QuizzesRepository;
use Psr\Http\Message\UploadedFileInterface;
use App\Domain\QuizAnswers\QuizAnswersRepository;
use App\Domain\QuizQuestions\QuizQuestionsRepository;

/**
 * Service.
 */
final class QuizzesService
{
    private $answerRepository;
    private $uploadDirectory;
    private $questionRepository;
    private $repository;

    /**
     * The constructor.
     *
     */
    public function __construct(ContainerInterface $container, QuizzesRepository $repository, QuizAnswersRepository $answerRepository, QuizQuestionsRepository $questionRepository)
    {
        $this->answerRepository = $answerRepository;
        $this->repository = $repository;
        $this->questionRepository = $questionRepository;
        $this->uploadDirectory = $container->get('settings')['uploadDirectory'];
    }

    /**
     * Create a new quiz.
     *
     * @return int The content id
     */
    public function create($items, $contentId): string
    {
        // Insert question and answers
        foreach ($items as $item) {
            $question = $item['topic'];
            $questionId = $this->questionRepository->insert($question, $contentId);

            $answers = $item['answers'];
            foreach ($answers as $answer) {
                $answerObject = [];
                $answerObject['content'] = $answer;
                $answerObject['correct'] = 0;
                if (strpos($answer, '(c)') !== false) {
                    $answerObject['content'] = str_replace('(c)', '', $answer);
                    $answerObject['correct'] = 1;
                }
                $answerId = $this->answerRepository->insert($answerObject, $questionId);
            }
        }
        // Logging here: User created successfully

        return $contentId;
    }

    /**
     * Get all questions.
     *
     * @return int The content id
     */
    public function details($id): array
    {
        if (empty($id)) {
            throw new UnexpectedValueException('Missing id parameter');
        }
        $quiz['items'] = [];
        $questions = $this->questionRepository->findQuestionsByContentId($id);
        foreach ($questions as $question) {
            $answer = $this->answerRepository->findAnswersByQuestionId($question['id']);
            $builder = [
                'topic' => $question,
                'answers' => $answer
            ];
            array_push($quiz['items'], $builder);
        }

        return $quiz;
    }

    /**
     * Compute score and pecentage.
     *
     * @param array $answers the answer array
     * @param int $contentId the contentId
     * @param int $studentId the studentId
     *
     * @return array exam result
     */
    public function evaluate($answers, $contentId, $studentId, $files): array
    {
        $uploadedFile = $files['file'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = $this->moveUploadedFile($this->uploadDirectory, $uploadedFile);
        }

        $score = 0;
        $total = count($answers);
        foreach($answers as $answer) {
            $questionId = $answer['topic'];
            $answerId = $answer['answer'];
            $correct = $this->answerRepository->findCorrectAnswerIdByQuestionId($questionId);

            if ($correct == $answerId) {
                $score++;
            }
        }
        $percentage = ($score / $total) * 100;
        $percentage = round($percentage);

        $resultId = $this->answerRepository->insertQuizResults($score, $percentage, $contentId, $studentId, $filename);

        $result = [
            "resultId" => $resultId,
            "score" => $score,
            "percentage" => $percentage
        ];

        return $result;
    }

    /**
     * Get all quizzes.
     *
     * @return array Courses list
     */
    public function listStudents($studentId): array
    {
        $results = $this->repository->findAllResultsForStudent($studentId);
        return $results;
    }

    /**
    * Moves the uploaded file to the upload directory and assigns it a unique name
    * to avoid overwriting an existing uploaded file.
    *
    * @param string $directory directory to which the file is moved
    * @param UploadedFileInterface $uploaded file uploaded file to move
    * @return string filename of moved file
    */
    public function moveUploadedFile($directory, UploadedFileInterface $uploadedFile)
    {
        $extension = 'png';
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
        return $filename;
    }
}