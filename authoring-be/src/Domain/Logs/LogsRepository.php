<?php

namespace App\Domain\Logs;

use ParagonIE\EasyDB\EasyDB;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class LogsRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    public function insert($userId, $type, $description): int
    {
        $now = new \DateTime();
        $row = [
            'fldLogType' => $type,
            'fldLogDescription' => $description,
            'fldLogModified' => $now->format('Y-m-d H:i:s'),
            'tblUsers_fldUserId' => $userId,
        ];

        $this->connection->insert('tblLogs', $row);

        $id = (int)$this->connection->lastInsertId();

        return $id;
    }

    /**
     * Find all logs
     *
     * @return array Logs list
     */
    public function findAll(): array
    {

        $sql = "SELECT tblLogs.fldLogId AS id, tblLogs.fldLogType AS type, tblLogs.fldLogDescription AS description, tblLogs.fldLogCreated AS created, tblUsers.fldUserUsername AS username FROM tblLogs LEFT JOIN tblUsers ON tblUsers.fldUserId = tblLogs.tblUsers_fldUserId WHERE tblLogs.fldLogId != 1 ORDER BY tblLogs.fldLogCreated DESC";

        try {
            $logs = $this->connection->run($sql);
            if ($logs) {
                return $logs;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Find all logs
     *
     * @return array Logs list
     */
    public function findLogsByUserId($userId, $type): array
    {
        $sql = "SELECT tblLogs.fldLogId AS id, tblLogs.fldLogType AS type, tblLogs.fldLogDescription AS description, tblLogs.fldLogCreated AS created, tblUsers.fldUserUsername AS username FROM tblLogs LEFT JOIN tblUsers ON tblUsers.fldUserId = tblLogs.tblUsers_fldUserId WHERE tblLogs.fldLogId != 1 AND tblUsers.fldUserId = ? AND tblLogs.fldLogType = ? ORDER BY tblLogs.fldLogCreated DESC";

        try {
            $logs = $this->connection->run($sql, $userId, $type);
            if ($logs) {
                return $logs;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }
}