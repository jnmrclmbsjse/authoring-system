<?php

namespace App\Domain\Logs;

use UnexpectedValueException;
use App\Domain\Logs\LogsRepository;
use App\Domain\Students\StudentsRepository;
use App\Domain\Users\UsersNotFoundException;

/**
 * Service.
 */
final class LogsService
{

    /**
     * @var LogsRepository
     */
    private $repository;

    private $studentsRepository;

    /**
     * The constructor.
     *
     */
    public function __construct(LogsRepository $repository, StudentsRepository $studentsRepository)
    {
        $this->repository = $repository;
        $this->studentsRepository = $studentsRepository;
    }

    /**
     * Get all logs.
     *
     * @return array Users list
     */
    public function list(): array
    {
        $logs = $this->repository->findAll();

        if (!$logs) {
            throw new UsersNotFoundException;
        }

        return $logs;
    }

    /**
     * Get all logs.
     *
     * @return array Users list
     */
    public function studentLogs($studentId, $type): array
    {
        $userId = $this->studentsRepository->findUserByStudentId($studentId);
        $logs = $this->repository->findLogsByUserId($userId, $type);

        if (!$logs) {
            throw new UsersNotFoundException;
        }

        return $logs;
    }
}