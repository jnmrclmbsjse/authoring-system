<?php

namespace App\Domain\Courses;

use ParagonIE\EasyDB\EasyDB;
use App\Domain\Courses\CoursesObject;
use PHPassLib\Application\Context;
use Psr\Container\ContainerInterface;
use App\Application\Interfaces\FactoryInterface;

/**
 * Repository.
 */
class CoursesRepository
{
    /**
     * @var EasyDB The database connection
     */
    private $connection;
    /**
     * @var Context password hasher
     */
    private $context;

    /**
     * Constructor.
     *
     * @param EasyDB $connection The database connection
     * @param Context $context password hasher
     */
    public function __construct(EasyDB $connection, Context $context)
    {
        $this->connection = $connection;
        $this->context = $context;
    }

    /**
     * Insert user row.
     *
     * @param CoursesObject $user The user
     *
     * @return int The new ID
     */
    public function insert(CoursesObject $course): int
    {
        $now = new \DateTime();
        $row = [
            'fldCourseCode' => $course->code,
            'fldCourseTitle' => $course->title,
            'fldCourseModified' => $now->format('Y-m-d H:i:s'),
        ];

        $this->connection->insert('tblCourses', $row);

        $id = (int)$this->connection->lastInsertId();

        return $id;
    }

    /**
     * Create a new assignment instance.
     *
     * @param array $request
     *
     * @return int The new ID
     */
    public function insertTeacherAssignment($courseId, $teacherId, $semesterId): int
    {
        if ($courseId == 'null') {
            $this->connection->update('tblTeachersCourses', [
                'fldTeacherCourseDeleted' => 1
            ], [
                'tblTeachers_fldTeacherId' => $teacherId
            ]);

            return 1;
        }

        $isTaken = $this->connection->cell('SELECT fldTeacherCourseId AS id FROM tblTeachersCourses WHERE fldTeacherCourseDeleted != 1 AND tblCourses_fldCourseId = ?', $courseId);

        if($isTaken) {
            return 0;
        }

        $now = new \DateTime();
        $row = [
            'tblTeachers_fldTeacherId' => $teacherId,
            'tblCourses_fldCourseId' => $courseId,
            'tblSemesters_fldSemesterId' => $semesterId,
            'fldTeacherCourseModified' => $now->format('Y-m-d H:i:s'),
        ];

        $exist = $this->connection->cell('SELECT fldTeacherCourseId AS id FROM tblTeachersCourses WHERE fldTeacherCourseDeleted != 1 AND tblTeachers_fldTeacherId = ?', $teacherId);

        if ($exist) {
            $this->connection->update('tblTeachersCourses', $row, [
                'tblTeachers_fldTeacherId' => $teacherId
            ]);
            return $exist;
        }

        $this->connection->insert('tblTeachersCourses', $row);

        return (int)$this->connection->lastInsertId();
    }

    /**
     * Create a new assignment instance.
     *
     * @param array $request
     *
     * @return int The new ID
     */
    public function insertStudentAssignment($teacherCourseId, $studentId): int
    {
        $now = new \DateTime();
        $row = [
            'tblStudents_fldStudentId' => $studentId,
            'tblTeachersCourses_fldTeacherCourseId' => $teacherCourseId,
            'fldStudentCourseModified' => $now->format('Y-m-d H:i:s'),
        ];

        $exist = $this->connection->cell('SELECT fldStudentCourseId AS id FROM tblStudentsCourses WHERE fldStudentCourseDeleted != 1 AND tblStudents_fldStudentId = ?', $studentId);
        
        if ($exist) {
            $this->connection->update('tblStudentsCourses', $row, [
                'tblStudents_fldStudentId' => $studentId
            ]);
            return $exist;
        }

        $this->connection->insert('tblStudentsCourses', $row);

        return (int)$this->connection->lastInsertId();
    }

    /**
     * Find all courses
     *
     * @return array Courses list
     */
    public function findAll(): array
    {

        $sql = "SELECT tblCourses.fldCourseId AS id, tblCourses.fldCourseCode AS code, tblCourses.fldCourseTitle AS title, tblCourses.fldCourseCreated AS created FROM tblCourses WHERE tblCourses.fldCourseDeleted != 1";

        try {
            $courses = $this->connection->run($sql);
            if ($courses) {
                return $courses;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Find all courses
     *
     * @return array Courses list
     */
    public function findAllCoursesTeachers(): array
    {

        $sql = "SELECT tblTeachersCourses.fldTeacherCourseId AS id, tblCourses.fldCourseCode AS code, tblCourses.fldCourseTitle AS title, tblCourses.fldCourseCreated AS created, tblTeachers.fldTeacherId AS teacherId, tblTeachers.fldTeacherFirstname AS firstname, tblTeachers.fldTeacherLastname AS lastname FROM tblTeachersCourses LEFT JOIN tblCourses ON tblCourses.fldCourseId = tblTeachersCourses.tblCourses_fldCourseId LEFT JOIN tblTeachers ON tblTeachers.fldTeacherId = tblTeachersCourses.tblTeachers_fldTeacherId WHERE tblCourses.fldCourseDeleted != 1 AND tblTeachersCourses.fldTeacherCourseDeleted != 1";

        try {
            $courses = $this->connection->run($sql);
            if ($courses) {
                return $courses;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    public function findCourseByTeacherAndSemester($semesterId, $teacherId) {
        $sql = "SELECT tblTeachersCourses.fldTeacherCourseId AS teacherCourseId, tblCourses.fldCourseId AS courseId, tblCourses.fldCourseTitle AS title FROM tblTeachersCourses LEFT JOIN tblCourses ON tblCourses.fldCourseId = tblTeachersCourses.tblCourses_fldCourseId WHERE tblTeachersCourses.tblTeachers_fldTeacherId = ? AND tblTeachersCourses.tblSemesters_fldSemesterId = ?";

        try {
            $course = $this->connection->row($sql, $teacherId, $semesterId);
            error_log(\json_encode($semesterId));
            if ($course) {
                return $course;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }

    public function findConceptIdByTeacherCourseId($teacherCourseId, $semesterId) {
        $sql = "SELECT tblConcepts.fldConceptId AS id FROM tblConcepts LEFT JOIN tblTeachers ON tblConcepts.tblTeachers_fldTeacherId = tblTeachers.fldTeacherId LEFT JOIN tblTeachersCourses ON tblTeachersCourses.tblTeachers_fldTeacherId = tblTeachers.fldTeacherId WHERE tblTeachersCourses.fldTeacherCourseId = ? AND tblTeachersCourses.tblSemesters_fldSemesterId = ?";

        $conceptId = $this->connection->cell($sql, $teacherCourseId, $semesterId);

        return $conceptId;
    }

    public function deleteCourse($courseId)
    {
        $now = new \DateTime();
        $row = [
            'fldCourseDeleted' => 1,
            'fldCourseModified' => $now->format('Y-m-d H:i:s'),
        ];

        $this->connection->update('tblCourses', $row, [
            'fldCourseId' => $courseId
        ]);

        return 1;
    }

    public function updateCourse($course, $courseId)
    {
        $now = new \DateTime();
        $row = [
            'fldCourseCode' => $course['code'],
            'fldCourseTitle' => $course['title'],
            'fldCourseModified' => $now->format('Y-m-d H:i:s'),
        ];

        $this->connection->update('tblCourses', $row, [
            'fldCourseId' => $courseId
        ]);

        return 1;
    }

    /**
     * Find course using ID
     *
     * @param int $id
     *
     * @return array Course details
     */
    public function findCourseDetailsById($id): array
    {

        $sql = "SELECT tblCourses.fldCourseId AS id, tblCourses.fldCourseTitle AS title, tblCourses.fldCourseCode AS code FROM tblCourses WHERE tblCourses.fldCourseId = ?;";

        try {
            $details = $this->connection->row($sql, $id);
            if ($details) {
                return $details;
            } else {
                return [];
            }    
        } catch (Exception $e) {
            return [];
        }
    }
}