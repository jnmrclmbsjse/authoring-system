<?php

namespace App\Domain\Courses;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;
use UnexpectedValueException;
use App\Domain\Courses\CoursesObject;
use App\Domain\Courses\CoursesRepository;
use App\Domain\Concepts\ConceptsRepository;
use App\Domain\Users\UsersNotFoundException;
use App\Domain\Semesters\SemestersRepository;

/**
 * Service.
 */
final class CoursesService
{
    /**
     * @var CoursesRepository
     */
    private $repository;
    private $conceptRepository;
    private $semesterRepository;

    /**
     * The constructor.
     *
     */
    public function __construct(CoursesRepository $repository, ConceptsRepository $conceptRepository, SemestersRepository $semesterRepository)
    {
        $this->repository = $repository;
        $this->conceptRepository = $conceptRepository;
        $this->semesterRepository = $semesterRepository;
    }

    /**
     * Create a new user.
     *
     * @param CoursesObject $user The user data
     *
     * @return int The new user ID
     */
    public function create(CoursesObject $course): int
    {
        // Insert user
        $courseId = $this->repository->insert($course);

        // Logging here: User created successfully

        return $courseId;
    }

    /**
     * Get all courses.
     *
     * @return array Courses list
     */
    public function list(): array
    {
        $courses = $this->repository->findAll();
        if (!$courses) {
            $courses = [];
        }
        return $courses;
    }

    /**
     * Get all courses with teachers.
     *
     * @return array Courses list
     */
    public function coursesTeachersList(): array
    {
        $courses = $this->repository->findAllCoursesTeachers();
        return $courses;
    }

    /**
     * Assign a course to user.
     *
     * @param array $request The course and user id
     *
     * @return int the assignment id
     */
    public function assign($courseId, $userId, $role): int
    {
        // Insert user
        if ($role == 'student') {
            $assignmentId = $this->repository->insertStudentAssignment($courseId, $userId);
        }

        if ($role == 'teacher') {
            $semesterId = $this->semesterRepository->findActiveSemesterId();
            $assignmentId = $this->repository->insertTeacherAssignment($courseId, $userId, $semesterId);
        }

        // Logging here: User created successfully

        return $assignmentId;
    }

    /**
     * Get concept details.
     *
     * @param int $id The user ID
     *
     * @return array User details
     */
    public function conceptDetails($teacherCourseId)
    {
        $semesterId = $this->semesterRepository->findActiveSemesterId();

        $conceptId = $this->repository->findConceptIdByTeacherCourseId($teacherCourseId, $semesterId); 

        $details = $this->conceptRepository->findConceptDetailsById($conceptId);

        return $details;
    }

    /**
     * Delete course.
     *
     * @return int success
     */
    public function delete($courseId): int
    {
        $success = $this->repository->deleteCourse($courseId);

        return $success;
    }

    /**
     * Get course details.
     *
     * @param int $id The course ID
     *
     * @return array Course details
     */
    public function details($courseId): array
    {
        if (empty($courseId)) {
            throw new UnexpectedValueException('Missing id parameter');
        }

        $details = $this->repository->findCourseDetailsById($courseId);

        if (!$details) {
            throw new UsersNotFoundException;
        }

        return $details;
    }

    /**
     * Update course.
     *
     * @return int id
     */
    public function update($course, $courseId): int
    {
        $id = $this->repository->updateCourse($course, $courseId);

        return $id;
    }
}