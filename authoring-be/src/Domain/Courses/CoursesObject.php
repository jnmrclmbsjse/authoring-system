<?php

namespace App\Domain\Courses;

final class CoursesObject
{
    /** @var string */
    public $code;

    /** @var string */
    public $title;
}