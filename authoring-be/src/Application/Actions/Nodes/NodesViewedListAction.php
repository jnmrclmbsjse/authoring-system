<?php

namespace App\Application\Actions\Nodes;

use App\Application\Actions\Nodes\NodesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class NodesViewedListAction extends NodesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->args;
        // Mapping (should be done in a mapper class)
        $studentId = $input['studentId'];

        // Invoke the Domain with inputs and retain the result
        $nodes = $this->service->viewedList($studentId);

        // Transform the result into the JSON representation
        $response = [
            'nodes' => $nodes,
            'message' => 'Nodes were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}