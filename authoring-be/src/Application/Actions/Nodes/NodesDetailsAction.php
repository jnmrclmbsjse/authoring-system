<?php

namespace App\Application\Actions\Nodes;

use App\Application\Actions\Nodes\NodesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class NodesDetailsAction extends NodesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->args;

        // Mapping (should be done in a mapper class)
        $id = $input['id'];

        // Invoke the Domain with inputs and retain the result
        $node = $this->service->details($id);

        // Transform the result into the JSON representation
        $response = [
            'node' => $node,
            'message' => 'Node was found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}