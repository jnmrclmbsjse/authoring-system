<?php
declare(strict_types=1);

namespace App\Application\Actions\Nodes;

use Psr\Log\LoggerInterface;
use App\Domain\Nodes\NodesService;
use App\Application\Actions\Action;

abstract class NodesAction extends Action
{
    /**
     * @var NodesService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, NodesService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 