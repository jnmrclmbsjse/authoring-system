<?php

namespace App\Application\Actions\Nodes;

use App\Domain\Nodes\NodesObject;
use App\Application\Actions\ActionPayload;
use App\Application\Actions\Nodes\NodesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class NodesCreateAction extends NodesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $node = new NodesObject();
        $node->id = $input['id'];
        $node->title = $input['title'];
        $node->metadata = $input['metadata'];

        $contents = $input['contents'];

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->create($node, $contents);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Node has been created',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}