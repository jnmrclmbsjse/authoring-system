<?php

namespace App\Application\Actions\Nodes;

use App\Application\Actions\Nodes\NodesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class NodesDoneAction extends NodesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->args;
        // Mapping (should be done in a mapper class)
        $nodeId = $input['nodeId'];
        $studentId = $input['studentId'];

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->done($studentId, $nodeId);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Node was done',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}