<?php
declare(strict_types=1);

namespace App\Application\Actions\Logs;

use Psr\Log\LoggerInterface;
use App\Domain\Logs\LogsService;
use App\Application\Actions\Action;

abstract class LogsAction extends Action
{
    /**
     * @var LogsService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, LogsService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
