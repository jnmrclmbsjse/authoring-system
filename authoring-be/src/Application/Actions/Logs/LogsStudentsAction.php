<?php

namespace App\Application\Actions\Logs;

use App\Application\Actions\Logs\LogsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class LogsStudentsAction extends LogsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $args = (array)$this->args;

        // Mapping (should be done in a mapper class)
        $studentId = $args['studentId'];
        $params = (array)$this->request->getQueryParams();
        $type = isset($params['type']) ? $params['type'] : null;
        // Invoke the Domain with inputs and retain the result
        $logs = $this->service->studentLogs($studentId, $type);

        // Transform the result into the JSON representation
        $response = [
            'logs' => $logs,
            'message' => 'Logs were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}