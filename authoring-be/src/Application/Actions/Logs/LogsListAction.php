<?php

namespace App\Application\Actions\Logs;

use App\Application\Actions\Logs\LogsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class LogsListAction extends LogsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Invoke the Domain with inputs and retain the result
        $logs = $this->service->list();

        // Transform the result into the JSON representation
        $response = [
            'logs' => $logs,
            'message' => 'Logs were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}