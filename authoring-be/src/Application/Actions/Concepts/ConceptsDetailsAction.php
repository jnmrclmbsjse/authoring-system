<?php

namespace App\Application\Actions\Concepts;

use App\Application\Actions\Concepts\ConceptsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class ConceptsDetailsAction extends ConceptsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->args;

        // Mapping (should be done in a mapper class)
        $id = $input['id'];

        // Invoke the Domain with inputs and retain the result
        $concept = $this->service->details($id);

        // Transform the result into the JSON representation
        $response = [
            'concept' => $concept,
            'message' => 'Concept was found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}