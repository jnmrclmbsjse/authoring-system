<?php

namespace App\Application\Actions\Concepts;

use Psr\Log\LoggerInterface;
use App\Domain\Token\TokenService;
use App\Domain\Concepts\ConceptsObject;
use App\Domain\Concepts\ConceptsService;
use App\Application\Actions\ActionPayload;
use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Actions\Concepts\ConceptsAction;

final class ConceptsCreateAction extends ConceptsAction
{
    private $tokenService;
    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, ConceptsService $service, TokenService $tokenService)
    {
        parent::__construct($logger, $service, $tokenService);
        $this->tokenService = $tokenService;
    }
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();
        $tokenPayload = $this->attributes['tokenPayload'];
        $teacherId = $tokenPayload->extra->teacherId;

        // Mapping (should be done in a mapper class)
        $concept = new ConceptsObject();
        $concept->title = $input['title'];
        $concept->metadata = $input['metadata'];

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->create($concept, $teacherId);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Concept has been created',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}