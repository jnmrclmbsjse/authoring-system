<?php

namespace App\Application\Actions\Concepts;

use App\Application\Actions\Concepts\ConceptsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class ConceptsListAction extends ConceptsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $tokenPayload = $this->attributes['tokenPayload'];
        $teacherId = $tokenPayload->extra->teacherId;
        // Invoke the Domain with inputs and retain the result
        $concepts = $this->service->list($teacherId);

        // Transform the result into the JSON representation
        $response = [
            'concepts' => $concepts,
            'message' => 'Concepts were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}