<?php
declare(strict_types=1);

namespace App\Application\Actions\Concepts;

use Psr\Log\LoggerInterface;
use App\Domain\Concepts\ConceptsService;
use App\Application\Actions\Action;

abstract class ConceptsAction extends Action
{
    /**
     * @var ConceptsService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, ConceptsService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 