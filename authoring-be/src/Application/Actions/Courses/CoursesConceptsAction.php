<?php

namespace App\Application\Actions\Courses;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Courses\CoursesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class CoursesConceptsAction extends CoursesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $args = $this->args;
        $teacherCourseId = $args['id'];
        // Invoke the Domain with inputs and retain the result
        $concept = $this->service->conceptDetails($teacherCourseId);

        // Transform the result into the JSON representation
        $response = [
            'concept' => $concept,
            'message' => 'Concept was found',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}