<?php
declare(strict_types=1);

namespace App\Application\Actions\Courses;

use Psr\Log\LoggerInterface;
use App\Domain\Courses\CoursesService;
use App\Application\Actions\Action;

abstract class CoursesAction extends Action
{
    /**
     * @var CoursesService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, CoursesService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 