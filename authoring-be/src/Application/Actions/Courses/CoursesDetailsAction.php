<?php

namespace App\Application\Actions\Courses;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Courses\CoursesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class CoursesDetailsAction extends CoursesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $args = (array)$this->args;

        // Mapping (should be done in a mapper class)
        $courseId = $args['courseId'];

        // Invoke the Domain with inputs and retain the result
        $course = $this->service->details($courseId);

        // Transform the result into the JSON representation
        $response = [
            'course' => $course,
            'message' => 'Course was found',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(200, $response);
        return $this->respond($payload);
    }
}