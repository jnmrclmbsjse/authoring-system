<?php

namespace App\Application\Actions\Courses;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Courses\CoursesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class CoursesAssignStudentAction extends CoursesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();
        $args = (array)$this->args;

        // Mapping (should be done in a mapper class)
        $request = [];
        $studentId = $args['id'];
        $teacherCourseId = $input['teacherCourseId'];

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->assign($teacherCourseId, $studentId, 'student');

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Course assignment has been created',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}