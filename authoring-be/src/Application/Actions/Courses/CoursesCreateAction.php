<?php

namespace App\Application\Actions\Courses;

use App\Domain\Courses\CoursesObject;
use App\Application\Actions\ActionPayload;
use App\Application\Actions\Courses\CoursesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class CoursesCreateAction extends CoursesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $course = new CoursesObject();
        $course->code = $input['code'];
        $course->title = $input['title'];

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->create($course);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Course has been created',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}