<?php

namespace App\Application\Actions\Courses;

use App\Application\Actions\Courses\CoursesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class CoursesListAction extends CoursesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Invoke the Domain with inputs and retain the result
        $courses = $this->service->list();

        // Transform the result into the JSON representation
        $response = [
            'courses' => $courses,
            'message' => 'Courses were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}