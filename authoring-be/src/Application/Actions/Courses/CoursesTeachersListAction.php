<?php

namespace App\Application\Actions\Courses;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Courses\CoursesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class CoursesTeachersListAction extends CoursesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Invoke the Domain with inputs and retain the result
        $courses = $this->service->coursesTeachersList();

        // Transform the result into the JSON representation
        $response = [
            'courses' => $courses,
            'message' => 'Courses were found',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}