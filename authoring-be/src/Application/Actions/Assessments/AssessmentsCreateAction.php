<?php

namespace App\Application\Actions\Assessments;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Assessments\AssessmentsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class AssessmentsCreateAction extends AssessmentsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $assessments = $this->request->getParsedBody();
        $tokenPayload = $this->attributes['tokenPayload'];
        $teacherId = $tokenPayload->extra->teacherId;
        
        // Invoke the Domain with inputs and retain the result
        $id = $this->service->create($assessments, $teacherId);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Assessment has been created',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}