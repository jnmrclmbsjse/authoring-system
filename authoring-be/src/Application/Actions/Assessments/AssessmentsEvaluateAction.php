<?php

namespace App\Application\Actions\Assessments;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Assessments\AssessmentsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class AssessmentsEvaluateAction extends AssessmentsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $args = $this->args;
        $studentId = $args['studentId'];
        $teacherCourseId = $args['teacherCourseId'];
        $input = $this->request->getParsedBody();
        $answers = $input['answers'];
        // $tokenPayload = $this->attributes['tokenPayload'];
        // $teacherId = $tokenPayload->extra->teacherId;
        
        // Invoke the Domain with inputs and retain the result
        $results = $this->service->evaluate($answers, $teacherCourseId, $studentId);

        // Transform the result into the JSON representation
        $response = [
            'results' => $results,
            'message' => 'Exam results were calculated',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}