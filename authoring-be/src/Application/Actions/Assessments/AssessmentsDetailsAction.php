<?php

namespace App\Application\Actions\Assessments;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Assessments\AssessmentsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class AssessmentsDetailsAction extends AssessmentsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $args = $this->args;
        $teacherCourseId = $args['id'];
        // $tokenPayload = $this->attributes['tokenPayload'];
        // $teacherId = $tokenPayload->extra->teacherId;
        
        // Invoke the Domain with inputs and retain the result
        $assessment = $this->service->details($teacherCourseId);

        // Transform the result into the JSON representation
        $response = [
            'assessment' => $assessment,
            'message' => 'Assessment was found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}