<?php
declare(strict_types=1);

namespace App\Application\Actions\Assessments;

use Psr\Log\LoggerInterface;
use App\Domain\Assessments\AssessmentsService;
use App\Application\Actions\Action;

abstract class AssessmentsAction extends Action
{
    /**
     * @var AssessmentsService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, AssessmentsService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 