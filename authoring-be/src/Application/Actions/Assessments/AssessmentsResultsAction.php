<?php

namespace App\Application\Actions\Assessments;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Assessments\AssessmentsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class AssessmentsResultsAction extends AssessmentsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $args = $this->args;
        $studentId = $args['studentId'];
        $teacherCourseId = $args['teacherCourseId'];
        
        // Invoke the Domain with inputs and retain the result
        $results = $this->service->getResults($teacherCourseId, $studentId);

        // Transform the result into the JSON representation
        $response = [
            'results' => $results,
            'message' => 'Exam results were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}