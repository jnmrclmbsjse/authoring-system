<?php

namespace App\Application\Actions\Habits;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Habits\HabitsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class HabitsCreateAction extends HabitsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $habits = $this->request->getParsedBody();
        // $tokenPayload = $this->attributes['tokenPayload'];
        // $teacherId = $tokenPayload->extra->teacherId;
        
        // Invoke the Domain with inputs and retain the result
        $id = $this->service->create($habits);

        if ($id == "undefined") {
            $payload = new ActionPayload(412, [
                "message" => "No active semester"
            ]);
            return $this->respond($payload);
        }

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Habit has been created',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}