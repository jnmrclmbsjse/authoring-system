<?php
declare(strict_types=1);

namespace App\Application\Actions\Habits;

use Psr\Log\LoggerInterface;
use App\Domain\Habits\HabitsService;
use App\Application\Actions\Action;

abstract class HabitsAction extends Action
{
    /**
     * @var HabitsService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, HabitsService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 