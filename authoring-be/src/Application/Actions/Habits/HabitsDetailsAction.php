<?php

namespace App\Application\Actions\Habits;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Habits\HabitsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class HabitsDetailsAction extends HabitsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Invoke the Domain with inputs and retain the result
        $habit = $this->service->details();

        // Transform the result into the JSON representation
        $response = [
            'habit' => $habit,
            'message' => 'Assessment was found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}