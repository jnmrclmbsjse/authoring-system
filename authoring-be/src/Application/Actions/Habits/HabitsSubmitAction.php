<?php

namespace App\Application\Actions\Habits;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Habits\HabitsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class HabitsSubmitAction extends HabitsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $args = $this->args;
        $studentId = $args['studentId'];
        $input = $this->request->getParsedBody();
        $answers = $input['answers'];
        
        // Invoke the Domain with inputs and retain the result
        $results = $this->service->submit($answers, $studentId);

        // Transform the result into the JSON representation
        $response = [
            'answers' => $results,
            'message' => 'Exam results were calculated',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}