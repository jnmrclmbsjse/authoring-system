<?php

namespace App\Application\Actions\Semesters;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Semesters\SemestersAction;
use Psr\Http\Message\ResponseInterface as Response;

final class SemestersCreateAction extends SemestersAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();
        $semester = [
            'title' => $input['title']
        ];
        // Invoke the Domain with inputs and retain the result
        $id = $this->service->create($semester);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Semester has been created',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}