<?php
declare(strict_types=1);

namespace App\Application\Actions\Semesters;

use Psr\Log\LoggerInterface;
use App\Domain\Semesters\SemestersService;
use App\Application\Actions\Action;

abstract class SemestersAction extends Action
{
    /**
     * @var SemestersService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, SemestersService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 