<?php

namespace App\Application\Actions\Semesters;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Semesters\SemestersAction;
use Psr\Http\Message\ResponseInterface as Response;

final class SemestersDeleteAction extends SemestersAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->args;
        $semesterId = $input['semesterId'];
        // Invoke the Domain with inputs and retain the result
        $id = $this->service->delete($semesterId);

        // Transform the result into the JSON representation
        $response = [
            'success' => true,
            'message' => 'Semester has been deleted',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(200, $response);
        return $this->respond($payload);
    }
}