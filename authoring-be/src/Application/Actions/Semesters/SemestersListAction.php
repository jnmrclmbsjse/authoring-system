<?php

namespace App\Application\Actions\Semesters;

use App\Application\Actions\Semesters\SemestersAction;
use Psr\Http\Message\ResponseInterface as Response;

final class SemestersListAction extends SemestersAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Invoke the Domain with inputs and retain the result
        $semesters = $this->service->list();

        // Transform the result into the JSON representation
        $response = [
            'semesters' => $semesters,
            'message' => 'Semesters were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}