<?php
declare(strict_types=1);

namespace App\Application\Actions\Roles;

use Psr\Log\LoggerInterface;
use App\Domain\Roles\RolesService;
use App\Application\Actions\Action;

abstract class RolesAction extends Action
{
    /**
     * @var RolesService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, RolesService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 