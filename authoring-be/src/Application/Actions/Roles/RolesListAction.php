<?php

namespace App\Application\Actions\Roles;

use App\Application\Actions\Roles\RolesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class RolesListAction extends RolesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Invoke the Domain with inputs and retain the result
        $roles = $this->service->list();

        // Transform the result into the JSON representation
        $response = [
            'roles' => $roles,
            'message' => 'Roles were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}