<?php
declare(strict_types=1);

namespace App\Application\Actions\Files;

use Psr\Log\LoggerInterface;
use App\Domain\Files\FilesService;
use App\Application\Actions\Action;

abstract class FilesAction extends Action
{
    /**
     * @var FilesService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, FilesService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 