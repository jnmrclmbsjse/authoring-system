<?php

namespace App\Application\Actions\Files;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Files\FilesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class FilesUploadAction extends FilesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $files = $this->request->getUploadedFiles();

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->upload($files);

        // Transform the result into the JSON representation
        $response = [
            'filename' => $id,
            'success' => true,
            'message' => 'File has been uploaded',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}