<?php

namespace App\Application\Actions\Files;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Files\FilesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class FilesDownloadAction extends FilesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getQueryParams();
        $file = $input['file'];
        $file = 'photos/'.$file;
        // Build the HTTP response
        return $this->respondWithOctet($file);
    }
}