<?php

namespace App\Application\Actions\Quizzes;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Quizzes\QuizzesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class QuizzesDetailsAction extends QuizzesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();
        $args = $this->args;
        $contentId = $args['id'];
        // Invoke the Domain with inputs and retain the result
        $details = $this->service->details($contentId);

        // Transform the result into the JSON representation
        $response = [
            'quiz' => $details,
            'message' => 'Quiz was found',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}