<?php

namespace App\Application\Actions\Quizzes;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Quizzes\QuizzesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class QuizzesEvaluateAction extends QuizzesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $args = $this->args;
        $studentId = $args['studentId'];
        $contentId = $args['contentId'];
        $files = $this->request->getUploadedFiles();
        $input = $this->request->getParsedBody();
        $answers = json_decode($input['answers'], true);
        // $tokenPayload = $this->attributes['tokenPayload'];
        // $teacherId = $tokenPayload->extra->teacherId;
        
        // Invoke the Domain with inputs and retain the result
        $results = $this->service->evaluate($answers, $contentId, $studentId, $files);

        // Transform the result into the JSON representation
        $response = [
            'results' => $results,
            'message' => 'Exam results were calculated',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}