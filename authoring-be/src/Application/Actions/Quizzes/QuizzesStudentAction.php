<?php

namespace App\Application\Actions\Quizzes;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Quizzes\QuizzesAction;
use Psr\Http\Message\ResponseInterface as Response;

final class QuizzesStudentAction extends QuizzesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();
        $args = $this->args;
        $studentId = $args['studentId'];
        // Invoke the Domain with inputs and retain the result
        $results = $this->service->listStudents($studentId);

        // Transform the result into the JSON representation
        $response = [
            'quizzes' => $results,
            'message' => 'Quiz was found',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}