<?php
declare(strict_types=1);

namespace App\Application\Actions\Quizzes;

use Psr\Log\LoggerInterface;
use App\Domain\Quizzes\QuizzesService;
use App\Application\Actions\Action;

abstract class QuizzesAction extends Action
{
    /**
     * @var QuizzesService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, QuizzesService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 