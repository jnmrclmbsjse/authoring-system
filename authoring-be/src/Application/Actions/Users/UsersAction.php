<?php
declare(strict_types=1);

namespace App\Application\Actions\Users;

use Psr\Log\LoggerInterface;
use App\Domain\Users\UsersService;
use App\Application\Actions\Action;

abstract class UsersAction extends Action
{
    /**
     * @var UsersService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, UsersService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
