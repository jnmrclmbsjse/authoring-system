<?php

namespace App\Application\Actions\Users;

use App\Application\Actions\Users\UsersAction;
use Psr\Http\Message\ResponseInterface as Response;

final class UsersListAction extends UsersAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Invoke the Domain with inputs and retain the result
        $users = $this->service->list();

        // Transform the result into the JSON representation
        $response = [
            'users' => $users,
            'message' => 'Users were found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}