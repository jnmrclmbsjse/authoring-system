<?php

namespace App\Application\Actions\Users;

use App\Domain\Users\UsersObject;
use App\Application\Actions\ActionPayload;
use App\Application\Actions\Users\UsersAction;
use Psr\Http\Message\ResponseInterface as Response;

final class UsersCreateAction extends UsersAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $user = new UsersObject();
        $user->username = $input['username'];
        $user->password = $input['password'];
        $user->role = $input['role'];

        $details = [];
        if ($user->role == "2") {
            $details['firstname'] = $input['firstname'];
            $details['lastname'] = $input['lastname'];
            $details['mobile'] = $input['mobile'];
        }

        if ($user->role == "3") {
            $details['firstname'] = $input['firstname'];
            $details['middlename'] = $input['middlename'];
            $details['lastname'] = $input['lastname'];
            $details['mobile'] = $input['mobile'];
        }

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->create($user, $details);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'User has been created',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(201, $response);
        return $this->respond($payload);
    }
}