<?php

namespace App\Application\Actions\Users;

use App\Domain\Users\UsersObject;
use App\Application\Actions\ActionPayload;
use App\Application\Actions\Users\UsersAction;
use Psr\Http\Message\ResponseInterface as Response;

final class UsersAuthenticateAction extends UsersAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $user = new UsersObject();
        $user->username = $input['username'];
        $user->password = $input['password'];

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->authenticate($user);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'User has been authenticated',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}