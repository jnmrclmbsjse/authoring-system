<?php

namespace App\Application\Actions\Users;

use App\Application\Actions\ActionPayload;
use App\Application\Actions\Users\UsersAction;
use Psr\Http\Message\ResponseInterface as Response;

final class UsersDeleteAction extends UsersAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $args = (array)$this->args;

        // Mapping (should be done in a mapper class)
        $userId = $args['userId'];

        // Invoke the Domain with inputs and retain the result
        $success = $this->service->delete($userId);

        // Transform the result into the JSON representation
        $response = [
            'success' => $success,
            'message' => 'User has been deleted',
        ];

        // Build the HTTP response
        $payload = new ActionPayload(200, $response);
        return $this->respond($payload);
    }
}