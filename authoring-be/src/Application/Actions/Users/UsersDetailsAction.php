<?php

namespace App\Application\Actions\Users;

use App\Application\Actions\Users\UsersAction;
use Psr\Http\Message\ResponseInterface as Response;

final class UsersDetailsAction extends UsersAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->args;

        // Mapping (should be done in a mapper class)
        $id = $input['id'];

        // Invoke the Domain with inputs and retain the result
        $details = $this->service->details($id);

        // Transform the result into the JSON representation
        $response = [
            'details' => $details,
            'message' => 'User was found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}