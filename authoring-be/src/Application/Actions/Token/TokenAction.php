<?php
declare(strict_types=1);

namespace App\Application\Actions\Token;

use Psr\Log\LoggerInterface;
use App\Domain\Token\TokenService;
use App\Application\Actions\Action;

abstract class TokenAction extends Action
{
    /**
     * @var TokenService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, TokenService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 