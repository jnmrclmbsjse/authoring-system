<?php

namespace App\Application\Actions\Token;

use App\Domain\Token\TokenObject;
use Psr\Http\Message\ResponseInterface as Response;

final class TokenEncodeAction extends TokenAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getParsedBody();

        $payload = new TokenObject();
        $payload->role = $input['role'];
        $payload->username = $input['username'];
        $payload->id = $input['id'];
        $payload->extra = $input['extra'];

        $token = $this->service->encode($payload);

        $response = [
            'token' => $token,
            'message' => 'Token has been generated',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}