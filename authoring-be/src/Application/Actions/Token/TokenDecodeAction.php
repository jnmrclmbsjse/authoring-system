<?php

namespace App\Application\Actions\Token;

use App\Domain\Token\TokenObject;
use Psr\Http\Message\ResponseInterface as Response;

final class TokenDecodeAction extends TokenAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->request->getQueryParams();

        $token = isset($input['token']) ? $input['token'] : null;

        $data = $this->service->decode($token);

        $response = [
            'payload' => $data,
            'message' => 'Token has been decoded',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}