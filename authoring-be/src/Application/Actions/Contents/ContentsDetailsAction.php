<?php

namespace App\Application\Actions\Contents;

use App\Application\Actions\Contents\ContentsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class ContentsDetailsAction extends ContentsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->args;
        $tokenPayload = $this->attributes['tokenPayload'];
        $studentId = $tokenPayload->extra->studentId;

        // Mapping (should be done in a mapper class)
        $id = $input['id'];

        // Invoke the Domain with inputs and retain the result
        $content = $this->service->details($id, $studentId);

        // Transform the result into the JSON representation
        $response = [
            'content' => $content,
            'message' => 'Content was found',
        ];

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}