<?php

namespace App\Application\Actions\Contents;

use App\Application\Actions\Contents\ContentsAction;
use Psr\Http\Message\ResponseInterface as Response;

final class ContentsViewedAction extends ContentsAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        // Collect input from the HTTP request
        $input = (array)$this->args;

        // Mapping (should be done in a mapper class)
        $studentId = $input['studentId'];
        $contentId = $input['contentId'];

        // Invoke the Domain with inputs and retain the result
        $id = $this->service->viewed($studentId, $contentId);

        // Transform the result into the JSON representation
        $response = [
            'id' => $id,
            'message' => 'Content was viewed',
        ];

        if (!$id) {
            $response = [
                'id' => null,
                'message' => 'Content was not viewed'
            ];
        }

        // Build the HTTP response
        return $this->respondWithData($response);
    }
}