<?php
declare(strict_types=1);

namespace App\Application\Actions\Contents;

use Psr\Log\LoggerInterface;
use App\Domain\Contents\ContentsService;
use App\Application\Actions\Action;

abstract class ContentsAction extends Action
{
    /**
     * @var ContentsService
     */
    protected $service;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, ContentsService $service)
    {
        parent::__construct($logger);
        $this->service = $service;
    }
}
 