<?php
declare(strict_types=1);

namespace App\Application\Middleware;

use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Server\MiddlewareInterface as Middleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

class TokenMiddleware implements Middleware
{
    /**
     * {@inheritdoc}
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        $session = $request->getHeader('X-Session-Token');
        $token = null;
        if (isset($session[0]) && $session[0] !== 'undefined') {
            $token = $session[0];
            $payload = JWT::decode($token, getenv('jwt_secret'), ['HS256']);
            $request = $request->withAttribute('tokenPayload', $payload);
        }
        return $handler->handle($request);
    }
}
