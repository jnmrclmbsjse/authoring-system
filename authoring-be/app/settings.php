<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'uploadDirectory' => './photos',
            'displayErrorDetails' => (bool)getenv('displayErrorDetails') || true, // Should be set to false in production
            'logger' => [
                'name' => getenv('name'),
                'path' => getenv('path'),
                'level' => Logger::DEBUG,
            ],
            'database' => [
                'driver' => getenv('dbdriver'),
                'host' => getenv('databasehost'),
                'port' => getenv('databaseport'),
                'username' => getenv('databaseusername'),
                'password' => getenv('databasepassword'),
                'dbname' => getenv('databasename'),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci'
            ],
        ],
    ]);
};
