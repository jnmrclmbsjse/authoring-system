<?php
declare(strict_types=1);

use Monolog\Logger;
use DI\ContainerBuilder;
use ParagonIE\EasyDB\EasyDB;
use Psr\Log\LoggerInterface;
use ParagonIE\EasyDB\Factory;
use Monolog\Handler\StreamHandler;
use PHPassLib\Application\Context;
use Monolog\Processor\UidProcessor;
use App\Interfaces\SettingsInterface;
use Psr\Container\ContainerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        EasyDB::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');
            $config = $settings['database'];

            $host = $config['host'];
            $dbname =  $config['dbname'];
            $username = $config['username'];
            $password = $config['password'];
            $charset = $config['charset'];
            $dsn = "mysql:host=$host;dbname=$dbname";

            $connection = Factory::fromArray([
                $dsn,
                $username,
                $password
            ]);
            return $connection;
        },
        Context::class => function () {
            $context = new Context;
            $context->addConfig('bcrypt');

            return $context;
        },
        SettingsInterface::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');
            return $settings;
        }
    ]);
};
