<?php
declare(strict_types=1);

use Slim\App;
use Tuupola\Middleware\JwtAuthentication;
use App\Application\Middleware\SessionMiddleware;

return function (App $app) {
    $jwt = new JwtAuthentication([
        'secret' => getenv('jwt_secret'),
        'path' => ['/api'],
        'ignore' => ['/api/token', '/api/auth'],
        'header' => 'X-User-Token'
    ]);

    $app->add(SessionMiddleware::class);
};
