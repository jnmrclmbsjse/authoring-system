<?php
declare(strict_types=1);

use Slim\App;
use App\Application\Middleware\TokenMiddleware;
use App\Application\Actions\Logs\LogsListAction;
use App\Application\Actions\Nodes\NodesDoneAction;

use App\Application\Actions\Roles\RolesListAction;
use App\Application\Actions\Users\UsersListAction;
use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Actions\Files\FilesUploadAction;
use App\Application\Actions\Nodes\NodesCreateAction;
use App\Application\Actions\Token\TokenDecodeAction;
use App\Application\Actions\Token\TokenEncodeAction;
use App\Application\Actions\Users\UsersCreateAction;
use App\Application\Actions\Users\UsersDeleteAction;
use App\Application\Actions\Nodes\NodesDetailsAction;
use App\Application\Actions\Users\UsersDetailsAction;
use App\Application\Actions\Courses\CoursesListAction;
use App\Application\Actions\Files\FilesDownloadAction;
use App\Application\Actions\Habits\HabitsCreateAction;
use App\Application\Actions\Habits\HabitsSubmitAction;
use App\Application\Actions\Habits\HabitsDetailsAction;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Application\Actions\Concepts\ConceptsListAction;
use App\Application\Actions\Courses\CoursesCreateAction;
use App\Application\Actions\Courses\CoursesDeleteAction;
use App\Application\Actions\Courses\CoursesUpdateAction;
use App\Application\Actions\Nodes\NodesViewedListAction;
use App\Application\Actions\Quizzes\QuizzesCreateAction;
use App\Application\Actions\Courses\CoursesDetailsAction;
use App\Application\Actions\Quizzes\QuizzesDetailsAction;
use App\Application\Actions\Quizzes\QuizzesStudentAction;
use App\Application\Actions\Concepts\ConceptsCreateAction;
use App\Application\Actions\Concepts\ConceptsUpdateAction;
use App\Application\Actions\Contents\ContentsViewedAction;
use App\Application\Actions\Courses\CoursesConceptsAction;
use App\Application\Actions\Quizzes\QuizzesEvaluateAction;
use App\Application\Actions\Semesters\SemestersListAction;
use App\Application\Actions\Users\UsersAuthenticateAction;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use App\Application\Actions\Concepts\ConceptsDetailsAction;
use App\Application\Actions\Contents\ContentsDetailsAction;
use App\Application\Actions\Semesters\SemestersCreateAction;
use App\Application\Actions\Semesters\SemestersDeleteAction;
use App\Application\Actions\Courses\CoursesTeachersListAction;
use App\Application\Actions\Courses\CoursesAssignStudentAction;
use App\Application\Actions\Courses\CoursesAssignTeacherAction;
use App\Application\Actions\Assessments\AssessmentsCreateAction;
use App\Application\Actions\Assessments\AssessmentsDetailsAction;
use App\Application\Actions\Assessments\AssessmentsResultsAction;
use App\Application\Actions\Assessments\AssessmentsEvaluateAction;
use App\Application\Actions\Logs\LogsStudentsAction;

return function (App $app) {
    $app->options('/{routes:.+}', function ($request, $response, $args) {
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write(json_encode($_SERVER));
        return $response;
    });
    
    $app->group('/api', function (Group $group) {
        $group->post('/auth', UsersAuthenticateAction::class);

        $group->group('/teachers', function (Group $group) {
            $group->post('/{id}/courses', CoursesAssignTeacherAction::class);
        });

        $group->group('/students', function (Group $group) {
            $group->post('/{id}/courses', CoursesAssignStudentAction::class);
            $group->post('/{studentId}/nodes/{nodeId}', NodesDoneAction::class);
            $group->get('/{studentId}/nodes', NodesViewedListAction::class);
            $group->get('/{studentId}/logs', LogsStudentsAction::class);
            $group->get('/{studentId}/quizzes', QuizzesStudentAction::class);
            $group->get('/{studentId}/contents/{contentId}', ContentsViewedAction::class);
            $group->post('/{studentId}/assessments/{teacherCourseId}', AssessmentsEvaluateAction::class);
            $group->get('/{studentId}/assessments/{teacherCourseId}', AssessmentsResultsAction::class);
            $group->post('/{studentId}/contents/{contentId}/quizzes', QuizzesEvaluateAction::class);
            $group->post('/{studentId}/habits', HabitsSubmitAction::class);
        }); 

        $group->group('/users', function (Group $group) {
            $group->post('', UsersCreateAction::class);
            $group->get('', UsersListAction::class);
            $group->delete('/{userId}', UsersDeleteAction::class);
            $group->get('/{id}', UsersDetailsAction::class);
        });
    
        $group->group('/token', function (Group $group) {
            $group->post('', TokenEncodeAction::class);
            $group->get('', TokenDecodeAction::class);
        });

        $group->group('/roles', function (Group $group) {
            $group->get('', RolesListAction::class);
        });

        $group->group('/logs', function (Group $group) {
            $group->get('', LogsListAction::class);
        });

        $group->group('/courses', function (Group $group) {
            $group->get('', CoursesListAction::class);
            $group->get('/teachers', CoursesTeachersListAction::class);
            $group->get('/{courseId}', CoursesDetailsAction::class);
            $group->put('/{courseId}', CoursesUpdateAction::class);
            $group->delete('/{courseId}', CoursesDeleteAction::class);
            $group->get('/{id}/concepts', CoursesConceptsAction::class);
            $group->post('', CoursesCreateAction::class);
        });

        $group->group('/concepts', function (Group $group) {
            $group->get('', ConceptsListAction::class);
            $group->get('/{id}', ConceptsDetailsAction::class);
            $group->put('/{id}', ConceptsUpdateAction::class);
            $group->post('', ConceptsCreateAction::class);
        });

        $group->group('/nodes', function (Group $group) {
            $group->get('/{id}', NodesDetailsAction::class);
            $group->post('', NodesCreateAction::class);
        });

        $group->group('/files', function (Group $group) {
            $group->get('', FilesDownloadAction::class);
            $group->post('', FilesUploadAction::class);
        });

        $group->group('/contents', function (Group $group) {
            $group->get('/{id}', ContentsDetailsAction::class);
            $group->post('/{id}/quizzes', QuizzesCreateAction::class);
            $group->get('/{id}/quizzes', QuizzesDetailsAction::class);
        });

        $group->group('/semesters', function (Group $group) {
            $group->post('', SemestersCreateAction::class);
            $group->get('', SemestersListAction::class);
            $group->delete('/{semesterId}', SemestersDeleteAction::class);
        });

        $group->group('/assessments', function (Group $group) {
            $group->get('/{id}', AssessmentsDetailsAction::class);
            $group->post('', AssessmentsCreateAction::class);
        });

        $group->group('/habits', function (Group $group) {
            $group->post('', HabitsCreateAction::class);
            $group->get('', HabitsDetailsAction::class);

        });
    })->add(new TokenMiddleware());
    
};
