import {Component, OnInit } from '@angular/core';
import { navItems as navAdmin } from '@app/navs/_nav.admin';
import { navItems as navTeacher } from '@app/navs/_nav.teacher';
import { navItems as navStudent } from '@app/navs/_nav.student';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { TokenRepository } from '@repository/token';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = [];
  public token = null;
  public username = "";

  constructor(
    private storage: LocalStorageHelper,
    private tokenRepository: TokenRepository,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { 
    this.token = this.storage.retrieve('a-ext-token');
  }

  async ngOnInit() {
    if (typeof this.token == 'undefined') {
      this.router.navigate(['/login']);
      return;
    }

    try {
      const tokenResponse = await this.tokenRepository.decode(this.token);
      const { role, extra, username } = tokenResponse;
      this.username = username;
      if (role.toLowerCase() === "administrator") {
        this.navItems = navAdmin;
      }
  
      if (role.toLowerCase() === "teachers") {
        this.navItems = navTeacher;
      }
  
      if (role.toLowerCase() === "students") {
        this.navItems = navStudent;
        const assessed = extra.assessed;
        if (!assessed) {
          this.router.navigate(['/habits/t']);
        }

        if (this.activatedRoute.snapshot.data.title === "Dashboard") {
          this.router.navigate(['/interims']);
        }
      }
    } catch (e) {
      this.storage.remove('a-ext-token');
      this.router.navigate(['/login']);
    }
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  logout() {
    this.storage.remove('a-ext-token');
    this.router.navigate(['/login']);
  }
}
