import { Injectable, ElementRef } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class PredictiveService {
    public threshold = {
        5: [
            {
                description: 'Exceptional',
                lesson: 5,
                equivalent: 'A*',
                min: 95,
                max: 100
            }
        ],
        4: [
            {
                description: 'Excelent',
                lesson: 4,
                equivalent: 'A',
                min: 90,
                max: 94
            }
        ],
        3: [
            {
                description: 'Good',
                lesson: 3,
                equivalent: 'B',
                min: 85,
                max: 89
            }
        ],
        2: [
            {
                description: 'Average',
                lesson: 2,
                equivalent: 'C',
                min: 80,
                max: 84
            }
        ],
        1: [
            {
                description: 'Mediocre',
                lesson: 1,
                equivalent: 'D',
                min: 75,
                max: 79
            },
            {
                description: 'Poor',
                lesson: 1,
                equivalent: 'E',
                min: 70,
                max: 74
            },
            {
                description: 'Very Poor',
                lesson: 1,
                equivalent: 'F',
                min: 65,
                max: 69
            }
        ]
    }

    constructor () {}

    analyzeLevel(percentage) {
        let lesson = 1;
        percentage = parseInt(percentage);
        for (let [key, descriptions] of Object.entries(this.threshold)) {
            descriptions.forEach(description => {
                if (percentage >= description.min && percentage <= description.max) {
                    lesson = description.lesson;
                }
            });
        }

        // Fallback to level 1 if logic above fails
        return lesson;
    }

    getCellLevel(origin) {
        let cell = origin;
        let level = 0;
        let halt = false;
        while (cell.getEdgeCount() !== 0 && !halt) {
            const source = cell.getEdgeAt(0).source;
            if (source != null && typeof source !== 'undefined' && source !== cell) {
                cell = source;
                level++;
            } else {
                halt = true;
            }
        }
        return level;
    }

    getRecommendations(graph, recommendedLevel) {
        let recommendations = [];
        const vertices = graph.getChildVertices()
        vertices.forEach(vertex => {
            const level = this.getCellLevel(vertex);
            if (level == recommendedLevel) {
                recommendations.push(vertex.getValue());
            }
        });

        return recommendations;
    }

    getPreRequisite(origin) {
        const source = origin.getEdgeAt(0).source;
        if (source == origin) {
            return undefined;
        }
        return source;
    }

    getPostRequisite(origin) {
        const target = origin.getEdgeAt(1) !== undefined ? origin.getEdgeAt(1).target : undefined;
        if (target == origin) {
            return undefined;
        }
        return target;
    }
}