import { Inject, Injectable } from '@angular/core';
import { LocalStorageHelper } from './localstorage.service';
import { TokenRepository } from '@app/repository/token';
import { UsersRepository } from '@app/repository/users';

@Injectable()
export class TokenService {
    private storage;
    private tokenRepository;
    private userRepository;
    private key = "a-ext-token";
    constructor(
        storage: LocalStorageHelper,
        tokenRepository: TokenRepository,
        userRepository: UsersRepository) {
        this.storage = storage;
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
    }

    public async refresh() {
        const outdatedToken = this.storage.retrieve(this.key);
        const { id: userId } = await this.tokenRepository.decode(outdatedToken);
        const payload = await this.buildPayload(userId);
        const token = await this.tokenRepository.encode(payload);
        this.storage.remove(this.key);
        this.storage.store(this.key, token);

    }

    public async create(id: object) {
        const payload = await this.buildPayload(id);
        const token = await this.tokenRepository.encode(payload);
        this.storage.store(this.key, token);
    }

    private async buildPayload(userId) {
      const details = await this.userRepository.getUserDetails(userId);
      const { info } = details;
      let extra = {};
      if (details.role.toLowerCase() === "teachers") {
        extra = {
          teacherId: info.teacherId
        };

        this.storage.store("course", info.courseId);
      }

      if (details.role.toLowerCase() === "students") {
        extra = {
          studentId: info.studentId,
          assessed: info.assessed,
          firstname: info.firstname,
          lastname: info.lastname,
        };
        this.storage.store("course", info.teacherCourseId);
      }

      const payload = {
        id: userId,
        role: details.role,
        username: details.username,
        extra
      };

      return payload;
    }
}