import { Injectable, ElementRef } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async postFile(file: File) {
    const form: FormData = new FormData();
    form.append('file', file, file.name);
    let headers = new Headers();
    /** In Angular 5, including the header Content-Type can invalidate your request */
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const { data } = await this.client.post('/files', form, {
      headers
    }); 
    const { data: { filename, success } } = data;

    return filename;
  }
}