import { Injectable, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { BehaviorSubject } from 'rxjs';
import {
  MxCell,
  MxGraph,
  MxEventObject,
  MXUTILITIES,
  MxGraphSelectionModel,
  MxPoint,
  MxGraphHandler,
  MxToolbar,
  MxGeometry,
  MxUndoManager,
  MxKeyHandler,
  MxConnectionHandler,
  MxHierarchicalLayout
} from '@helpers/mxgraph.class';

import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class GraphsService {

  private graphsSubject: BehaviorSubject < MxGraph[] > = new BehaviorSubject([]);
  private graphs$: Observable < MxGraph[] > ;
  private graphs: MxGraph[] = [];

  private selectedCellsSubject: BehaviorSubject < MxCell[] > = new BehaviorSubject([]);
  private selectedCells: MxCell[];

  private toolbar = null;
  constructor() {
    this.graphs$ = this.graphsSubject.asObservable();
  }

  /**
   * Generate a new graph into the received container
   *
   * @memberOf GraphsService
   */
  initGraph(graphContainer: ElementRef, toolbarContainer?: ElementRef, name ? : string, isContent = false): MxGraph {
    const graph: MxGraph = this.createGraph(graphContainer, toolbarContainer, name, isContent);
    this.graphs.push(graph);
    this.graphsSubject.next(this.graphs);
    return graph;
  }

  /**
   * Init new graph
   *
   * @memberOf GraphsService
   */
  private createGraph(graphContainer: ElementRef, toolbarContainer?: ElementRef, name ?: string, isContent = false) {
    MXUTILITIES.mxEvent.disableContextMenu(graphContainer.nativeElement);

    let graph: MxGraph;
    graph = new MxGraph(graphContainer.nativeElement);
    if (toolbarContainer) {
      this.toolbar = new MxToolbar(toolbarContainer.nativeElement);
    }

    // newGraph = this.overrideFunctions(newGraph);

    if (!name) name = 'concept-map';
    graph.getModel().getRoot().setValue(name);
    graph.setConnectable(true);
    graph.setCellsResizable(false);
    graph.setMultigraph(false);
    graph.setPanning(false);
    graph.panningHandler.useLeftButtonForPanning = true;

    // Multi-select
    // new MxRubberband(graph);
    new MxConnectionHandler(graph); 

    // Delete button handler
    const keyHandler = new MxKeyHandler(graph);
    keyHandler.bindKey(46, (evt) => {
      if (graph.isEnabled())
      {
        graph.removeCells();
      }
    });

    // Undo manager
    const undoManager = new MxUndoManager(50);
    const listener = (sender, evt) => {
      undoManager.undoableEditHappened(evt.getProperty('edit'));
    };
    graph.getModel().addListener(MXUTILITIES.mxEvent.UNDO, listener);
    graph.getView().addListener(MXUTILITIES.mxEvent.UNDO, listener);

    const undo = MXUTILITIES.mxUtils.button('Undo', () => {
      undoManager.undo();
    });

    const redo = MXUTILITIES.mxUtils.button('Redo', () => {
      undoManager.undo();
    });

    const zoomIn = MXUTILITIES.mxUtils.button('Zoom in', () => {
      graph.zoomIn();
    });

    const zoomOut = MXUTILITIES.mxUtils.button('Zoom out', () => {
      graph.zoomOut();
    });

    const arrange = MXUTILITIES.mxUtils.button('Arrange', () => {
      new MxHierarchicalLayout(graph).execute(graph.getDefaultParent());
    });

    keyHandler.bindControlKey(90, (evt) => {
      undoManager.undo();
    });

    keyHandler.bindControlKey(89, (evt) => {
      undoManager.redo();
    });

    keyHandler.bindControlShiftKey(90, (evt) => {
      undoManager.redo();
    });
    if (toolbarContainer) {
      toolbarContainer.nativeElement.appendChild(undo);
      toolbarContainer.nativeElement.appendChild(redo);
      toolbarContainer.nativeElement.appendChild(zoomIn);
      toolbarContainer.nativeElement.appendChild(zoomOut);
      toolbarContainer.nativeElement.appendChild(arrange);
    }

    // Add default box on toolbar
    if (toolbarContainer && !isContent) {
      graph = this.addVertex(graph, 'assets/img/icons/rectangle.png', 200, 40, '');
    }
    graph = this.addStyles(graph);
    graph.selectionModel.addListener(MXUTILITIES.mxEvent.CHANGE, (mxGraphSelectionModel: MxGraphSelectionModel, evt: MxEventObject) => {
      this.emitSelectedCell(mxGraphSelectionModel.cells as MxCell[]);
    });

    return graph;
  }

  private addStyles(graph) {
    const imageStyle = new Object();
    imageStyle[MXUTILITIES.mxConstants.STYLE_SHAPE] = MXUTILITIES.mxConstants.SHAPE_IMAGE;
    imageStyle[MXUTILITIES.mxConstants.STYLE_PERIMETER] = MXUTILITIES.mxPerimeter.RectanglePerimeter;
    imageStyle[MXUTILITIES.mxConstants.STYLE_IMAGE] = 'assets/img/icons/file.png';
    imageStyle[MXUTILITIES.mxConstants.STYLE_FONTCOLOR] = 'rgba(0,0,0,0)';
    graph.getStylesheet().putCellStyle('image', imageStyle);

    const identifierStyle = new Object();
    identifierStyle[MXUTILITIES.mxConstants.STYLE_FONTCOLOR] = 'rgba(0,0,0,0)';
    graph.getStylesheet().putCellStyle('identifier', identifierStyle);

    const quizStyle = new Object();
    quizStyle[MXUTILITIES.mxConstants.STYLE_SHAPE] = MXUTILITIES.mxConstants.SHAPE_IMAGE;
    quizStyle[MXUTILITIES.mxConstants.STYLE_PERIMETER] = MXUTILITIES.mxPerimeter.RectanglePerimeter;
    quizStyle[MXUTILITIES.mxConstants.STYLE_IMAGE] = 'assets/img/icons/quiz.png';
    quizStyle[MXUTILITIES.mxConstants.STYLE_FONTCOLOR] = 'rgba(0,0,0,0)';
    graph.getStylesheet().putCellStyle('quiz', quizStyle);

    return graph;
  }

  /**
   * Emits the selected cells from the graph
   * @memberOf GraphsService
   */
  private emitSelectedCell(cells: MxCell[]) {

    if (!cells) this.selectedCells = [];
    else this.selectedCells = cells;
    this.selectedCellsSubject.next(this.selectedCells);
  }

  public addVertex(graph, icon, w, h, style, type = 'node') {
    let toolbar = this.toolbar;
    const vertex = new MxCell(null, new MxGeometry(0, 0, w, h), style);
    vertex.setAttribute('type', type);
    vertex.setVertex(true);

    const img = this.addToolbarItem(graph, toolbar, vertex, icon);
    img.enabled = true;

    graph.getSelectionModel().addListener(MXUTILITIES.mxEvent.CHANGE, () => {
      const tmp = graph.isSelectionEmpty();
      MXUTILITIES.mxUtils.setOpacity(img, (tmp) ? 100 : 20);
      img.enabled = tmp;
    });

    return graph;
  }

  public addCustomVertex(graph, icon, w, h, style, type = 'node') {
    let toolbar = this.toolbar;
    var xml = MXUTILITIES.mxUtils.createXmlDocument();
    var node = xml.createElement(type.toUpperCase());
    node.setAttribute('type', type);
    const vertex = new MxCell(node, new MxGeometry(0, 0, w, h), style);
    vertex.setVertex(true);

    const img = this.addToolbarItem(graph, toolbar, vertex, icon, true);
    img.enabled = true;

    graph.getSelectionModel().addListener(MXUTILITIES.mxEvent.CHANGE, () => {
      const tmp = graph.isSelectionEmpty();
      MXUTILITIES.mxUtils.setOpacity(img, (tmp) ? 100 : 20);
      img.enabled = tmp;
    });

    return graph;
  }

  private addToolbarItem(graph, toolbar, prototype, image, isCustom = false) {
    // Function that is executed when the image is dropped on
    // the graph. The cell argument points to the cell under
    // the mousepointer if there is one.
    const funct = (graph, evt, cell, x, y) => {
      graph.stopEditing(false);
      const vertex = graph.getModel().cloneCell(prototype);
      if (isCustom) {
        const type = prototype.getAttribute("type");
        const label = graph.insertVertex(vertex, null, type, 0.5, 1, 0, 0, 'identifier', true);
        label.setConnectable(false);
        if (type === "quiz" || type === "final") {

        } else {
          
        }
      }

      vertex.geometry.x = x;
      vertex.geometry.y = y;
      vertex.setId(uuid.v4());
      graph.addCell(vertex);
      graph.setSelectionCell(vertex);
    }

    // Creates the image which is used as the drag icon (preview)
    const img = toolbar.addMode(null, image, (evt, cell) => {
      const pt = graph.getPointForEvent(evt);
      funct(graph, evt, cell, pt.x, pt.y);
    });

    // Disables dragging if element is disabled. This is a workaround
    // for wrong event order in IE. Following is a dummy listener that
    // is invoked as the last listener in IE.
    MXUTILITIES.mxEvent.addListener(img, 'mousedown', (evt) => {
      // do nothing
    });

    // This listener is always called first before any other listener
    // in all browsers.
    MXUTILITIES.mxEvent.addListener(img, 'mousedown', (evt) => {
      if (img.enabled == false) {
        MXUTILITIES.mxEvent.consume(evt);
      }
    });

    MXUTILITIES.mxUtils.makeDraggable(img, graph, funct);

    return img;
  }
  /* Custom functions */

  /**
   * returns the relative position of the given child
   */
  private overrideFunctions(graph) {
    /* Custom configs */
    graph.translateCell = function (cell, dx, dy) {
      const rel = getRelativePosition(this.view.getState(cell), dx * graph.view.scale, dy * graph.view.scale);

      if (rel != null) {
        let geo = this.model.getGeometry(cell);

        if (geo != null && geo.relative) {
          geo = geo.clone();
          geo.x = rel.x;
          geo.y = rel.y;

          this.model.setGeometry(cell, geo);
        }
      } else {
        MxGraph.prototype.translateCell.apply(this, arguments);
      }
    };

    function getRelativePosition(state, dx, dy): MxPoint {
      if (state != null) {
        const model = graph.getModel();
        const geo = model.getGeometry(state.cell);

        if (geo != null && geo.relative && !model.isEdge(state.cell)) {
          const parent = model.getParent(state.cell);

          if (model.isVertex(parent)) {
            const pstate = graph.view.getState(parent);

            if (pstate != null) {
              const scale = graph.view.scale;
              let x = state.x + dx;
              let y = state.y + dy;

              if (geo.offset != null) {
                x -= geo.offset.x * scale;
                y -= geo.offset.y * scale;
              }

              x = (x - pstate.x) / pstate.width;
              y = (y - pstate.y) / pstate.height;

              if (Math.abs(y - 0.5) <= Math.abs((x - 0.5) / 2)) {
                x = (x > 0.5) ? 1 : 0;
                y = Math.min(1, Math.max(0, y));
              } else {
                x = Math.min(1, Math.max(0, x));
                y = (y > 0.5) ? 1 : 0;
              }

              return new MxPoint(x, y);
            }
          }
        }
      }
      return null;
    }

    // Replaces move preview for relative children
    graph.graphHandler.getDelta = function (me) {
      const point = MXUTILITIES.mxUtils.convertPoint(this.graph.container, me.getX(), me.getY());
      let delta = new MxPoint(point.x - this.first.x, point.y - this.first.y);

      if (this.cells != null && this.cells.length > 0 && this.cells[0] != null) {
        const state = this.graph.view.getState(this.cells[0]);
        const rel = getRelativePosition(state, delta.x, delta.y);

        if (rel != null) {
          const pstate = this.graph.view.getState(this.graph.model.getParent(state.cell));

          if (pstate != null) {
            delta = new MxPoint(pstate.x + pstate.width * rel.x - state.getCenterX(),
              pstate.y + pstate.height * rel.y - state.getCenterY());
          }
        }
      }
      return delta;
    };

    // Relative children cannot be removed from parent
    graph.graphHandler.shouldRemoveCellsFromParent = function(parent, cells, evt)
    {
      return cells.length == 0 && !cells[0].geometry.relative && MxGraphHandler.prototype.shouldRemoveCellsFromParent.apply(this, arguments);
    };
    
    // Enables moving of relative children
    graph.isCellLocked = function(cell)
    {
      return false;
    };

    return graph;
  }

  
}