import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable()
export class LocalStorageHelper {
     constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

     public store (key: string, data): void {
        this.storage.set(key, data);
        console.log(this.storage.get(key) || 'Local storage not found.');
     }

     public retrieve (key: string): any {
        return this.storage.get(key);
     }

     public remove (key: string): void {
         this.storage.remove(key);
     }
}