import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
{
  title: true,
  name: 'Academics'
},
{
  name: 'Interim report',
  url: '/interims',
  icon: 'icon-speedometer'
},
{
  name: 'Concept map',
  url: '/map',
  icon: 'icon-speedometer'
},
{
  name: 'Grade Book',
  url: '/results',
  icon: 'icon-speedometer'
},];