import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [{
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    title: true,
    name: 'System Management'
  },
  {
    name: 'Habits',
    url: '/habits/c',
    icon: 'icon-speedometer'
  },
  {
    name: 'Users',
    url: '/users',
    icon: 'icon-puzzle',
    children: [{
        name: 'Manage users',
        url: '/users',
        icon: 'icon-puzzle'
      },
      {
        name: 'Create a new user',
        url: '/users/c',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Semesters',
    url: '/semesters',
    icon: 'icon-puzzle',
    children: [{
        name: 'Manage semesters',
        url: '/semesters',
        icon: 'icon-puzzle'
      },
      {
        name: 'Create a semester',
        url: '/semesters/c',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Courses',
    url: '/courses',
    icon: 'icon-puzzle',
    children: [{
        name: 'Manage courses',
        url: '/courses',
        icon: 'icon-puzzle'
      },
      {
        name: 'Create a new course',
        url: '/courses/c',
        icon: 'icon-puzzle'
      }
    ]
  }
];
