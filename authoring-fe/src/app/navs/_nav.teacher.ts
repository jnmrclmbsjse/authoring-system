import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [{
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'My Students',
    url: '/students',
    icon: 'icon-speedometer'
  },
  {
    name: 'Assessment',
    url: '/assessments/c',
    icon: 'icon-speedometer'
  },
  {
    title: true,
    name: 'Course Management'
  },
  {
    name: 'Concept maps',
    url: '/concepts',
    icon: 'icon-puzzle',
    children: [{
        name: 'Manage concept maps',
        url: '/concepts',
        icon: 'icon-puzzle'
      },
      {
        name: 'Concept map',
        url: '/concepts/c',
        icon: 'icon-puzzle'
      }
    ]
  }
];
