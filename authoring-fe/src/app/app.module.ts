import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from '@app/app.component';

// Import containers
import { DefaultLayoutComponent } from '@containers/index';

import { P404Component } from '@views/error/404.component';
import { P500Component } from '@views/error/500.component';
import { LoginComponent } from '@views/login/login.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from '@app/app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';

import { StorageServiceModule } from 'ngx-webstorage-service';

// Repositories
import { UsersRepository } from '@repository/users';
import { TokenRepository } from '@repository/token';

// Helpers
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { RolesRepository } from '@repository/roles';
import { CoursesRepository } from '@repository/courses';
import { ConceptsRepository } from '@repository/concepts';
import { NodesRepository } from '@repository/nodes';
import { QuizzesRepository } from '@repository/quizzes';
import { SemestersRepository } from '@repository/semesters';
import { AssessmentsRepository } from '@repository/assessments';
import { ContentsRepository } from '@repository/contents';
import { FilesRepository } from '@repository/files';
import { HttpClient } from '@infrastructure/http-client';
import { TeachersRepository } from './repository/teachers';
import { StudentsRepository } from './repository/students';
import { HabitsRepository } from './repository/habits';
import { TokenService } from './helpers/token.service';
import { PredictiveService } from './helpers/predictive.service';
import { LogsRepository } from './repository/logs';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    SweetAlert2Module.forRoot(),
    ChartsModule,
    StorageServiceModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
  ],
  providers: [{
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    UsersRepository,
    TokenRepository,
    RolesRepository,
    CoursesRepository,
    ConceptsRepository,
    NodesRepository,
    QuizzesRepository,
    SemestersRepository,
    AssessmentsRepository,
    TeachersRepository,
    StudentsRepository,
    HabitsRepository,
    ContentsRepository,
    FilesRepository,
    LocalStorageHelper,
    TokenService,
    LogsRepository,
    HttpClient,
    PredictiveService,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
