import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';

import { AssessmentsRepository } from '@app/repository/assessments';
import { Router } from '@angular/router';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { TokenRepository } from '@app/repository/token';
import { UsersRepository } from '@app/repository/users';
import Swal from 'sweetalert2';

@Component({
  templateUrl: 'take.component.html'
})
export class AssessmentsTakeComponent implements OnInit {
    public items;
    public token;
    public studentId;
    public course;
    constructor(
      private assessmentsRepository: AssessmentsRepository,
      private tokenRepository: TokenRepository,
      private userRepository: UsersRepository,
      private router: Router,
      private storage: LocalStorageHelper
    ) {}

    async ngOnInit() {
        const token = this.storage.retrieve('a-ext-token');
        const course = this.storage.retrieve('course');
        const { items } = await this.assessmentsRepository.getQuestionnaire(course);
        this.items = items;
        this.course = course;

        const { extra: { studentId } } = await this.tokenRepository.decode(token);
        this.studentId = studentId;
    }

    async onAssessmentsFormSubmit({ value: fields }: NgForm) {
        const { score, percentage } = await this.assessmentsRepository.evaluateAssessment(fields, this.studentId, this.course);
        Swal.fire('Action successful', `You got the score of ${score} with the percentage of ${percentage}%`, "success").then((result) => {
            this.router.navigate(['/interims']);
        });
    }
}
