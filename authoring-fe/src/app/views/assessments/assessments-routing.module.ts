import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssessmentsCreateComponent } from '@views/assessments/create/create.component';
import { AssessmentsTakeComponent } from './take/take.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Assessments'
  },
  children: [{
    path: 'c',
    component: AssessmentsCreateComponent,
    data: {
      title: 'Create Assessment'
    }
  },
  {
    path: 't',
    component: AssessmentsTakeComponent,
    data: {
      title: 'Take Assessment'
    }
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssessmentsRoutingModule {}
