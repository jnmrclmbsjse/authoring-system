// Angular
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Components Routing
import { AssessmentsRoutingModule } from '@views/assessments/assessments-routing.module';

// Components
import { AssessmentsCreateComponent } from './create/create.component';
import { AssessmentsTakeComponent } from './take/take.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AssessmentsRoutingModule,
  ],
  declarations: [
    AssessmentsCreateComponent,
    AssessmentsTakeComponent
  ]
})
export class AssessmentsModule {}
