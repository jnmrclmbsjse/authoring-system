import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, FormArray } from '@angular/forms';

import { AssessmentsRepository } from '@app/repository/assessments';
import { Router } from '@angular/router';
import Swal from "sweetalert2";
import { LocalStorageHelper } from '@app/helpers/localstorage.service';

@Component({
  templateUrl: 'create.component.html'
})
export class AssessmentsCreateComponent implements OnInit {
    constructor(
      private assessmentsRepository: AssessmentsRepository,
      private storage: LocalStorageHelper,
      private router: Router,
      private _formBuilder: FormBuilder
    ) {}

    public dynamicAssessmentsForm: FormGroup;

    async ngOnInit() {
        const course = this.storage.retrieve('course');

        this.dynamicAssessmentsForm = this._formBuilder.group({
            assessments: new FormArray([])
        });

        this.addAssessments();

        const result = await this.assessmentsRepository.getQuestionnaire(course);
        if (result) {
            Swal.fire("Sorry", "You already have created an assessmment for this course", "info").then(result => {
                this.router.navigate(['/concepts']);
            });
        }
    }

    // convenience getters for easy access to form fields
    get f() { return this.dynamicAssessmentsForm.controls; }
    get t() { return this.f.assessments as FormArray; }

    addAssessments() {
        this.t.push(this._formBuilder.group({
            topic: [''],
            answer: ['']
        }));
    }

    async onAssessmentsFormSubmit() {
        const { assessments } = this.dynamicAssessmentsForm.value;
        let items = [];
        assessments.forEach((assessment) => {
            const topic = assessment.topic;
            const answer = assessment.answer;
            const answers = answer.split('\n');
            const builder = {
                topic,
                answers
            };
            items.push(builder);
        });

        const payload = {
            items
        };
        const id = await this.assessmentsRepository.createAssessment(payload);
        Swal.fire('Action successful', "Pre-assessment for course has been created", "success");
        this.router.navigate(['/dashboard']);
    }

    async createAssessment({ value: fields }: NgForm) {
        const id = await this.assessmentsRepository.createAssessment(fields);

        if (id) {
            this.router.navigate(['/dashboard']);
        }
    }
}
