import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentsIndexComponent } from './index/index.component';

const routes: Routes = [{
  path: '',
  component: StudentsIndexComponent,
  data: {
    title: 'Students List'
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule {}
