import { Component, OnInit } from '@angular/core';

import { UsersRepository } from '@repository/users';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { CoursesRepository } from '@repository/courses';

@Component({
    templateUrl: 'show.component.html'
})
export class UsersShowComponent implements OnInit {
    private userId = null;
    public details = [];
    public courses = [];
    public info = null;
    constructor(
        private usersRepository: UsersRepository,
        private coursesRepository: CoursesRepository,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    async ngOnInit() {
        this.route.paramMap.pipe(
          switchMap((params: ParamMap) =>
            of (params.get('id'))
          )
        ).subscribe(async (id) => {
            this.userId = id;
            this.details = await this.usersRepository.getUserDetails(this.userId);
            this.info = this.details['info'];
        });

        this.courses = await this.coursesRepository.getAll();
    }

    async onChangeCourse(id) {
        const payload = {
            user: this.userId,
            course: id
        };

        await this.usersRepository.assignCourse(payload);
    }
}
