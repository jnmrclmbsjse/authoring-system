import { Component, OnInit } from '@angular/core';
import { UsersRepository } from '@app/repository/users';

@Component({
  templateUrl: 'index.component.html'
})
export class StudentsIndexComponent implements OnInit {
  public users = [];
  constructor(
      private userRepository: UsersRepository
  ) { }

  async ngOnInit() {
    const users = await this.userRepository.getAll();
    users.forEach(user => {
      if (user.role === 'Students') {
        this.users.push(user);
      }
    });
  }
}
