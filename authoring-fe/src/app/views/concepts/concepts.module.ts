// Angular
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Collapse Component
import { CollapseModule } from 'ngx-bootstrap/collapse';
// import { CollapsesComponent } from './collapses.component';

// Dropdowns Component
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Pagination Component
import { PaginationModule } from 'ngx-bootstrap/pagination';
// import { PopoversComponent } from './popovers.component';

// Popover Component
import { PopoverModule } from 'ngx-bootstrap/popover';
// import { PaginationsComponent } from './paginations.component';

// Tooltip Component
import { TooltipModule } from 'ngx-bootstrap/tooltip';
// import { TooltipsComponent } from './tooltips.component';

// Components Routing
import { ConceptsRoutingModule } from '@views/concepts/concepts-routing.module';

// Components
import { ConceptsCreateComponent } from '@app/views/concepts/create/create.component';
import { GraphsService } from '@app/helpers/graph.service';
import { ConceptsIndexComponent } from '@views/concepts/index/index.component';
import { ConceptsShowComponent } from '@views/concepts/show/show.component';
import { ConceptsBaseComponent } from '@views/concepts/base.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConceptsRoutingModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  providers: [
      GraphsService
  ],
  declarations: [
    ConceptsCreateComponent,
    ConceptsIndexComponent,
    ConceptsShowComponent,
    ConceptsBaseComponent,
  ]
})
export class ConceptsModule { }
