import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentsShowComponent } from './show/show.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Content editor'
  },
  children: [{
    path: ':contentId',
    component: ContentsShowComponent,
    data: {
      title: ''
    }
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentsRoutingModule {}
