import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { GraphsService } from "@helpers/graph.service";
import { MXUTILITIES, MxCodec } from '@helpers/mxgraph.class';
import { switchMap } from 'rxjs/operators';
import { ParamMap, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ModalDirective } from 'ngx-bootstrap';
import { FileUploadService } from '@app/helpers/fileupload.service';
import { NodesRepository } from '@app/repository/nodes';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { QuizzesRepository } from '@app/repository/quizzes';
import Swal from 'sweetalert2';

@Component({
  templateUrl: 'show.component.html'
})
export class ContentsShowComponent implements AfterViewInit {
    @ViewChild('diagram') diagram: ElementRef;
    @ViewChild('toolbar') toolbar: ElementRef;
    @ViewChild('divQuiz') divQuiz: ElementRef;
    @ViewChild('modalUpload') public modalUpload: ModalDirective;
    @ViewChild('modalQuiz') public modalQuiz: ModalDirective;
    @ViewChild('fileLabel') fileLabel: ElementRef;

    private fileToUpload: File = null;
    private graph = null;
    private activeCell = null;
    private nodeId = null;
    private node = null;
    private nodeTitle = null;

    public dynamicQuestionsForm: FormGroup;

    constructor(
      private graphService: GraphsService,
      private nodeRepository: NodesRepository,
      private quizRepository: QuizzesRepository,
      private route: ActivatedRoute,
      private fileUploadService: FileUploadService,
      private _formBuilder: FormBuilder
    ) {}

    ngOnInit() {
        this.dynamicQuestionsForm = this._formBuilder.group({
            questions: new FormArray([])
        });
        this.nodeTitle = this.route.snapshot.paramMap.get('title');
        this.addQuestions();
    }

    // convenience getters for easy access to form fields
    get f() { return this.dynamicQuestionsForm.controls; }
    get t() { return this.f.questions as FormArray; }

    async ngAfterViewInit() {
        this.route.paramMap.pipe(
          switchMap((params: ParamMap) =>
            of (params.get('contentId'))
          )
        ).subscribe(async (id) => {
            this.nodeId = id;

            this.graph = this.graphService.initGraph(this.diagram, this.toolbar, "content", true);
            this.graphService.addCustomVertex(this.graph, 'assets/img/icons/word.png', 200, 80, '', 'word');
            this.graphService.addCustomVertex(this.graph, 'assets/img/icons/presentation.png', 200, 80, '', 'presentation');
            this.graphService.addCustomVertex(this.graph, 'assets/img/icons/quiz.png', 200, 80, '', 'quiz');
            this.graphService.addCustomVertex(this.graph, 'assets/img/icons/video.png', 200, 80, '', 'video');
            this.graphService.addCustomVertex(this.graph, 'assets/img/icons/final.png', 200, 80, '', 'final');

            this.graph.popupMenuHandler.factoryMethod = (menu, cell, evt) => {
                return this.createPopupMenu(this.graph, menu, cell, evt);
            };

            try {
                this.node = await this.nodeRepository.getNodeDetails(this.nodeId);
                const { title, metadata } = this.node;
                var doc = MXUTILITIES.mxUtils.parseXml(metadata);
                var codec = new MxCodec(doc);
                codec.decode(doc.documentElement, this.graph.getModel());
            } finally {
                this.graph.getModel().endUpdate();
            };
        });
        
    }

    addQuestions() {
        this.t.push(this._formBuilder.group({
            topic: [''],
            answer: ['']
        }));
    }

    async onQuestionsFormSubmit() {
        const fileVertex = this.graph.insertVertex(this.activeCell, null, this.activeCell.getId(), 0.03, 0.6, 30, 30, 'quiz', true);
        fileVertex.setConnectable(false);
        this.modalQuiz.hide();
        const { questions } = this.dynamicQuestionsForm.value;
        let payload = [];
        questions.forEach((question) => {
            const topic = question.topic;
            const answer = question.answer;
            const answers = answer.split('\n');
            const builder = {
                topic,
                answers
            };
            payload.push(builder);
        });

        const contentId = this.activeCell.getId();
        const id = await this.quizRepository.createQuiz(payload, contentId);
        this.dynamicQuestionsForm = this._formBuilder.group({
            questions: new FormArray([])
        });
        this.addQuestions();
    }

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
        console.log(files.item(0));
        this.fileLabel.nativeElement.innerText = files.item(0).name;
    }

    async uploadFileToActivity() {
      const filename = await this.fileUploadService.postFile(this.fileToUpload);
      const childVertices = this.activeCell.children;
      let removeCells = [];
      childVertices.forEach(child => {
          if (child.style == 'image') {
              removeCells.push(child);
          }
      });
      this.graph.removeCells(removeCells);
      const fileVertex = this.graph.insertVertex(this.activeCell, null, filename, 0.9, 0.8, 30, 30, 'image', true);
      fileVertex.setConnectable(false);
      this.modalUpload.hide();
    }

    async onClickSaveConcept() {
        const encoder = new MxCodec(null);
        const node = encoder.encode(this.graph.getModel());

        let contents = [];
        Object.values(this.graph.getChildVertices(this.graph.getDefaultParent())).forEach((element: any) => {
            const id = element.getId();
            const title = element.getValue().localName ? element.getValue().localName : element.getValue();
            const identifier = element.getChildAt(0);
            const file = element.getChildAt(1);
            const filename = file.getValue();
            const type = identifier.getValue();
            const content = {
                id,
                title,
                type,
                filename
            };

            contents.push(content);

        })
        const xmlString = MXUTILITIES.mxUtils.getXml(node); // fetch xml (string or document/node)

        let metadata = xmlString.replace(/Mx/g, 'mx');
        const body = {
            id: this.nodeId,
            title: this.nodeTitle,
            metadata,
            contents
            // userId: this.userId
        };

        const conceptId = await this.nodeRepository.createNodes(body);

        Swal.fire("Action successful", "Content has been updated", "success");
    }

    private createPopupMenu(graph, menu, cell, evt) {
        if (cell != null) {
            if (['quiz', 'final'].indexOf(cell.getValue().getAttribute('type').toString()) !== -1) {
                menu.addItem('Create quiz', 'assets/img/icons/create.png', () => {
                  this.activeCell = cell;
                    this.modalQuiz.show();
                });
            }
            menu.addItem('Upload file', 'assets/img/icons/upload.png', () => {
              this.activeCell = cell;
              this.modalUpload.show();
            });
        }
    };
}
