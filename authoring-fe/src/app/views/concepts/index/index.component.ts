import { Component, OnInit } from '@angular/core';
import { TokenRepository } from '@app/repository/token';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { ConceptsRepository } from '@app/repository/concepts';

@Component({
  templateUrl: 'index.component.html'
})
export class ConceptsIndexComponent implements OnInit {
  public concepts = [];
  private token = null;

  constructor(
    private tokenRepository: TokenRepository,
    private conceptRepository: ConceptsRepository,
    private storage: LocalStorageHelper
  ) {
    this.token = this.storage.retrieve('a-ext-token');
  }

  async ngOnInit() {
    this.concepts = await this.conceptRepository.getAll();
  }
}
