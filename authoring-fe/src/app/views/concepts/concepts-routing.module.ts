import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConceptsCreateComponent } from '@views/concepts/create/create.component';
import { ConceptsIndexComponent } from '@views/concepts/index/index.component';
import { ConceptsShowComponent } from '@views/concepts/show/show.component';
import { ConceptsBaseComponent } from '@views/concepts/base.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Concept maps'
  },
  children: [{
      path: '',
      component: ConceptsIndexComponent,
      data: {
        title: 'Concept maps list'
      }
    },
    {
      path: 'c',
      component: ConceptsCreateComponent,
      data: {
        title: 'Create concept map'
      }
    },
    {
      path: ':id',
      component: ConceptsBaseComponent,
      data: {
        title: 'Concept map'
      },
      children: [{
          path: '',
          component: ConceptsShowComponent,
        },
        {
          path: 'contents',
          loadChildren: () => import('@views/concepts/contents/contents.module').then(m => m.ContentsModule)
        }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConceptsRoutingModule {}
