import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { GraphsService } from "@helpers/graph.service";
import { MXUTILITIES, MxCodec } from '@helpers/mxgraph.class';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { TokenRepository } from '@repository/token';
import { UsersRepository } from '@repository/users';
import { ConceptsRepository } from '@app/repository/concepts';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'create.component.html'
})
export class ConceptsCreateComponent implements AfterViewInit {
    @ViewChild('diagram') diagram: ElementRef;
    @ViewChild('toolbar') toolbar: ElementRef;
    private token = null;
    private graph = null;
    private userId;
    private courseTitle;

    constructor(
      private graphService: GraphsService,
      private storage: LocalStorageHelper,
      private userRepository: UsersRepository,
      private tokenRepository: TokenRepository,
      private conceptRepository: ConceptsRepository,
      private router: Router
    ) {
      this.token = this.storage.retrieve('a-ext-token');
    }

    async ngAfterViewInit() {
        this.graph = this.graphService.initGraph(this.diagram, this.toolbar);
        const { id } = await this.tokenRepository.decode(this.token);
        this.userId = id;
        const { info } = await this.userRepository.getUserDetails(this.userId);
        const { courseTitle } = info;
        this.courseTitle = courseTitle;

        try {
            const parent = this.graph.getDefaultParent();
            this.graph.getModel().beginUpdate();
            const scale = this.graph.view.scale;
            const container = this.graph.container;
            const width = 200;
            const height = 40;
            const x = (container.scrollLeft + container.clientWidth / 2) / scale - width / 2;
            const y = (container.scrollTop + container.clientHeight / 2) / scale - height / 2;

            var doc = MXUTILITIES.mxUtils.createXmlDocument();
            var node = doc.createElement(courseTitle.replace(' ', '-'))
            node.setAttribute('title', courseTitle);
            node.setAttribute('isCourse', true);
            const vertex1 = this.graph.insertVertex(parent, '1', node, x, 50, width, height);
            vertex1.setConnectable(true);
        } finally {
            this.graph.getModel().endUpdate();
        };
    }

    async onClickSaveConcept() {
        const encoder = new MxCodec(null);
        const node = encoder.encode(this.graph.getModel());
        const xmlString = MXUTILITIES.mxUtils.getXml(node); // fetch xml (string or document/node)

        let metadata = xmlString.replace(/Mx/g, 'mx');
        const body = {
            title: this.courseTitle,
            metadata,
            userId: this.userId
        };

        const conceptId = await this.conceptRepository.createConcept(body);
        this.router.navigate(['/concepts']);
    }
}
