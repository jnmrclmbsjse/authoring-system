import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { GraphsService } from "@helpers/graph.service";
import { MXUTILITIES, MxCodec } from '@helpers/mxgraph.class';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { ConceptsRepository } from '@app/repository/concepts';
import { switchMap } from 'rxjs/operators';
import { ParamMap, ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  templateUrl: 'show.component.html'
})
export class ConceptsShowComponent implements AfterViewInit {
    @ViewChild('diagram') diagram: ElementRef;
    @ViewChild('toolbar') toolbar: ElementRef;
    private token = null;
    private graph = null;
    private conceptId;
    private concept;

    constructor(
        private graphService: GraphsService,
        private storage: LocalStorageHelper,
        private conceptRepository: ConceptsRepository,
        private route: ActivatedRoute,
        private router: Router
    ) { 
        this.token = this.storage.retrieve('a-ext-token');
    }

    async ngAfterViewInit() {
        this.route.paramMap.pipe(
          switchMap((params: ParamMap) =>
            of (params.get('id'))
          )
        ).subscribe(async (id) => {
            this.conceptId = id;
            this.concept = await this.conceptRepository.getConceptDetails(this.conceptId);

            this.graph = this.graphService.initGraph(this.diagram, this.toolbar);
            this.graph.popupMenuHandler.factoryMethod = (menu, cell, evt) => {
                return this.createPopupMenu(this.graph, menu, cell, evt);
            };
            const { title, metadata } = this.concept;

            try {
                var doc = MXUTILITIES.mxUtils.parseXml(metadata);
                var codec = new MxCodec(doc);
                codec.decode(doc.documentElement, this.graph.getModel());
            } finally {
                this.graph.getModel().endUpdate();
            };
        });
        
    }

    async onClickSaveConcept() {
        const encoder = new MxCodec(null);
        const node = encoder.encode(this.graph.getModel());
        const xmlString = MXUTILITIES.mxUtils.getXml(node); // fetch xml (string or document/node)

        let metadata = xmlString.replace(/Mx/g, 'mx');
        const body = {
            title: 'Updated',
            metadata
        };

        const conceptId = await this.conceptRepository.updateConcept(body, this.conceptId);
        Swal.fire("Action successful", "Concept map has been updated", "success");
    }

    private createPopupMenu(graph, menu, cell, evt) {
        if (this.isInt(cell.id)) {
            return;
        }

        if (cell != null) {
          menu.addItem('Content editor', 'assets/img/icons/answer.png', () => {
              this.router.navigate(['contents', cell.id, { title: cell.value }],{relativeTo: this.route});
          });
        }
    };

    private isInt(value) {
        return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
    }
}
