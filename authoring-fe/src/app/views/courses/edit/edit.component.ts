import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CoursesRepository } from '@app/repository/courses';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  templateUrl: 'edit.component.html'
})
export class CoursesEditComponent implements OnInit {
    public roles:any;
    public courseId;
    public course:any;
    constructor(
      private coursesRepository: CoursesRepository,
      private router: Router,
      private route: ActivatedRoute
    ) {
      this.roles = [];
      this.course = [];
    }

    async ngOnInit() {
        this.roles = await this.coursesRepository.getAll();

        this.route.paramMap.pipe(
          switchMap((params: ParamMap) =>
            of (params.get('id'))
          )
        ).subscribe(async (id) => {
            this.courseId = id;
            const course = await this.coursesRepository.details(id);
            this.course = course;
        });
    }

    async updateCourse({ value: fields }: NgForm) {
        const id = await this.coursesRepository.updateCourse(fields, this.courseId);

        if (id) {
            this.router.navigate(['/courses']);
        }
    }
}
