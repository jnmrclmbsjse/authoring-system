import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoursesCreateComponent } from '@views/courses/create/create.component';
import { CoursesIndexComponent } from '@views/courses/index/index.component';
import { CoursesEditComponent } from '@views/courses/edit/edit.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Courses'
  },
  children: [{
      path: '',
      component: CoursesIndexComponent,
      data: {
        title: 'Manage Courses'
      }
    },
    {
      path: 'c',
      component: CoursesCreateComponent,
      data: {
        title: 'Create Course'
      }
    },
    {
      path: ':id',
      component: CoursesEditComponent,
      data: {
        title: 'Manage Courses'
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule {}
