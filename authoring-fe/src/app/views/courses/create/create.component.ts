import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CoursesRepository } from '@app/repository/courses';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'create.component.html'
})
export class CoursesCreateComponent implements OnInit {
    public roles = [];
    constructor(
      private coursesRepository: CoursesRepository,
      private router: Router
    ) {}

    async ngOnInit() {}

    async createCourse({ value: fields }: NgForm) {
        const id = await this.coursesRepository.createCourse(fields);

        if (id) {
            this.router.navigate(['/courses']);
        }
    }
}
