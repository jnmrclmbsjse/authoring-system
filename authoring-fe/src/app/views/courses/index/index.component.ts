import { Component, OnInit } from '@angular/core';
import { CoursesRepository } from '@app/repository/courses';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  templateUrl: 'index.component.html'
})
export class CoursesIndexComponent implements OnInit {
  public courses = [];
  constructor(
    private coursesRepository: CoursesRepository,
    private router: Router
  ) {}

  async ngOnInit() {
      this.courses = await this.coursesRepository.getAll();
  }

  async deleteCourse(courseId) {
    const success = this.coursesRepository.deleteCourse(courseId);

    if (success) {
      Swal.fire("Action successful", "Course has been deleted", "success");
      setTimeout(function () {
        location.reload();
      });
    }
  }

  editCourse(courseId) {
    this.router.navigate([`/courses/${courseId}`]);
  }
}
