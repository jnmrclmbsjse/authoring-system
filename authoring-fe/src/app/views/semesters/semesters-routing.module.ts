import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SemestersCreateComponent } from '@views/semesters/create/create.component';
import { SemestersIndexComponent } from '@views/semesters/index/index.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Semesters'
  },
  children: [{
      path: '',
      component: SemestersIndexComponent,
      data: {
        title: 'Manage Semesters'
      }
    },
    {
      path: 'c',
      component: SemestersCreateComponent,
      data: {
        title: 'Create Semester'
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SemestersRoutingModule {}
