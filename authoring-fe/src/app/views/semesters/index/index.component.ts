import { Component, OnInit } from '@angular/core';
import { SemestersRepository } from '@app/repository/semesters';
import Swal from "sweetalert2";

@Component({
  templateUrl: 'index.component.html'
})
export class SemestersIndexComponent implements OnInit {
  public semesters = [];
  constructor(
    private semestersRepository: SemestersRepository
  ) { }

  async ngOnInit() {
    this.semesters = await this.semestersRepository.getAll();
  }

  async deleteSemester(semesterId) {
    const success = await this.semestersRepository.delete(semesterId);
    if (success) {
      Swal.fire("Action successful", "Semester has been deleted", "success");
      setTimeout(function () {
        location.reload();
      });
    }
  }
}
