import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { SemestersRepository } from '@app/repository/semesters';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'create.component.html'
})
export class SemestersCreateComponent implements OnInit {
    constructor(
        private semestersRepository: SemestersRepository,
        private router: Router
    ) { }

    async ngOnInit() {}

    async createSemester({ value: fields }: NgForm) {
        const id = await this.semestersRepository.createSemester(fields);

        if (id) {
            this.router.navigate(['/semesters']);
        }
    }
}
