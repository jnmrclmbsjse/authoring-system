import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';

import { HabitsRepository } from '@app/repository/habits';
import { Router } from '@angular/router';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { TokenRepository } from '@app/repository/token';
import { UsersRepository } from '@app/repository/users';
import { TokenService } from '@app/helpers/token.service';
import * as data from "@app/data.json";
import Swal from "sweetalert2";

@Component({
  templateUrl: 'take.component.html'
})
export class HabitsTakeComponent implements OnInit {
    public objectKeys = Object.keys;

    public questions;
    public token;
    public studentId;
    public userId;

    public data: any = data;
    constructor(
      private habitsRepository: HabitsRepository,
      private tokenRepository: TokenRepository,
      private tokenService: TokenService,
      private router: Router,
      private storage: LocalStorageHelper
    ) {}

    async ngOnInit() {
        const token = this.storage.retrieve('a-ext-token');
        const { default: { questions } } = this.data;
        // let questionsByCategory = [];
        // questions.forEach(question => {
        //    const category = question.category.replace(/_/g, ' ');
        //    if (questionsByCategory[category] === undefined) {
        //     questionsByCategory[category] = [];
        //     questionsByCategory[category].push({
        //         id: question.id,
        //         content: question.content
        //     });
        //    } else {
        //        questionsByCategory[category].push({
        //         id: question.id,
        //         content: question.content
        //     });
        //    }
        // });
        this.questions = questions;

        const { extra: { studentId, id: userId } } = await this.tokenRepository.decode(token);
        this.studentId = studentId;
        this.userId = userId;
    }

    async onHabitsFormSubmit({ value: fields }: NgForm) {
        await this.habitsRepository.submitHabit(fields, this.studentId);
        await this.tokenService.refresh();
        Swal.fire('Action successful', "Habits assessment for semester has been taken", "success");
        window.location.href = "/dashboard";
    }
}
