// Angular
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Components Routing
import { HabitsRoutingModule } from '@views/habits/habits-routing.module';

// Components
import { HabitsCreateComponent } from './create/create.component';
import { HabitsTakeComponent } from './take/take.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HabitsRoutingModule,
  ],
  declarations: [
    HabitsCreateComponent,
    HabitsTakeComponent
  ]
})
export class HabitsModule {}
