import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, FormArray } from '@angular/forms';

import { HabitsRepository } from '@app/repository/habits';
import { Router } from '@angular/router';

import Swal from "sweetalert2";

@Component({
  templateUrl: 'create.component.html'
})
export class HabitsCreateComponent implements OnInit {
    constructor(
      private habitsRepository: HabitsRepository,
      private router: Router,
      private _formBuilder: FormBuilder
    ) {}

    public dynamicHabitsForm: FormGroup;

    ngOnInit() {
        this.dynamicHabitsForm = this._formBuilder.group({
            habits: new FormArray([])
        });

        this.addHabits();
    }

    // convenience getters for easy access to form fields
    get f() { return this.dynamicHabitsForm.controls; }
    get t() { return this.f.habits as FormArray; }

    addHabits() {
        this.t.push(this._formBuilder.group({
            category: [''],
            question: ['']
        }));
    }

    async onHabitsFormSubmit() {
        const { habits } = this.dynamicHabitsForm.value;
        const payload = {
            items: habits
        };
        const id = await this.habitsRepository.createHabits(payload);
        if (!id) {
            Swal.fire('Action failed', "There is no active semester", "error");
        }

        Swal.fire('Action successful', "Habits assessment for semester has been created", "success");
        this.router.navigate(['/dashboard']);
    }

    async createHabits({ value: fields }: NgForm) {
        const id = await this.habitsRepository.createHabits(fields);

        if (id) {
            this.router.navigate(['/dashboard']);
        }
    }
}
