import { Component, OnInit } from '@angular/core';
import { SemestersRepository } from '@app/repository/semesters';

@Component({
  templateUrl: 'index.component.html'
})
export class SemestersIndexComponent implements OnInit {
  public semesters = [];
  constructor(
    private semestersRepository: SemestersRepository
  ) {}

  async ngOnInit() {
    this.semesters = await this.semestersRepository.getAll();
  }
}
