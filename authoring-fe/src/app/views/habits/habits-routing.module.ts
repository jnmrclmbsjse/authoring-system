import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HabitsCreateComponent } from '@views/habits/create/create.component';
import { HabitsTakeComponent } from './take/take.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Habits'
  },
  children: [{
    path: 'c',
    component: HabitsCreateComponent,
    data: {
      title: 'Create Habits'
    }
  },
  {
    path: 't',
    component: HabitsTakeComponent,
    data: {
      title: 'Take Habits Assessment'
    }
  }
]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HabitsRoutingModule {}
