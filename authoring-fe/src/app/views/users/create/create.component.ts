import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { RolesRepository } from '@repository/roles';
import { UsersRepository } from '@app/repository/users';
import { Router } from '@angular/router';

@Component({
  templateUrl: './create.component.html'
})
export class UsersCreateComponent implements OnInit {
    public roles = [];
    constructor(
      private rolesRepository: RolesRepository,
      private usersRepository: UsersRepository,
      private router: Router
    ) {}

    async ngOnInit() {
        this.roles = await this.rolesRepository.getAll();
    }

    async createUser({ value: fields }: NgForm) {
        const {
            username,
            password,
            role
        } = fields;

        let payload;

        payload = {
            username,
            password,
            role,
        };

        if (role === '2') {
            payload = {
                username,
                password,
                role,
                firstname: fields.firstname,
                lastname: fields.lastname,
                mobile: fields.mobile
            };
        } 

        if (role === '3') {
            payload = {
                username,
                password,
                role,
                firstname: fields.firstname,
                middlename: fields.middlename,
                lastname: fields.lastname,
                mobile: fields.mobile
            };
        }

        const id = await this.usersRepository.createUser(payload);

        if (id) {
            this.router.navigate(['/users']);
        }
    }
}
