// Angular
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Collapse Component
import { CollapseModule } from 'ngx-bootstrap/collapse';
// import { CollapsesComponent } from './collapses.component';

// Dropdowns Component
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Pagination Component
import { PaginationModule } from 'ngx-bootstrap/pagination';
// import { PopoversComponent } from './popovers.component';

// Popover Component
import { PopoverModule } from 'ngx-bootstrap/popover';
// import { PaginationsComponent } from './paginations.component';

// Tooltip Component
import { TooltipModule } from 'ngx-bootstrap/tooltip';
// import { TooltipsComponent } from './tooltips.component';

// Components Routing
import { UsersRoutingModule } from '@views/users/users-routing.module';

// Components
import { UsersCreateComponent } from '@views/users/create/create.component';
import { UsersIndexComponent } from '@views/users/index/index.component';
import { UsersShowComponent } from '@views/users/show/show.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UsersRoutingModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    UsersCreateComponent,
    UsersIndexComponent,
    UsersShowComponent,
  ]
})
export class UsersModule { }
