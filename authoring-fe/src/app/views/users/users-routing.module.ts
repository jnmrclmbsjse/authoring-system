import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersCreateComponent } from '@views/users/create/create.component';
import { UsersIndexComponent } from '@views/users/index/index.component';
import { UsersShowComponent } from './show/show.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Users'
  },
  children: [{
      path: '',
      component: UsersIndexComponent,
      data: {
        title: 'Manage Users'
      }
    },
    {
      path: 'c',
      component: UsersCreateComponent,
      data: {
        title: 'Create User'
      }
    },
    {
      path: ':id',
      component: UsersShowComponent,
      data: {
        title: 'User Details'
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
