import { Component, OnInit } from '@angular/core';

import { UsersRepository } from '@repository/users';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { CoursesRepository } from '@repository/courses';
import { TeachersRepository } from '@app/repository/teachers';
import { StudentsRepository } from '@app/repository/students';
import Swal from "sweetalert2";

@Component({
    templateUrl: 'show.component.html'
})
export class UsersShowComponent implements OnInit {
    public details:any;
    public courses:any;
    public info = null;
    constructor(
      private usersRepository: UsersRepository,
      private coursesRepository: CoursesRepository,
      private teachersRepository: TeachersRepository,
      private studentsRepository: StudentsRepository,
      private route: ActivatedRoute
    ) {
      this.details = [];
      this.courses = [];
    }

    async ngOnInit() {
        this.route.paramMap.pipe(
          switchMap((params: ParamMap) =>
            of (params.get('id'))
          )
        ).subscribe(async (id) => {
            this.details = await this.usersRepository.getUserDetails(id);
            this.info = this.details['info'];

            if (this.details['role'].toLowerCase() === 'teachers') {
              this.courses = await this.coursesRepository.getAll();
              this.courses.unshift({
                id: null,
                title: 'Select a course'
              });
            }

            if (this.details['role'].toLowerCase() === 'students') {
              this.courses = await this.coursesRepository.getTeachersCourses();
            }
        });

        
    }

    async onChangeCourse(id) {
        if (this.details['role'].toLowerCase() === 'teachers') {
          const payload = {
              teacherId: this.info.teacherId,
              courseId: id
          };

          const teacherCourseId = await this.teachersRepository.assignCourse(payload);
          if (teacherCourseId == 0) {
            Swal.fire("Action failed", "This course is already taken.", 'info').then(result => {
              location.reload();
            })
          }
        }

        if (this.details['role'].toLowerCase() === 'students') {
          const payload = {
            studentId: this.info.studentId,
            teacherCourseId: id
          };

          await this.studentsRepository.assignCourse(payload);
        }
       
    }
}
