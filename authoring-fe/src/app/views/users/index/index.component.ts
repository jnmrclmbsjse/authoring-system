import { Component, OnInit } from '@angular/core';
import { UsersRepository } from '@app/repository/users';
import Swal from "sweetalert2";

@Component({
  templateUrl: 'index.component.html'
})
export class UsersIndexComponent implements OnInit {
  public users = [];
  constructor(
    private userRepository: UsersRepository
  ) {}

  async ngOnInit() {
    this.users = await this.userRepository.getAll();
  }

  async deleteUser(userId) {
    const success = this.userRepository.delete(userId);

    if (success) {
      Swal.fire("Action successful", "User has been deleted", "success");
      setTimeout(function () {
        location.reload();
      });
    }
  }
}
