import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';

import { InterimsComponent } from './interims.component';
import { InterimsRoutingModule } from './interims-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InterimsRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot()
  ],
  declarations: [ InterimsComponent ]
})
export class InterimsModule { }
