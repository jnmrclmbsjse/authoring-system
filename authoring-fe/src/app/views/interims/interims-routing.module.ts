import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InterimsComponent } from './interims.component';

const routes: Routes = [
  {
    path: '',
    component: InterimsComponent,
    data: {
      title: 'Interims'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterimsRoutingModule {}
