import { Component, OnInit } from '@angular/core';
import { UsersRepository } from '@app/repository/users';
import { CoursesRepository } from '@app/repository/courses';
import { LogsRepository } from '@app/repository/logs';
import { NodesRepository } from '@app/repository/nodes';
import { TokenRepository } from '@app/repository/token';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { AssessmentsRepository } from '@app/repository/assessments';
import { Router } from '@angular/router';
import { QuizzesRepository } from '@app/repository/quizzes';
import { environment } from '../../../environments/environment';
import * as data from "@app/data.json";

@Component({
  templateUrl: 'interims.component.html'
})
export class InterimsComponent implements OnInit {
  private token = null;
  public results = [];
  private nodes = [];
  private quizzes = [];
  private studentName = "";
  private studentId = "";
  private courseDetails = [];
  private usage = 0;
  private logs = [];
  env = environment;
  public data: any = data;

  constructor (
    private nodesRepository: NodesRepository,
    private tokenRepository: TokenRepository,
    private storage: LocalStorageHelper,
    private assessmentsRepository: AssessmentsRepository,
    private quizRepository: QuizzesRepository,
    private coursesRepository: CoursesRepository,
    private logsRepository: LogsRepository,
    private router: Router,
  ) {
    this.token = this.storage.retrieve('a-ext-token');
  }

  async ngOnInit() {
    const courseId = this.storage.retrieve('course');
    const { extra: { studentId, firstname, lastname } } = await this.tokenRepository.decode(this.token);
    const courseDetails = await this.coursesRepository.details(courseId);
    this.courseDetails = courseDetails;
    this.studentName = `${firstname} ${lastname}`;
    this.studentId = studentId;
    const results = await this.assessmentsRepository.getStudentAssessment(studentId, courseId);
    if (!results) {
        this.router.navigate(['/assessments/t']);
    }
    this.results = results;
    const nodes = await this.nodesRepository.getDoneNodes(studentId);
    const logs = await this.logsRepository.getLogsByType(studentId, "authenticate");
    this.logs = logs;
    const usage = logs.splice(0, 5);
    this.usage = usage;
    this.nodes = nodes;

    const quizzes = await this.quizRepository.getAllForStudent(studentId);

    let quizBuilder = [];
    const { efforts } = data;
    Object.values(quizzes).forEach(quiz => {
      let item: any = quiz;
      item.image = `${this.env.photosDirectory}/${item.reference}`;

      Object.keys(efforts).forEach((key: any) => {
        const max: number = key.split('-')[0];
        const min: number = key.split('-')[1];
        if (parseInt(item.percentage) <= max && parseInt(item.percentage) >= min) {
          item = {
            ...item,
            ...efforts[key]
          };
        }
      });
      console.log(item);
      quizBuilder.push(item);
    });
    this.quizzes = quizBuilder;
    // generate random values for mainChart
  }
}
