import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';

import { AchievementsComponent } from './achievements.component';
import { AchievementsRoutingModule } from './achievements-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AchievementsRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot()
  ],
  declarations: [ AchievementsComponent ]
})
export class AchievementsModule { }
