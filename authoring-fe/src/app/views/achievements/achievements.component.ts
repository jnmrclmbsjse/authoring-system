import { Component, OnInit } from '@angular/core';
import { UsersRepository } from '@app/repository/users';
import { CoursesRepository } from '@app/repository/courses';
import { LogsRepository } from '@app/repository/logs';
import { NodesRepository } from '@app/repository/nodes';
import { TokenRepository } from '@app/repository/token';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';

@Component({
  templateUrl: 'achievements.component.html'
})
export class AchievementsComponent implements OnInit {
  private token = null;
  public nodes;

  constructor (
    private nodesRepository: NodesRepository,
    private tokenRepository: TokenRepository,
    private storage: LocalStorageHelper,
  ) {
    this.token = this.storage.retrieve('a-ext-token');
  }

  async ngOnInit() {
    const { extra: { studentId } } = await this.tokenRepository.decode(this.token);
    const nodes = await this.nodesRepository.getDoneNodes(studentId);
    this.nodes = nodes;
    // generate random values for mainChart
  }
}
