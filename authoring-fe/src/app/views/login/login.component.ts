import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UsersRepository } from '@repository/users';
import { TokenRepository } from '@repository/token';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { TokenService } from '@app/helpers/token.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit { 
  public isLoggingIn = false;
  constructor(
    private userRepository: UsersRepository,
    // private tokenRepository: TokenRepository,
    private tokenService: TokenService,
    private storage: LocalStorageHelper,
    public router: Router
  ) {}

  ngOnInit() {
    this.storage.remove("a-ext-token");
    this.storage.remove("course");
  }

  async onClickLogin(username: string, password: string) {
    this.isLoggingIn = true;
    const data = {
      username,
      password
    };

    try {
      const id = await this.userRepository.authenticate(data);

      await this.tokenService.create(id);
      
      window.location.href = '/dashboard';
    } catch (e) {
      this.isLoggingIn = false;
    }

  }
}
