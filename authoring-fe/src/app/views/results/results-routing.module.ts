import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResultsIndexComponent } from '@views/results/index/index.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Results'
  },
  children: [{
      path: '',
      component: ResultsIndexComponent,
      data: {
        title: 'Grade book'
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultsRoutingModule {}
