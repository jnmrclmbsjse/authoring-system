import { Component, OnInit } from '@angular/core';
import { CoursesRepository } from '@app/repository/courses';
import { QuizzesRepository } from '@app/repository/quizzes';
import { AssessmentsRepository } from '@app/repository/assessments';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { TokenRepository } from '@app/repository/token';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  templateUrl: 'index.component.html'
})
export class ResultsIndexComponent implements OnInit {
  public quizzes = [];
  public assessment = [];
  env = environment;
  constructor(
    private quizRepository: QuizzesRepository,
    private assessmentsRepository: AssessmentsRepository,
    private tokenRepository: TokenRepository,
    private storage: LocalStorageHelper,
    private router: Router
  ) {}

  async ngOnInit() {
      const course = this.storage.retrieve('course');
      const token = this.storage.retrieve('a-ext-token');
      const { extra: { studentId } } = await this.tokenRepository.decode(token);
      const assessment = await this.assessmentsRepository.getStudentAssessment(studentId, course);
      if (!assessment) {
          this.router.navigate(['/assessments/t']);
      }
      this.assessment = assessment;
      const quizzes = await this.quizRepository.getAllForStudent(studentId);

      let quizBuilder = [];

      Object.values(quizzes).forEach(quiz => {
        let item: any = quiz;
        item.image = `${this.env.photosDirectory}/${item.reference}`;
        quizBuilder.push(item);
      });
      this.quizzes = quizBuilder;
  }
}
