// Angular
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Collapse Component
import { CollapseModule } from 'ngx-bootstrap/collapse';
// import { CollapsesComponent } from './collapses.component';

// Dropdowns Component
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Pagination Component
import { PaginationModule } from 'ngx-bootstrap/pagination';
// import { PopoversComponent } from './popovers.component';

// Popover Component
import { PopoverModule } from 'ngx-bootstrap/popover';
// import { PaginationsComponent } from './paginations.component';

// Tooltip Component
import { TooltipModule } from 'ngx-bootstrap/tooltip';
// import { TooltipsComponent } from './tooltips.component';

// Components Routing
import { ResultsRoutingModule } from '@views/results/results-routing.module';

// Components
import { ResultsIndexComponent } from '@views/results/index/index.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ResultsRoutingModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    ResultsIndexComponent
  ]
})
export class ResultsModule { }
