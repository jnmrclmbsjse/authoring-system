import { Component, OnInit } from '@angular/core';
import { UsersRepository } from '@app/repository/users';
import { CoursesRepository } from '@app/repository/courses';
import { LogsRepository } from '@app/repository/logs';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  public users = [];
  public courses = [];
  public logs = [];

  constructor (
    private usersRepository: UsersRepository,
    private coursesRepository: CoursesRepository,
    private logsRepository: LogsRepository
  ) {

  }

  async ngOnInit() {
    const users = await this.usersRepository.getAll();
    this.users = users;

    const courses = await this.coursesRepository.getAll();
    this.courses = courses;

    let logs = await this.logsRepository.getAll();
    if (logs.length > 10) {
      logs = logs.slice(0, 9);
    }

    this.logs = logs;
    // generate random values for mainChart
  }
}
