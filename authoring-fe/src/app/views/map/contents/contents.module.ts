// Angular
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Tooltip Component
import { TooltipModule } from 'ngx-bootstrap/tooltip';
// import { TooltipsComponent } from './tooltips.component';

import { ModalModule } from 'ngx-bootstrap/modal';

// Components Routing
import { ContentsRoutingModule } from '@app/views/map/contents/contents-routing.module';

// Servives
import { GraphsService } from '@app/helpers/graph.service';
import { FileUploadService } from '@app/helpers/fileupload.service';

// Components
import { ContentsShowComponent } from './show/show.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ContentsRoutingModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  providers: [
      GraphsService,
      FileUploadService
  ],
  declarations: [
    ContentsShowComponent
  ]
})
export class ContentsModule { }
