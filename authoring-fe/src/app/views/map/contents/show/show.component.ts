import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { GraphsService } from "@helpers/graph.service";
import { MXUTILITIES, MxCodec } from '@helpers/mxgraph.class';
import { switchMap } from 'rxjs/operators';
import { ParamMap, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ModalDirective } from 'ngx-bootstrap';
import { FileUploadService } from '@app/helpers/fileupload.service';
import { NodesRepository } from '@app/repository/nodes';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { QuizzesRepository } from '@app/repository/quizzes';
import { ContentsRepository } from '@app/repository/contents';
import { FilesRepository } from '@app/repository/files';
import { environment } from '../../../../../environments/environment';
import { TokenRepository } from '@app/repository/token';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import html2canvas from "html2canvas";
import { PredictiveService } from '@app/helpers/predictive.service';
import Swal from 'sweetalert2';

@Component({
  templateUrl: 'show.component.html'
})
export class ContentsShowComponent implements AfterViewInit, OnInit {
    @ViewChild('diagram') diagram: ElementRef;
    @ViewChild('toolbar') toolbar: ElementRef;
    @ViewChild('divQuiz') divQuiz: ElementRef;
    @ViewChild('modalUpload') public modalUpload: ModalDirective;
    @ViewChild('modalQuiz') public modalQuiz: ModalDirective;
    @ViewChild('answerSheet') answerSheet: ElementRef;
    env = environment;
    private fileToUpload: File = null;
    private graph = null;
    private activeCell = null;
    private nodeId = null;
    private node = null;
    private questionnaire = null;
    private studentId;

    public dynamicQuestionsForm: FormGroup;

    constructor(
      private graphService: GraphsService,
      private nodeRepository: NodesRepository,
      private quizRepository: QuizzesRepository,
      private contentsRepository: ContentsRepository,
      private tokenRepository: TokenRepository,
      private predictive: PredictiveService,
      private route: ActivatedRoute,
      private storage: LocalStorageHelper,
      private _formBuilder: FormBuilder
    ) {}

    ngOnInit() {
        this.dynamicQuestionsForm = this._formBuilder.group({
            questions: new FormArray([])
        });
    }

    // convenience getters for easy access to form fields
    get f() { return this.dynamicQuestionsForm.controls; }
    get t() { return this.f.questions as FormArray; }

    async ngAfterViewInit() {
        const token = this.storage.retrieve('a-ext-token');

        const { extra: { studentId } } = await this.tokenRepository.decode(token);
        this.studentId = studentId;
        
        this.route.paramMap.pipe(
          switchMap((params: ParamMap) =>
            of (params.get('contentId'))
          )
        ).subscribe(async (id) => {
            console.log(id);
            this.nodeId = id;

            this.graph = this.graphService.initGraph(this.diagram, null);

            this.graph.popupMenuHandler.factoryMethod = (menu, cell, evt) => {
                return this.createPopupMenu(this.graph, menu, cell, evt);
            };

            this.graph.setEnabled(false);

            try {
                this.node = await this.nodeRepository.getNodeDetails(this.nodeId);
                const { title, metadata } = this.node;
                var doc = MXUTILITIES.mxUtils.parseXml(metadata);
                var codec = new MxCodec(doc);
                codec.decode(doc.documentElement, this.graph.getModel());
            } finally {
                this.graph.getModel().endUpdate();
            };
        });
        
    }

    dataURItoBlob(dataURI) {
      var binary = atob(dataURI.split(',')[1]);
      var array = [];
      for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }
      return new Blob([new Uint8Array(array)], {
        type: 'image/png'
      });
    }

    async onQuestionsFormSubmit(form) {
        const fileVertex = this.graph.insertVertex(this.activeCell, null, this.activeCell.getId(), 0.03, 0.6, 30, 30, 'quiz', true);
        fileVertex.setConnectable(false);
        this.modalQuiz.hide();
        const { questions } = form.value;
        let payload = [];
        questions.forEach((question) => {
            const topic = question.topic;
            const answer = question.answer;
            const builder = {
                topic,
                answer
            };
            payload.push(builder);
        });
        // console.log(payload);
        const formData: FormData = new FormData();

        await html2canvas(this.answerSheet.nativeElement).then(canvas => {
            var file:Blob = this.dataURItoBlob(canvas.toDataURL('image/png'));
            formData.append('file', file);
        });

        const contentId = this.activeCell.getId();
        const { score, percentage } = await this.quizRepository.evaluateQuiz(payload, this.studentId, contentId, formData);
        if (parseInt(percentage) <= 65) {
            Swal.fire("Quiz has been submitted", `You got the score of ${score} with the percentage of ${percentage}%. Unfortunately you failed the exam. Please read your lessons again.`, "info");
        } else {
            const details = await this.contentsRepository.getContentDetails(contentId);
            this.checkLastContent(this.activeCell);
            Swal.fire("Quiz has been submitted", `You got the score of ${score} with the percentage of ${percentage}%. You can now move on to the next content/topic`, "success");
        }

        // const id = await this.quizRepository.createQuiz(payload, contentId);
    }

    public async buildQuestionnaire(items: any) {
        for (let [key, value] of Object.entries(items)) {
            const item:any = value;
            this.t.push(this._formBuilder.group({
                topic: [item.topic.id, Validators.required],
                answer: ['']
            }));
        }
    }

    private async createPopupMenu(graph, menu, cell, evt) {
        if (cell != null) {
            try {
                if (['quiz', 'final'].indexOf(cell.getValue().getAttribute('type').toString()) !== -1 ) {
                    menu.addItem('Take quiz', 'assets/img/icons/answer.png', async () => {
                        const source = this.predictive.getPreRequisite(cell);
                        if (typeof source !== 'undefined') {
                            const viewed = await this.contentsRepository.getViewedContent(source.id, this.studentId);
                            if (!viewed) {
                                Swal.fire("Access forbidden", "Sorry you haven\'t viewed/taken prerequisite contents", "info");
                                return;
                            }
                        }
                        const contentId = cell.id;
                        const quiz = await this.quizRepository.getQuiz(contentId);
                        this.questionnaire = quiz.items;
                        this.dynamicQuestionsForm = this._formBuilder.group({
                            questions: new FormArray([])
                        });
                        await this.buildQuestionnaire(quiz.items);
                        this.activeCell = cell;
                        this.modalQuiz.show();
                    });
                } else {
                    menu.addItem('Download file', 'assets/img/icons/download.png', async () => {
                        const source = this.predictive.getPreRequisite(cell);

                        if (typeof source !== 'undefined') {
                            const viewed = await this.contentsRepository.getViewedContent(source.id, this.studentId);
                            if (!viewed) {
                                Swal.fire("Access forbidden", "Sorry you haven\'t viewed/taken prerequisite contents", "info");
                                return;
                            }
                        }

                        this.activeCell = cell;
                        this.checkLastContent(cell);

                        const contentId = cell.id;
                        const details = await this.contentsRepository.getContentDetails(contentId);
                        window.location.href = this.env.backend + '/files?file=' + details.file;
                    });
                }
            } catch (e) {

            }
            
        }
    };

    private checkLastContent(cell) {
        if (cell.getEdgeCount() > 1) {
            return false;
        }

        const target = this.predictive.getPostRequisite(cell);
        if (typeof target === 'undefined') {
            this.nodeRepository.updateNodeDone(this.studentId, this.nodeId);
        }

        return true;
    }
}
