import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { GraphsService } from "@helpers/graph.service";
import { MxHierarchicalLayout, MXUTILITIES, MxCodec } from '@helpers/mxgraph.class';
import { LocalStorageHelper } from '@app/helpers/localstorage.service';
import { TokenRepository } from '@repository/token';
import { UsersRepository } from '@repository/users';
import { parseString } from 'xml2js';
import { ConceptsRepository } from '@app/repository/concepts';
import { switchMap } from 'rxjs/operators';
import { ParamMap, ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { AssessmentsRepository } from '@app/repository/assessments';
import { CoursesRepository } from '@app/repository/courses';
import { PredictiveService } from '@app/helpers/predictive.service';
import { NodesRepository } from '@app/repository/nodes';
import Swal from "sweetalert2";

@Component({
  templateUrl: 'show.component.html'
})
export class MapShowComponent implements AfterViewInit, OnInit {
    @ViewChild('diagram') diagram: ElementRef;
    @ViewChild('toolbar') toolbar: ElementRef;
    private token = null;
    private graph = null;
    private conceptId;
    public results = null;
    private concept;

    private recommendations;
    private nodes = [];
    private recommendedLevel;

    constructor(
        private graphService: GraphsService,
        private storage: LocalStorageHelper,
        private conceptRepository: ConceptsRepository,
        private tokenRepository: TokenRepository,
        private assessmentsRepository: AssessmentsRepository,
        private nodesRepository: NodesRepository,
        private route: ActivatedRoute,
        private predictive: PredictiveService,
        private router: Router
    ) { 
        this.token = this.storage.retrieve('a-ext-token');
    }

    async ngOnInit() {
        const course = this.storage.retrieve('course');
        const { extra: { studentId } } = await this.tokenRepository.decode(this.token);
        const results = await this.assessmentsRepository.getStudentAssessment(studentId, course);
        if (!results) {
            this.router.navigate(['/assessments/t']);
        }
        this.results = results;
        const doneNodes = await this.nodesRepository.getDoneNodes(studentId);
        // this.doneNodes = doneNodes;
        const recommendedLevel = this.predictive.analyzeLevel(this.results.percentage);
        const recommendations = this.predictive.getRecommendations(this.graph, recommendedLevel);
        this.recommendations = recommendations;
        this.recommendedLevel = recommendedLevel;
        this.createDoneNodesList(doneNodes);

    }

    async ngAfterViewInit() {
        const course = this.storage.retrieve('course');

        this.concept = await this.conceptRepository.getConceptDetailsByCourse(course);
        // this.concept = await this.conceptRepository.getConceptDetails(this.conceptId);

        this.graph = this.graphService.initGraph(this.diagram, null);
        this.graph.popupMenuHandler.factoryMethod = (menu, cell, evt) => {
          return this.createPopupMenu(this.graph, menu, cell, evt);
        };
        this.graph.setEnabled(false);
        const { title, metadata } = this.concept;
        try {
          var doc = MXUTILITIES.mxUtils.parseXml(metadata);
          var codec = new MxCodec(doc);
          codec.decode(doc.documentElement, this.graph.getModel());
        } finally {
          this.graph.getModel().endUpdate();
        };

    }

    private createDoneNodesList (doneNodes) {
        Object.values(doneNodes).forEach(item => {
            const node: any = item;
            const vertex = this.graph.getModel().getCell(node.nodeId);
            const name = vertex.getValue();
            const builder = {
                id: node.nodeId,
                name: name
            };
            this.nodes.push(builder);

            const index = this.recommendations.indexOf(name);

            if (index !== -1) {
                this.recommendations.splice(index, 1);
            }
        });

        console.log(this.nodes);
    }

    async onClickSaveConcept() {
        const encoder = new MxCodec(null);
        const node = encoder.encode(this.graph.getModel());
        const testString = MXUTILITIES.mxUtils.getXml(node); // fetch xml (string or document/node)
        let result;

        parseString(testString, (err, res) => {
            result = JSON.stringify(res);
        }); // parses to JSON object

        const body = {
            title: "My Concept",
            metadata: result,
        };

        const conceptId = await this.conceptRepository.createConcept(body);
    }

    private createPopupMenu(graph, menu, cell, evt) {
        const level = this.predictive.getCellLevel(cell);
        if (level > this.recommendedLevel) {
            const source = this.predictive.getPreRequisite(cell);
            let allowed = false;
            Object.values(this.nodes).forEach(node => {
                const nodeId = node.id;
                if (nodeId === source.id) {
                    allowed = true;
                }
            });
            if (!allowed) {
                Swal.fire("Access forbidden", "Sorry you are not yet allowed to access this topic", "info");
                return;
            }
        }

        if (this.isInt(cell.id)) {
            return;
        }
        if (cell != null) {
          menu.addItem('View content', 'assets/img/icons/eye.png', () => {
              this.router.navigate(['contents', cell.id],{relativeTo: this.route});
          });
        }
    };

    private isInt(value) {
        return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
    }
}
