import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapShowComponent } from '@app/views/map/show/show.component';
// import { InterimBaseComponent } from '@views/concepts/base.component';

const routes: Routes = [{
  path: '',
  data: {
    title: 'Concept map'
  },
  children: [{
      path: '',
      component: MapShowComponent,
      data: {
        title: 'Student concept map'
      },
    },
    {
      path: 'contents',
      loadChildren: () => import('@app/views/map/contents/contents.module').then(m => m.ContentsModule)
    }
    
    // {
    //   path: ':id',
    //   component: InterimBaseComponent,
    //   data: {
    //     title: 'Concept map'
    //   },
    //   children: [{
    //       path: '',
    //       component: InterimShowComponent,
    //     },
    //     {
    //       path: 'contents',
    //       loadChildren: () => import('@views/concepts/contents/contents.module').then(m => m.ContentsModule)
    //     }
    //   ]
    // }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapRoutingModule {}
