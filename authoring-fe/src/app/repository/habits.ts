import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class HabitsRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async createHabits(body: object) {
    const { data } = await this.client.post('/habits', body);
    const { data: { id } } = data;
    return id;
  }

  async getQuestionnaire() {
    const { data } = await this.client.get(`/habits`);
    const { data: { habit } } = data;
    return habit;
  }

  async submitHabit(answers: object, studentId: string) {
    const { data } = await this.client.post(`/students/${studentId}/habits`, {
      answers
    });
    const { data: { habit } } = data;
    return habit;
  }
}