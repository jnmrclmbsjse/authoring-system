import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class UsersRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }
  
  async createUser(body: object) {
    const { data } = await this.client.post('/users', body);
    const { data: { id } } = data;
    return id;
  }

  async authenticate(body: object) {
    const { data } = await this.client.post('/auth', body);
    const { data: { id } } = data;
    return id;
  }

  async getUserDetails(id: string) {
    const { data } = await this.client.get(`/users/${id}`);
    const { data: { details } } = data;
    return details;
  }

  async getAll() {
    const { data } = await this.client.get('/users');
    const { data: { users } } = data;
    return users;
  }

  async delete(userId: number) {
    const { data } = await this.client.delete(`/users/${userId}`);
    const { data: { success } } = data;
    return success;
  }
}