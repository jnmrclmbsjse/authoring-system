import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class AssessmentsRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async createAssessment(body: object) {
    const { data } = await this.client.post('/assessments', body);
    const { data: { id } } = data;
    return id;
  }

  async getQuestionnaire(id: object) {
    const { data } = await this.client.get(`/assessments/${id}`);
    const { data: { assessment } } = data;
    return assessment;
  }

  async evaluateAssessment(answers: object, studentId: string, course: string) {
    const { data } = await this.client.post(`/students/${studentId}/assessments/${course}`, {
      answers
    });
    const { data: { results } } = data;
    return results;
  }

  async getStudentAssessment(studentId: object, course: string) {
    const { data } = await this.client.get(`/students/${studentId}/assessments/${course}`);
    const { data: { results } } = data;
    return results;
  }
}