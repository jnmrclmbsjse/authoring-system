import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class CoursesRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async createCourse(body: object) {
    const { data } = await this.client.post('/courses', body);
    const { data: { id } } = data;
    return id;
  }

  async updateCourse(body: object, courseId: number) {
    const { data } = await this.client.put(`/courses/${courseId}`, body);
    const { data: { id } } = data;
    return id;
  }

  async deleteCourse(courseId: number) {
    const { data } = await this.client.delete(`/courses/${courseId}`);
    const { data: { success } } = data;
    return success;
  }

  async details(courseId) {
    const { data } = await this.client.get(`/courses/${courseId}`);
    const { data: { course } } = data;
    return course;
  }

  async getAll() {
    const { data } = await this.client.get('/courses');
    const { data: { courses } } = data;
    return courses;
  }

  async getTeachersCourses() {
    const { data } = await this.client.get('/courses/teachers');
    const { data: { courses } } = data;
    return courses;
  }
}