import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class TokenRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async encode(body: object) {
    const { data } = await this.client.post('/token', body);
    const { data: { token } } = data;
    return token;
  }

  async decode(token: string) {
    const { data } = await this.client.get(`/token`, {
        params: {
            token
        }
    });
    const { data: { payload } } = data;
    return payload;
  }
}