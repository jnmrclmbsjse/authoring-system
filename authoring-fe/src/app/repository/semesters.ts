import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class SemestersRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async createSemester(body: object) {
    const { data } = await this.client.post('/semesters', body);
    const { data: { id } } = data;
    return id;
  }

  async getAll() {
    const { data } = await this.client.get('/semesters');
    const { data: { semesters } } = data;
    return semesters;
  }

  async delete(semesterId: number) {
    const { data } = await this.client.delete(`/semesters/${semesterId}`);
    const { data: { success } } = data;
    return success;
  }
}