import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class LogsRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async getAll() {
    const { data } = await this.client.get(`/logs`);
    const { data: { logs } } = data;
    return logs;
  }

  async getLogsByType(studentId, type) {
    const { data } = await this.client.get(`/students/${studentId}/logs`, {
      params: {
        type,
      }
    });
    const { data: { logs } } = data;
    return logs;
  }
}