import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class TeachersRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async assignCourse({ courseId, teacherId }) {
    const body = {
      courseId
    };
    const { data } = await this.client.post(`/teachers/${teacherId}/courses`, body);
    const { data: { id } } = data;
    return id;
  }
}