import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class RolesRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async getAll() {
    const { data } = await this.client.get('/roles');
    const { data: { roles } } = data;
    return roles;
  }
}