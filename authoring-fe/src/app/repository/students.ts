import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class StudentsRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async assignCourse({ teacherCourseId, studentId }) {
    const body = {
        teacherCourseId
    };
    const { data } = await this.client.post(`/students/${studentId}/courses`, body);
    const { data: { id } } = data;
    return id;
  }
}