import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class FilesRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async upload(form: FormData, headers: Headers) {
    const { data } = await this.client.post('/files', form, {
        headers
    });
    const { data: { filename, success } } = data;

    return filename;
  }

  async download(filename: string) {
    const { data } = await this.client.get(`/files`, {
        params: {
          file: filename
        }
    });
    // const { data: { content } } = data;
    return data;
  }
}