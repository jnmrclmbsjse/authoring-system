import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class ConceptsRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async createConcept(body: object) {
    const { data } = await this.client.post('/concepts', body);
    const { data: { id } } = data;
    return id;
  }

  async updateConcept(body: object, conceptId) {
    const { data } = await this.client.put(`/concepts/${conceptId}`, body);
    const { data: { id } } = data;
    return id;
  }

  async getAll() {
    const { data } = await this.client.get('/concepts');
    const { data: { concepts } } = data;
    return concepts;
  }

  async getConceptDetails(conceptId: number) {
    const { data } = await this.client.get(`/concepts/${conceptId}`);
    const { data: { concept } } = data;
    return concept;
  }

  async getConceptDetailsByCourse(teacherCourseId: number) {
    const { data } = await this.client.get(`/courses/${teacherCourseId}/concepts`);
    const { data: { concept } } = data;
    return concept;
  }
}