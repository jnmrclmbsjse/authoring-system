import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class NodesRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async createNodes(body: object) {
    const { data } = await this.client.post('/nodes', body);
    const { data: { id } } = data;
    return id;
  }

  async getNodeDetails(id: string) {
    const { data } = await this.client.get(`/nodes/${id}`);
    const { data: { node } } = data;
    return node;
  }

  async updateNodeDone(studentId: string, nodeId: string) {
    const { data } = await this.client.post(`/students/${studentId}/nodes/${nodeId}`, {
      done: true
    });
    const { data: { id } } = data;
    return id;
  }

  async getDoneNodes(studentId: string) {
    const { data } = await this.client.get(`/students/${studentId}/nodes`);
    const { data: { nodes } } = data;
    return nodes;
  }
}