import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class QuizzesRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async createQuiz(body: object, contentId: string) {
    const { data } = await this.client.post(`/contents/${contentId}/quizzes`, body);
    const { data: { id } } = data;
    return id;
  }

  async getQuiz(contentId: string) {
    const { data } = await this.client.get(`/contents/${contentId}/quizzes`);
    const { data: { quiz } } = data;
    return quiz;
  }

  async getAllForStudent(studentId: string) {
    const { data } = await this.client.get(`/students/${studentId}/quizzes`);
    const { data: { quizzes } } = data;
    return quizzes;
  }

  async evaluateQuiz(answers: object, studentId: string, contentId: string, formData: FormData) {
    let headers = new Headers();
    /** In Angular 5, including the header Content-Type can invalidate your request */
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    formData.append('answers', JSON.stringify(answers));
    console.log(formData);
    const { data } = await this.client.post(`/students/${studentId}/contents/${contentId}/quizzes`, formData, {
      headers
    });
    const { data: { results } } = data;
    return results;
  }
}