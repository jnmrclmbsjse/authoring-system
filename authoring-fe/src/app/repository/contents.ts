import { Injectable } from '@angular/core';
import { HttpClient } from '@infrastructure/http-client';

@Injectable()
export class ContentsRepository {
  private client;
  constructor (public http: HttpClient) {
    this.client = http.client;
  }

  async getContentDetails(contentId: number) {
    const { data } = await this.client.get(`/contents/${contentId}`);
    const { data: { content } } = data;
    return content;
  }

  async getViewedContent(contentId: number, studentId: number) {
    const { data } = await this.client.get(`/students/${studentId}/contents/${contentId}`);
    const { data: { id } } = data;
    return id;
  }
}