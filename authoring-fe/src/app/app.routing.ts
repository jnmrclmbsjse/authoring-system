import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from '@containers/index';

import { LoginComponent } from '@views/login/login.component';

export const routes: Routes = [{
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Dashboard'
    },
    children: [{
        path: 'students',
        loadChildren: () => import('@views/students/students.module').then(m => m.StudentsModule)
      },
      {
        path: 'users',
        loadChildren: () => import('@views/users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'habits',
        loadChildren: () => import('@views/habits/habits.module').then(m => m.HabitsModule)
      },
      {
        path: 'map',
        loadChildren: () => import('@app/views/map/map.module').then(m => m.MapModule)
      },
      {
        path: 'courses',
        loadChildren: () => import('@views/courses/courses.module').then(m => m.CoursesModule)
      },
      {
        path: 'achievements',
        loadChildren: () => import('@views/achievements/achievements.module').then(m => m.AchievementsModule)
      },
      {
        path: 'concepts',
        loadChildren: () => import('@views/concepts/concepts.module').then(m => m.ConceptsModule)
      },
      {
        path: 'semesters',
        loadChildren: () => import('@views/semesters/semesters.module').then(m => m.SemestersModule)
      },
      {
        path: 'assessments',
        loadChildren: () => import('@views/assessments/assessments.module').then(m => m.AssessmentsModule)
      },
      {
        path: 'interims',
        loadChildren: () => import('@views/interims/interims.module').then(m => m.InterimsModule)
      },
      {
        path: 'results',
        loadChildren: () => import('@views/results/results.module').then(m => m.ResultsModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('@views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'icons',
        loadChildren: () => import('@views/icons/icons.module').then(m => m.IconsModule)
      },
    ]
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
