import axios from 'axios';
import { LocalStorageHelper } from '@helpers/localstorage.service';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class HttpClient {
    public client;
    env = environment;
    constructor(private storage: LocalStorageHelper) {
        this.client = axios.create({
            baseURL: this.env.backend,
            headers: {'X-Session-Token': storage.retrieve('a-ext-token')}
        });
    }
}